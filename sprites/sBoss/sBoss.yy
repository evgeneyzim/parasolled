{
  "bboxMode": 2,
  "collisionKind": 2,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 36,
  "bbox_right": 315,
  "bbox_top": 34,
  "bbox_bottom": 313,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 350,
  "height": 350,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 8,
  "gridY": 8,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a5aaff81-df4f-4a8e-8d4f-f8a5b03151ef","path":"sprites/sBoss/sBoss.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a5aaff81-df4f-4a8e-8d4f-f8a5b03151ef","path":"sprites/sBoss/sBoss.yy",},"LayerId":{"name":"0386b698-3d35-4126-bd66-6bd78b731d00","path":"sprites/sBoss/sBoss.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBoss","path":"sprites/sBoss/sBoss.yy",},"resourceVersion":"1.0","name":"a5aaff81-df4f-4a8e-8d4f-f8a5b03151ef","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sBoss","path":"sprites/sBoss/sBoss.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 60.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"677d327c-b670-4733-9d4d-4bcc60dfe4ad","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a5aaff81-df4f-4a8e-8d4f-f8a5b03151ef","path":"sprites/sBoss/sBoss.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 175,
    "yorigin": 175,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sBoss","path":"sprites/sBoss/sBoss.yy",},
    "resourceVersion": "1.3",
    "name": "sBoss",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0386b698-3d35-4126-bd66-6bd78b731d00","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Enemies",
    "path": "folders/Fonts/Sprite/Sprites/Enemies.yy",
  },
  "resourceVersion": "1.0",
  "name": "sBoss",
  "tags": [],
  "resourceType": "GMSprite",
}