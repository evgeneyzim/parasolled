{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 8,
  "bbox_right": 54,
  "bbox_top": 27,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"46597a88-6106-4ef2-ab1b-19fadcf11336","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"46597a88-6106-4ef2-ab1b-19fadcf11336","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":{"name":"710d5684-94ec-4cc7-8eb1-df887ab02df3","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerMove","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"46597a88-6106-4ef2-ab1b-19fadcf11336","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70aad77b-634d-4cc1-9f6e-27d09cbe3111","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70aad77b-634d-4cc1-9f6e-27d09cbe3111","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":{"name":"710d5684-94ec-4cc7-8eb1-df887ab02df3","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerMove","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"70aad77b-634d-4cc1-9f6e-27d09cbe3111","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a03c4c17-767f-4bd3-84f5-ff1a6446a429","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a03c4c17-767f-4bd3-84f5-ff1a6446a429","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":{"name":"710d5684-94ec-4cc7-8eb1-df887ab02df3","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerMove","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"a03c4c17-767f-4bd3-84f5-ff1a6446a429","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d16a3e67-c9c4-4d44-b4be-7746cb288b74","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d16a3e67-c9c4-4d44-b4be-7746cb288b74","path":"sprites/sPlayerMove/sPlayerMove.yy",},"LayerId":{"name":"710d5684-94ec-4cc7-8eb1-df887ab02df3","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sPlayerMove","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","name":"d16a3e67-c9c4-4d44-b4be-7746cb288b74","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sPlayerMove","path":"sprites/sPlayerMove/sPlayerMove.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"bb3406aa-b9f3-4bce-b0be-1cf8dbf5bc8a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"46597a88-6106-4ef2-ab1b-19fadcf11336","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af83ad5d-1b59-4727-9ffe-fc1b0e081bde","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70aad77b-634d-4cc1-9f6e-27d09cbe3111","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9835f293-25f1-4c23-8d7c-cc4143896f62","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a03c4c17-767f-4bd3-84f5-ff1a6446a429","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e5b0c323-8d25-4437-ac67-695b52b32f26","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d16a3e67-c9c4-4d44-b4be-7746cb288b74","path":"sprites/sPlayerMove/sPlayerMove.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 33,
    "yorigin": 50,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sPlayerMove","path":"sprites/sPlayerMove/sPlayerMove.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"710d5684-94ec-4cc7-8eb1-df887ab02df3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player",
    "path": "folders/Fonts/Sprite/Sprites/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "sPlayerMove",
  "tags": [],
  "resourceType": "GMSprite",
}