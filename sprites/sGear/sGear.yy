{
  "bboxMode": 0,
  "collisionKind": 0,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"0ac66d77-81cb-420c-bc14-a245e51728d0","path":"sprites/sGear/sGear.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0ac66d77-81cb-420c-bc14-a245e51728d0","path":"sprites/sGear/sGear.yy",},"LayerId":{"name":"99f0bb08-a07e-45e7-a63a-0f7c381ab883","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGear","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":"0ac66d77-81cb-420c-bc14-a245e51728d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf4dff40-6056-405a-904a-2f46a0d9339f","path":"sprites/sGear/sGear.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf4dff40-6056-405a-904a-2f46a0d9339f","path":"sprites/sGear/sGear.yy",},"LayerId":{"name":"99f0bb08-a07e-45e7-a63a-0f7c381ab883","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGear","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":"bf4dff40-6056-405a-904a-2f46a0d9339f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"221fc718-fa80-417c-bfa7-c2ffac7c79ae","path":"sprites/sGear/sGear.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"221fc718-fa80-417c-bfa7-c2ffac7c79ae","path":"sprites/sGear/sGear.yy",},"LayerId":{"name":"99f0bb08-a07e-45e7-a63a-0f7c381ab883","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGear","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":"221fc718-fa80-417c-bfa7-c2ffac7c79ae","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8c5a2f14-2277-4412-b9fa-64a0654a103f","path":"sprites/sGear/sGear.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8c5a2f14-2277-4412-b9fa-64a0654a103f","path":"sprites/sGear/sGear.yy",},"LayerId":{"name":"99f0bb08-a07e-45e7-a63a-0f7c381ab883","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGear","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","name":"8c5a2f14-2277-4412-b9fa-64a0654a103f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sGear","path":"sprites/sGear/sGear.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f8701682-d723-4543-ac7f-d4effe351c1d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0ac66d77-81cb-420c-bc14-a245e51728d0","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"133f2bb8-142a-4916-91fc-a7398f532153","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf4dff40-6056-405a-904a-2f46a0d9339f","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f1c70bf-300c-4902-986a-b9ca6ae93ac9","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"221fc718-fa80-417c-bfa7-c2ffac7c79ae","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"97dafb43-ef04-4865-b3e1-12f3b658f43d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8c5a2f14-2277-4412-b9fa-64a0654a103f","path":"sprites/sGear/sGear.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sGear","path":"sprites/sGear/sGear.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"99f0bb08-a07e-45e7-a63a-0f7c381ab883","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Level",
    "path": "folders/Fonts/Sprite/Sprites/Level.yy",
  },
  "resourceVersion": "1.0",
  "name": "sGear",
  "tags": [],
  "resourceType": "GMSprite",
}