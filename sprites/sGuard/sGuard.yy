{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 1,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 93,
  "bbox_top": 1,
  "bbox_bottom": 95,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 96,
  "height": 96,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a10c94f1-1678-4282-93a3-f0a610675f18","path":"sprites/sGuard/sGuard.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a10c94f1-1678-4282-93a3-f0a610675f18","path":"sprites/sGuard/sGuard.yy",},"LayerId":{"name":"f9ef109c-ee6d-4e73-b8ef-6fefa85cb9d9","path":"sprites/sGuard/sGuard.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGuard","path":"sprites/sGuard/sGuard.yy",},"resourceVersion":"1.0","name":"a10c94f1-1678-4282-93a3-f0a610675f18","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"16a07061-8981-464a-83fc-e55a788b5447","path":"sprites/sGuard/sGuard.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"16a07061-8981-464a-83fc-e55a788b5447","path":"sprites/sGuard/sGuard.yy",},"LayerId":{"name":"f9ef109c-ee6d-4e73-b8ef-6fefa85cb9d9","path":"sprites/sGuard/sGuard.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGuard","path":"sprites/sGuard/sGuard.yy",},"resourceVersion":"1.0","name":"16a07061-8981-464a-83fc-e55a788b5447","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sGuard","path":"sprites/sGuard/sGuard.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9809e269-6cea-41c9-8784-774bd672282d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a10c94f1-1678-4282-93a3-f0a610675f18","path":"sprites/sGuard/sGuard.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"85dc380e-9262-4e49-993c-4678d380ca1c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"16a07061-8981-464a-83fc-e55a788b5447","path":"sprites/sGuard/sGuard.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 48,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sGuard","path":"sprites/sGuard/sGuard.yy",},
    "resourceVersion": "1.3",
    "name": "sGuard",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f9ef109c-ee6d-4e73-b8ef-6fefa85cb9d9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Enemies",
    "path": "folders/Fonts/Sprite/Sprites/Enemies.yy",
  },
  "resourceVersion": "1.0",
  "name": "sGuard",
  "tags": [],
  "resourceType": "GMSprite",
}