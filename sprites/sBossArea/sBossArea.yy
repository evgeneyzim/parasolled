{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 63,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d657adb8-5284-4731-acea-d7f03f3c97bb","path":"sprites/sBossArea/sBossArea.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d657adb8-5284-4731-acea-d7f03f3c97bb","path":"sprites/sBossArea/sBossArea.yy",},"LayerId":{"name":"3cbbbb90-93d9-4e78-9da4-740a5ee05624","path":"sprites/sBossArea/sBossArea.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBossArea","path":"sprites/sBossArea/sBossArea.yy",},"resourceVersion":"1.0","name":"d657adb8-5284-4731-acea-d7f03f3c97bb","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sBossArea","path":"sprites/sBossArea/sBossArea.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7845e693-eef3-4e83-9c22-f37cd183da86","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d657adb8-5284-4731-acea-d7f03f3c97bb","path":"sprites/sBossArea/sBossArea.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 32,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sBossArea","path":"sprites/sBossArea/sBossArea.yy",},
    "resourceVersion": "1.3",
    "name": "sBossArea",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":30.0,"displayName":"default","resourceVersion":"1.0","name":"3cbbbb90-93d9-4e78-9da4-740a5ee05624","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Marks",
    "path": "folders/Fonts/Sprite/Sprites/Marks.yy",
  },
  "resourceVersion": "1.0",
  "name": "sBossArea",
  "tags": [],
  "resourceType": "GMSprite",
}