{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 56,
  "bbox_top": 17,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c38b3cbe-f633-46b9-9e1f-1169a2ea279d","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c38b3cbe-f633-46b9-9e1f-1169a2ea279d","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":{"name":"428f9570-48cb-4349-b741-955c2801c6e2","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoonWalk","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"c38b3cbe-f633-46b9-9e1f-1169a2ea279d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"58f16157-0e2a-40d4-9324-6770a88765ee","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"58f16157-0e2a-40d4-9324-6770a88765ee","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":{"name":"428f9570-48cb-4349-b741-955c2801c6e2","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoonWalk","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"58f16157-0e2a-40d4-9324-6770a88765ee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f99f3c5f-9ef7-4c53-b490-289c4eacb4ee","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f99f3c5f-9ef7-4c53-b490-289c4eacb4ee","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":{"name":"428f9570-48cb-4349-b741-955c2801c6e2","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoonWalk","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"f99f3c5f-9ef7-4c53-b490-289c4eacb4ee","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1d09338f-26ba-4e71-819a-36c5c9000e1c","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1d09338f-26ba-4e71-819a-36c5c9000e1c","path":"sprites/sGoonWalk/sGoonWalk.yy",},"LayerId":{"name":"428f9570-48cb-4349-b741-955c2801c6e2","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sGoonWalk","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","name":"1d09338f-26ba-4e71-819a-36c5c9000e1c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sGoonWalk","path":"sprites/sGoonWalk/sGoonWalk.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e796c514-e0e8-4d52-abe0-b30cfec2680d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c38b3cbe-f633-46b9-9e1f-1169a2ea279d","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6aa053d3-5aed-4cc1-a120-8e9486128fba","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"58f16157-0e2a-40d4-9324-6770a88765ee","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a9d96ac1-a688-4789-9200-1ce973c55d09","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f99f3c5f-9ef7-4c53-b490-289c4eacb4ee","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"05d73201-9b7a-42ed-8ab2-66d06e1d958f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1d09338f-26ba-4e71-819a-36c5c9000e1c","path":"sprites/sGoonWalk/sGoonWalk.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 50,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sGoonWalk","path":"sprites/sGoonWalk/sGoonWalk.yy",},
    "resourceVersion": "1.3",
    "name": "sGoonWalk",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"428f9570-48cb-4349-b741-955c2801c6e2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Enemies",
    "path": "folders/Fonts/Sprite/Sprites/Enemies.yy",
  },
  "resourceVersion": "1.0",
  "name": "sGoonWalk",
  "tags": [],
  "resourceType": "GMSprite",
}