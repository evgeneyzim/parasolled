{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "soSlimeLanding.wav",
  "duration": 0.383005,
  "parent": {
    "name": "SFX",
    "path": "folders/Sounds/SFX.yy",
  },
  "resourceVersion": "1.0",
  "name": "soSlimeLanding",
  "tags": [],
  "resourceType": "GMSound",
}