{
  "compression": 0,
  "volume": 0.35,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "musTron",
  "duration": 10.9241161,
  "parent": {
    "name": "Music",
    "path": "folders/Sounds/Music.yy",
  },
  "resourceVersion": "1.0",
  "name": "musTron",
  "tags": [],
  "resourceType": "GMSound",
}