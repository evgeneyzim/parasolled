//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

const vec4 COLOR = vec4(0.0, 1.0, 0.0, 1.0);

uniform float offsetX;
uniform float offsetY;

const float mult = 10.0;


void main()
{
	vec4 color;
	float alpha = 1.0;
	
	alpha *= texture2D( gm_BaseTexture, v_vTexcoord - mult * offsetX ).a;
    alpha *= texture2D( gm_BaseTexture, v_vTexcoord + mult * offsetX ).a;
    alpha *= texture2D( gm_BaseTexture, v_vTexcoord - mult * offsetY ).a;
    alpha *= texture2D( gm_BaseTexture, v_vTexcoord + mult * offsetY ).a;
	
	if (alpha == 0.0)
	{
		color = COLOR;	
	}
	else
	{
		color = v_vColour;
	}
		
    gl_FragColor = color * texture2D( gm_BaseTexture, v_vTexcoord );
}
