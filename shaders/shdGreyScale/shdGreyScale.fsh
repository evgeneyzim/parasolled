//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float time;

void main()
{
    vec4 input_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
	vec4 des_col = input_col;
	des_col.rgb = vec3((input_col.r + input_col.g + input_col.b)/3.0);
	
	
	input_col.r += (des_col.r - input_col.r) * (time / 20.0);
	input_col.g += (des_col.g - input_col.g) * (time / 20.0);
	input_col.b += (des_col.b - input_col.b) * (time / 20.0);
	
	
	gl_FragColor = input_col;
}
