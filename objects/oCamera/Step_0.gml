/// @description follow target, resize screen

//Set target
if (timer_special == 0) 
{
	if (instance_exists(oPlayer) && room != rMenu)
	{
		var _found = false;
		with (oCameraFrame)
		{
			if (place_meeting(x, y, oPlayer))
			{
				oCamera.follow = id;	
				_found = true;
				break;
			}	
		}
	
		if (!_found)
		{
			oCamera.follow = oPlayer;	
		}
	}
}
else
{
	timer_special--;
}

//Follow target
instance_activate_object(follow);
if (instance_exists(follow)) 
{
	var _follow_x = follow.x;
	var _follow_y = follow.y;
	
	if (follow == oPlayer)
	{
		var _follow_x = follow.x + 5 * follow.xspeed_saved;
		var _follow_y = follow.y + follow.xspeed_saved;	
	}
	
	//Vary speed
	if (follow == oPlayer && (room == rDoubleJumpIntro || room == rHorizontalFan || room == rChase))
	{
		if (abs(_follow_x - x) > 10)		x += (_follow_x - x)/5;
		if (abs(_follow_y - y) > 10)		y += (_follow_y - y)/5;
	}
	else
	{
		if (abs(_follow_x - x) > 10)		x += (_follow_x - x)/50;
		if (abs(_follow_y - y) > 10)		y += (_follow_y - y)/50;
	}
}

//Clamp camera
x = clamp(x, view_w_half + buff, room_width - view_w_half - buff);
y = clamp(y, view_h_half + buff, room_height - view_h_half - buff);

//Shake screen
x += random_range(-shake_remain, shake_remain);
y += random_range(-shake_remain, shake_remain);
shake_remain = max(0, shake_remain - ((1/shake_lenght)*shake_magnitude));


//Set position and size
view_w_half = lerp(view_w_half, view_w * zoom / 2, acc);
view_h_half = lerp(view_h_half, view_h * zoom / 2, acc);
	
camera_set_view_size(camera, view_w_half * 2, view_h_half * 2);
camera_set_view_pos(camera, x - view_w_half, y - view_h_half);


