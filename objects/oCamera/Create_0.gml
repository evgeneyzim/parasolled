/// @description Initialize variables

camera = view_camera[0];

view_w = camera_get_view_width(camera);
view_h = camera_get_view_height(camera);

view_w_half = view_w / 2;
view_h_half = view_h / 2;


mode = (room == rMenu) ? CameraMode.MENU : CameraMode.NORMAL;


switch(mode)
{
	case CameraMode.MENU:
		follow = oMenu;
		break;
		
	case CameraMode.NORMAL:
		follow = oPlayer;
		break;
}

//Screen shake
shake_lenght = 0;
shake_magnitude = 0;
shake_remain = 0;
buff = 0;


//Zoom
zoom = 1;
acc = 0.02; //zoom speed

//for zooming
timer_special_max = 2 * 60;
timer_special = 0;

draw_target_x = -1;
draw_target_y = -1;


cursor_block = false;

FindCursorPosition = function()
{
	//Draw cursor
	if (global.input_type == InputType.GAMEPAD)
	{		
		if (instance_exists(oPlayer)) with (oPlayer)
		{
			if ((parasol == noone && gun == noone) || just_taken)
			{
				target_x = -1;
				target_y = -1;
			}
			else if ((parasol != noone && parasol.state == ParasolStates.CLOSED) || gun != noone)
			{
				var _hor = gamepad_axis_value(SelectGamepad(0), gp_axisrh);
				var _ver = gamepad_axis_value(SelectGamepad(0), gp_axisrv);
		
				if (abs(_hor) > 0.2 || abs(_ver) > 0.2)
				{
					pointed_angle = point_direction(0, 0, _hor, _ver);
				}	
		
				var _x = oPlayer.x;
				var _y = oPlayer.y;
	
				var _top = oCamera.y - oCamera.view_h_half;
				var _bottom = oCamera.y + oCamera.view_h_half;
				var _left = oCamera.x - oCamera.view_w_half;
				var _right = oCamera.x + oCamera.view_w_half;
	
	
				while (!position_meeting(_x, _y, oBlock) && !position_meeting(_x, _y, oBoss) && !position_meeting(_x, _y, oSlime) && !position_meeting(_x, _y, oGuard) && !position_meeting(_x, _y, oGoon) && !position_meeting(_x, _y, oStaticSquare) && _x > _left && _x < _right && _y > _top && _y < _bottom)
				{
					_x += lengthdir_x(1, pointed_angle);
					_y += lengthdir_y(1, pointed_angle);
				}
		
		
	
				target_desired_x = _x;
				target_desired_y = _y;
	
				target_x = lerp(target_x, target_desired_x, 0.3);
				target_y = lerp(target_y, target_desired_y, 0.3);
	
				if (parasol != noone)
				{
					var _dist_x = target_x - x;	
					var _dist_y = target_y - y;	
		
					if (sqr(_dist_x) + sqr(_dist_y) < sqr(PARASOL_WIDTH))
					{
						_dist_x = lengthdir_x(PARASOL_WIDTH, pointed_angle);	
						_dist_y = lengthdir_y(PARASOL_WIDTH, pointed_angle);	
			
						target_x = x + _dist_x;
						target_y = y + _dist_y;
					}
				}
		
			}
			else if (parasol != noone && parasol.state == ParasolStates.SHIELD)
			{
				var _hor = gamepad_axis_value(SelectGamepad(0), gp_axislh);
				var _ver = gamepad_axis_value(SelectGamepad(0), gp_axislv);	
		
				if (abs(_hor) > 0.2 || abs(_ver) > 0.2)
				{
					pointed_angle = point_direction(0, 0, _hor, _ver);	
				}	
		
				var _dist_x = lengthdir_x(PARASOL_WIDTH, pointed_angle);	
				var _dist_y = lengthdir_y(PARASOL_WIDTH, pointed_angle);	
			
				target_x = x + _dist_x;
				target_y = y + _dist_y;			
			}
			else
			{
				target_x = -1;
				target_y = -1;
			}
		
			oCamera.draw_target_x = target_x;
			oCamera.draw_target_y = target_y;
		}	
	}
}