if (cursor_block)	exit;

if (global.input_type == InputType.KEYBOARD)
{
	//Draw mouse
	var _x = device_mouse_x_to_gui(0);
	var _y = device_mouse_y_to_gui(0);

	if (_x < 0 || _y < 0 || _x > display_get_gui_width() || _y > display_get_height())
	{
		//pass	
	}
	else
	{
		if (room !=  rMenu && global.input_type == InputType.KEYBOARD)
		{
			if (instance_exists(oPlayer) && oPlayer.parasol != noone && oPlayer.state != PlayerStates.VICTORY)
																		draw_set_alpha(oPlayer.parasol.draw_alpha);
			else if (instance_exists(oPlayer) && oPlayer.gun != noone)	draw_set_alpha(1);
			else														draw_set_alpha(0.5);
			
			draw_set_color(c_white);
			draw_sprite(sTarget, 0, _x, _y);
			draw_set_alpha(1);
		}
	}
}
else
{
	FindCursorPosition();
	
	if (draw_target_x != -1 && draw_target_y != -1)
	{
		var _cam = oCamera.camera;
		
		var _cw = camera_get_view_width(_cam);
		var _ch = camera_get_view_height(_cam);
		
		var _display_scale_x = display_get_gui_width() / _cw;
		var _display_scale_y = display_get_gui_height() / _ch;
		
		var _cx = camera_get_view_x(_cam);
		var _cy = camera_get_view_y(_cam);
		
		var _xx = (draw_target_x - _cx) * _display_scale_x;
		var _yy = (draw_target_y - _cy) * _display_scale_y;
				
		
		if (instance_exists(oPlayer) && oPlayer.parasol != noone)
			draw_set_alpha(oPlayer.parasol.draw_alpha);
		else if (instance_exists(oPlayer) && oPlayer.gun != noone)
			draw_set_alpha(1);
		else						
			draw_set_alpha(0.5);
		draw_set_color(c_white);
		draw_sprite(sTarget, 0, _xx, _yy);
		draw_set_alpha(1);
	}
}