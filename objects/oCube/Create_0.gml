event_inherited();

enum CubeState
{
	FREE,
	HOLD,
};

state = CubeState.FREE;

xspeed = 0;
yspeed = 0;

start_of_the_level = true;
once = true;


DrawIt = function()
{
	var _size = sprite_get_width(sCube) / 3;
	var _x1 = x;
	var _y1 = y;
	var _x2 = x + sprite_width - _size;
	var _y2 = y + sprite_height - _size;
	
	var _w = _x2 - _x1;
	var _h = _y2 - _y1;
	
	var _columns = _w div _size;
	var _rows	 = _h div _size;
	
	//CORNERS
	//top left
	draw_sprite_part(sCube, 0, 0, 0, _size, _size, _x1, _y1);
	
	//top right
	draw_sprite_part(sCube, 0, _size * 2, 0, _size, _size, _x1 + ((_columns) * _size), _y1);
	
	//bottom left
	draw_sprite_part(sCube, 0, 0, _size * 2, _size, _size, _x1, _y1 + ((_rows) * _size));
	
	//bottom right
	draw_sprite_part(sCube, 0, _size * 2, _size * 2, _size, _size, _x1 + ((_columns) * _size), _y1 + ((_rows) * _size));
	
	
	//EDGES
	for (var i = 1; i < (_rows); ++i)
	{
		//left edge
		draw_sprite_part(sCube, 0, 0, _size, _size, _size, _x1, _y1 + (i * _size));
		
		//right edge
		draw_sprite_part(sCube, 0, _size * 2, _size, _size, _size, _x1 + (_columns * _size), _y1 + (i * _size));
		
	}
	
	for (var i = 1; i < (_columns); ++i)
	{
		//top edge
		draw_sprite_part(sCube, 0, _size, 0, _size, _size, _x1 + (i * _size), _y1);
		
		//bottom edge
		draw_sprite_part(sCube, 0, _size, _size * 2, _size, _size, _x1 + (i * _size), _y1 + (_rows * _size));
		
	}
	

	//MIDDLE
	for (var i =  1; i < (_columns); ++i)
	{
		for (var j = 1; j < (_rows); ++j)
		{
			draw_sprite_part(sCube, 0, _size, _size, _size, _size, _x1 + (i * _size), _y1 + (j * _size));	
		}
	}
}
