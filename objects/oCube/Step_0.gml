instance_activate_region(x - abs(sprite_width), y - abs(sprite_height), 2 * abs(sprite_width), 2 * abs(sprite_height), true);


switch (state)
{
	case CubeState.FREE:
		if (place_meeting(x + xspeed, y, oBlock) && !place_meeting(x + xspeed, y, oGuard))
		{
			repeat(abs(xspeed))	
			{
				if (place_meeting(x + sign(xspeed), y, oBlock))		break;
				x += sign(xspeed);
			}
			xspeed = 0;
		}

		x += xspeed;	
		
		yspeed += global.grav;
		
		if (place_meeting(x, y + yspeed, oBlock) && !place_meeting(x, y + yspeed, oGuard))
		{
			
			if (yspeed != global.grav)
			{
				if (!start_of_the_level)
				{
					if (once)
						audio_play_sound(soBoxFalling, 0, false);
					once = false;
					
				}
				else
				{
					start_of_the_level = false;	
				}
			}
			
			repeat(abs(yspeed))	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))		break;
				y += sign(yspeed);
			}
			yspeed = 0;
		}

		yspeed = sign(yspeed) * min(15, abs(yspeed));

		y += yspeed;
		
		
		//Kill
		var _targets = [oPlayer, oSlime, oGuard];
		
		for (var i = 0; i < array_length(_targets); ++i)
		{
			with (_targets[i])
			{
				var _target = id;
			
				//if target inside
				if (_target.bbox_right > other.bbox_left && _target.bbox_left < other.bbox_right)
				{
					//if falling over the target
					if (other.yspeed > 0 && _target.bbox_top < other.bbox_bottom)
					{
						//if target is blocked
						if (place_meeting(x, y + 1, oWall) || place_meeting(x, y + 1, oSpecialWall))
						{
							if (object_index == oPlayer)
							{
								var _height = abs(max_height - y);		
								audio_sound_pitch(soDying, choose(0.8, 0.9, 1.0));
								audio_sound_gain(soDying, (0.8 + _height/room_height) * global.sfx_volume, 0);
								audio_play_sound(soDying, 1, false);	
							}
							instance_destroy(_target);
						}
					}
				}
			}
		}
	
		
		break;
	default:
		break;
}