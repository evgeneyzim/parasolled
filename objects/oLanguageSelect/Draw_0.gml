draw_set_font(fntSpeedrun);
draw_set_halign(fa_center);
draw_set_color(c_white);

alpha -= alpha_dec;

if (alpha <= 0.1)
{
	alpha = 1;
	index++;
	if (index >= array_length(titles))
		index = 0;
}

draw_text_color(room_width/2, 24, titles[index], c_white, c_white, c_white, c_white, alpha);



DrawMenu(buttons, position, room_width/2, room_height/2, true);
