if (file_exists(global.filename))
{
	room_goto(rMenu);	
}

titles = 
[
	"Choose your language:",
	"Выберите язык:",
];

index = 0;
alpha = 1;
alpha_dec = 0.005;

buttons = 
[
	"English",
	"Русский"
];


//For options
angle = 0;
angle_max = 3.5;
scale = 1.2;
scale_avr = 1.2;
scale_bounce = 0.05;

position = 0;
position_max = 2;

center = true;
