//Waving
angle = angle_max * sin(get_timer()/250000);
scale = scale_avr + scale_bounce * sin(get_timer()/500000);	

//Window
if (center)
{
	center = false;
	window_center();
}

//Input
var _gp = SelectGamepad(0);

var _input = new GamepadInput(_gp);

var _up = keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W")) || gamepad_button_check_pressed(_gp, gp_padu) || (_input.up);
var _down = keyboard_check_pressed(vk_down) || keyboard_check_pressed(ord("S")) || gamepad_button_check_pressed(_gp, gp_padd) || (_input.down);

var _select = keyboard_check_pressed(vk_space) || keyboard_check_pressed(vk_enter) || gamepad_button_check_pressed(_gp, gp_face1);
var _escape = keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(_gp, gp_select);

delete _input;


if (_escape)
	game_end();

//Sounds
if (_down || _up || _select)
{
	audio_play_sound(soMenuClick, 0, false);	
}

//Posiiton
position += (_down - _up);

if (position > position_max - 1)
	position = 0;
if (position < 0)
	position = position_max - 1;
	
	
//Confirm
if (_select)
{
	switch (position)
	{
		case 0:
			global.language = "English";
			break;
		case 1:
			global.language = "Russian";
			break;
		default:
			Raise("Language error\n");
			break;
	}
	
}

if (_select)
{
	room_goto(rMenu);	
}