if (start)
{

	if (change)
	{
		
		if (amount < amount_max)
		{
		
			amount++;
	
		
			do 
			{
				_id = instance_find(oButtonPress, max(0, irandom(instance_number(oButtonPress)) - 1));
			} until (_id != prev_id);
		
			prev_id = _id;
		
		
			with (_id)
			{
				functions[0] = function()
				{
					with (oButtonController)
					{
						change = true;
					}	
				
					special = false;
					state_changer = true;
				}
			
				special = true;
			}
				}
		else
		{
		
			var _pr = oButtonPrefab;
		
	with (instance_create_layer(_pr.x, _pr.y, "Buttons", oButton))
	{
			
		sprite_index = sButtonFinal;
		animate = true;
			
		linked_block = _pr.linked_block;
		linked_block.button = id;
					
		functions[0] = function()
		{
			with (inst_537606CD) 
			{
				start = true;
				spd = 0.5;
			}
	
			with (oFinalBoss) 
			{
				state = FinalBossStates.CHASE;
			}
	
			with (inst_4C1C9C4D) start = true;
			with (inst_74DD3EC3) start = false;
			with (inst_295C1B28) start = false;
	
			with (oCamera)
			{
				timer_special = timer_special_max;
				follow = oPortal;
			}
					
			with (inst_3300AC11) 
			{
				start = false;
			}
					
			instance_create_layer(0, 0, "Items", oAlarm);
					
			ShakeScreen(30, 30);
					
					
		}
			
		deactivators[0] = function()
		{
			with (inst_537606CD)
			{
				start = false;
				spd = 8;
			}
			
			with (oButtonPrefab)
			{
				image_speed = -1;
				
			}
			
			instance_destroy();
		}
	}
				
		}
	
		change = false;
	}
}