if (room == rThanksForPlaying || room == rSpeedrunResults)
{
	state = SpeedrunnerStates.SHOW_RESULTS;
	
	application_surface_draw_enable(true);	
}
else
{
	start_time[array_length(start_time)] = current_time;
}

