enum SpeedrunnerStates
{
	RECORD,
	SHOW_RESULTS,
};

state = SpeedrunnerStates.RECORD;

//Start of the room
start_time = [];

//End of the room
end_time = [];


//Total time
total_time = 0;

room_time = 0;

record = false;

angle = 0;
angle_max = 3.5;
scale = 1.2;
scale_avr = 1.2;
scale_bounce = 0.05;

alpha_screen = 1;

timers = [70];
for (var i = 0; i < NUMBER_OF_LEVELS; ++i)
	timers[i + 1] = 10;

timers[25] = 60;
timers[26] = 60;
timers[27] = 75;
timers[28] = 60;

index = 0;

GetTime = function(_time)
{
	var _milliseconds_total = _time % 1000;
	var _seconds_total = _time div 1000;
	var _minutes_total = _seconds_total div 60;

	_seconds_total %= 60;
	
	var _milliseconds_total_text;
	var _seconds_total_text;
	var _minutes_total_text;

	if (_milliseconds_total < 10)
	{
		_milliseconds_total_text = "00" + string(_milliseconds_total);
	}
	else if (_milliseconds_total < 100)
	{
		_milliseconds_total_text = "0" + string(_milliseconds_total);
	}
	else
	{
		_milliseconds_total_text = string(_milliseconds_total);
	}


	if (_seconds_total < 10)
	{
		_seconds_total_text = "0" + string(_seconds_total);
	}
	else
	{
		_seconds_total_text = string(_seconds_total);
	}

	if (_minutes_total < 10)
	{
		_minutes_total_text = "0" + string(_minutes_total);
	}
	else
	{
		_minutes_total_text = string(_minutes_total);
	}
	
	var _total_text = _minutes_total_text + ":" + _seconds_total_text + "." + _milliseconds_total_text;
	return _total_text;
}

DrawTime = function(_x, _y, _time)
{	
	var _total_text = GetTime(_time);
	
	draw_text(_x, _y, _total_text);
}

DrawTimer = function(_time, _offset_y)
{
	var _milliseconds_total = _time % 1000;
	var _seconds_total = _time div 1000;
	var _minutes_total = _seconds_total div 60;

	_seconds_total %= 60;


	var _milliseconds_total_text;
	var _seconds_total_text;
	var _minutes_total_text;

	if (_milliseconds_total < 10)
	{
		_milliseconds_total_text = "00" + string(_milliseconds_total);
	}
	else if (_milliseconds_total < 100)
	{
		_milliseconds_total_text = "0" + string(_milliseconds_total);
	}
	else
	{
		_milliseconds_total_text = string(_milliseconds_total);
	}


	if (_seconds_total < 10)
	{
		_seconds_total_text = "0" + string(_seconds_total);
	}
	else
	{
		_seconds_total_text = string(_seconds_total);
	}

	if (_minutes_total < 10)
	{
		_minutes_total_text = "0" + string(_minutes_total);
	}
	else
	{
		_minutes_total_text = string(_minutes_total);
	}

	var _total_text = _minutes_total_text + ":" + _seconds_total_text + "." + _milliseconds_total_text;

	draw_set_font(fontTitle);
	//draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);

	var _char_width = string_width("0");

	var _offset_x = 14;

	var _len = string_length(_total_text);

	for (var i = 0; i < _len; ++i)
	{
		var _chr = string_char_at(_total_text, i + 1);

		draw_text(_ww - _offset_x - (_len - i - 1) * _char_width, _offset_y, _chr);
	}
}




