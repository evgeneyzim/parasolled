if (room == rSpeedrunResults)
{
	var _ww = room_width;
	var _wh = room_height;

	var _offset_y = 24;
		
	var _space_y = 60;
	var _space_x = _ww/4;
		
	draw_set_font(fntSpeedrun);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_color(c_white);
		
	if (timers[0] == 0)
		draw_text(_ww/2, _offset_y, GetText(51));
		
	for (var j = 0; j < 3; ++j)
	{
		for (var i = 0; i < 8; ++i)
		{
			if (j * 8 + i >= array_length(end_time))
			{
				break;	
			}
				
			if (timers[j * 8 + i + 1] > 0)	break;
				
			var _xadd;
				
			if (j == 0)	_xadd = -48;
			if (j == 1)	_xadd = 0;
			if (j == 2) _xadd = 48;
				
			draw_set_color(c_gray);
			draw_text(_space_x * (j + 1/2) + _xadd, _offset_y + _space_y * (i + 2.5), string(j * 8 + i + 1) + ".");
				
			if (global.speedrun_levels[j * 8 + i] == -1 || global.speedrun_levels[j * 8 + i] > end_time[j * 8 + i])
			{
				draw_set_color(c_green);	
			}
			else
			{
				draw_set_color(c_red);	
			}
			DrawTime(_space_x * (j + 1) + _xadd, _offset_y + _space_y * (i + 2.5), end_time[j * 8 + i]);
		}
	}
		
	var _cur_y = _offset_y + _space_y * (8 + 2.5);
	var _draw_y = (_wh + _cur_y / 2)/2 + _space_y;
		
	var _total = GetTime(total_time);
	var _best =  GetTime(global.speedrun_highscore);
		
	if (timers[25] == 0)
	{
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(_space_x, _draw_y, GetText(52));
		draw_set_halign(fa_right);
		draw_set_color(c_red);
		if (total_time == global.speedrun_highscore)
			draw_set_color(c_green);
		draw_text(_ww - _space_x, _draw_y, _total);
	}
	if (timers[26] == 0)
	{
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_text(_space_x, _draw_y + 3 * _offset_y, GetText(53));
		draw_set_halign(fa_right);
		draw_set_color(c_green);
		draw_text(_ww - _space_x, _draw_y + 3 * _offset_y, _best);
	}
		
	draw_set_halign(fa_center);
	var _col = merge_color(c_white, c_gray, 0.5 + 0.5 * sin(get_timer()/1000000));
	if (total_time == global.speedrun_highscore && timers[27] == 0)
		draw_text_ext_transformed_color(_ww/2, _draw_y + 7 * _offset_y, GetText(54), 1.5, 20000, scale, scale, angle, _col, _col, _col, _col, image_alpha);	
		
	draw_set_color(c_white);
	draw_set_alpha(alpha_screen);		
	draw_rectangle(0, 0, room_width, room_height, false);
	draw_set_alpha(1);		
	alpha_screen = max(0, alpha_screen - 0.02);
}