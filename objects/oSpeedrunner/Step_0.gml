if (room == rMenu)
{
	instance_destroy();	
}


switch (state)
{
	case SpeedrunnerStates.RECORD:
		//Total time
		total_time = 0;
		for (var i = 0; i < array_length(end_time); ++i)
		{
			total_time += end_time[i];	
		}

		if (instance_exists(oPlayer) && oPlayer.state == PlayerStates.VICTORY) {}
		else	total_time += (current_time - start_time[array_length(start_time) - 1]);

		if (record)
		{
			record = false;
			end_time[array_length(end_time)] = current_time - start_time[array_length(end_time)];
		}


		//Room time
		if (instance_exists(oPlayer) && oPlayer.state == PlayerStates.VICTORY) 
			room_time = end_time[array_length(end_time) - 1];
		else
			room_time = current_time - start_time[array_length(start_time) - 1];
			
		break;
		
	case SpeedrunnerStates.SHOW_RESULTS:
	
		angle = angle_max * sin(get_timer()/250000);
		scale = scale_avr + scale_bounce * sin(get_timer()/500000);	
	
		if (room != rSpeedrunResults)	break;
		else
		{
			//Animation
			
			if (timers[index] == 0)
			{
				if (index < 27) 
					audio_play_sound(soSpeedrunLevels, 0, false);	
				if (index == 27 && total_time == global.speedrun_highscore)
					audio_play_sound(soSpeedrunLevels, 0, false);
				
				index = min(array_length(timers) - 1, index + 1);
				
			}
			else
			{
				timers[index]--;	
			}
			
			if (keyboard_check_pressed(vk_escape) || keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_space) ||
					gamepad_button_check_pressed(SelectGamepad(0), gp_start) || gamepad_button_check_pressed(SelectGamepad(0), gp_select) || 
						gamepad_button_check_pressed(SelectGamepad(0), gp_face1))
			{
				if (timers[28] == 0)
				{
					for (var i = 0; i < array_length(end_time); ++i)
					{
						if (global.speedrun_levels[i] == -1 || global.speedrun_levels[i] > end_time[i])
						{
							global.speedrun_levels[i] = end_time[i];	
						}
					}
					SaveGame();
					room_goto(rEnd);	
				}
				else
				{
					for (var i = 0; i <= 28; ++i)
					{
						timers[i] = 0;	
						index = 28;
						audio_play_sound(soSpeedrunLevels, 0, false);	
					}
				}
			}
		}
		break;

}