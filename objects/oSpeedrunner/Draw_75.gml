if (room == rMenu) exit;

_wh = window_get_height();
_ww = window_get_width();

switch (state)
{
	case SpeedrunnerStates.RECORD:
		with (oPause)
		{
			if (paused)
			{
				draw_set_alpha(1);	
			}
			else
			{
				draw_set_alpha(0.6);	
			}
		}

		draw_set_color(c_black);

		draw_set_font(fontTitle);

		draw_rectangle(_ww - 9 * string_width("0") - 14, 0, _ww, string_height("0") * 1.6, false);

		draw_set_alpha(1);

		draw_set_color(c_white);
		DrawTimer(total_time, 2);
		
		if (global.speedrun_levels[GetNumberFromLevel(room) - 1] == -1 || global.speedrun_levels[GetNumberFromLevel(room) - 1] > room_time)
		{
			draw_set_color(c_green);	
		}
		else
		{
			draw_set_color(c_red);	
		}
		
		DrawTimer(room_time, 34);
		
		break;
		
	case SpeedrunnerStates.SHOW_RESULTS:
		
	
		break;
}
