/// @description

event_inherited();

if (appear_alpha > 0)
{
	draw_set_alpha(1 - appear_alpha);
}

draw_self();


if (appear_alpha > 0)
{
	draw_set_alpha(1);
		
	gpu_set_blendmode(bm_add);
	draw_sprite_ext(sAppear, 0, x + sprite_width/2, y + sprite_height/2, 1 + 2 * appear_alpha, 1 + 2 * appear_alpha, 0, c_red, 1);
	gpu_set_blendmode(bm_normal);
	
	appear_alpha -= appear_alpha_dec;
		
}


if (label != "")
{
	draw_set_color(c_white);
	draw_set_font(fntText);
	draw_set_valign(fa_center);
	draw_set_halign(fa_middle);
	draw_text((bbox_left + bbox_right)/2, (bbox_top + bbox_bottom)/2, label);
}