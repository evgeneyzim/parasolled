/// @description

event_inherited();

enum ButtonState
{
	DISABLED,
	ENABLED,
};

image_speed = 0;
image_index = ButtonState.DISABLED;

functions = [];
deactivators = [];
length = 0;

linked_block = instance_place(x, y, oBlock);
with (linked_block)  button = other.id;


//Animate
animate = false;

//Appearing
appear_alpha = 0;
appear_alpha_dec = 0.02;

u_strength = shader_get_uniform(shdBrightness, "strength");

//immortal
immortal = false;


//Can be pressed more than once
repeatable = false;
timer_active_button = 0;

//label
label = "";