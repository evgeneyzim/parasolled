if (animate)
{
	if (OnView(id))
	{
		animate = false;
		with (instance_create_layer(x + sprite_width/2, y + sprite_height/2, "Particles", oPlayerCreateCircle))
		{
			color = c_red;
		}
		appear_alpha = 1;
	}

}

if (repeatable)
{
	if (image_index == ButtonState.ENABLED)
	{
		if (timer_active_button == 0)
		{
			timer_active_button = 40;
		}
	
		timer_active_button = max(0, timer_active_button - 1);
		if (timer_active_button == 0)
		{
			image_index = ButtonState.DISABLED;	
		}
	}
}

