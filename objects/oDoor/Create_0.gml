event_inherited();

enum Directions
{
	LEFT, 
	RIGHT, 
	UP, 
	DOWN, 
};

u_strength = shader_get_uniform(shdBrightness, "strength");

start = false; //the state when the room restarts
opened_by = 0;
direct = Directions.LEFT;
spd = 1; //the speed of moving
