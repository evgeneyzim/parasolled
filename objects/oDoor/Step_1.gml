//Opening and closing

opened_by = clamp(opened_by, 0, length);

if (start)
{
	switch(direct)
	{
		case Directions.LEFT:
			if (opened_by + spd < length) 
			{
				x -= spd;
			}
			else
			{
				x -= length - opened_by;	
			}
			opened_by += spd;
			break;
			
		case Directions.RIGHT:
			if (opened_by + spd < length)
			{
				x += spd;
			}
			else
			{
				x += length - opened_by;	
			}
			opened_by += spd;
			break;
			
		case Directions.UP:
			if (opened_by + spd < length)
			{
				y -= spd;
			}
			else
			{
				y -= length - opened_by;	
			}
			opened_by += spd;
			break;
			
		case Directions.DOWN:
			if (opened_by + spd < length)
			{
				y += spd;
			}
			else
			{
				y += length - opened_by;	
			}
			opened_by += spd;
			break;
		
		default:
			Raise("Problems with the direction of the door");
			break;	
	}
}
else
{
	switch(direct)
	{
		case Directions.LEFT:
			if (opened_by - spd > 0) 
			{
				x += spd;
			}
			else
			{
				x += opened_by;	
			}
			opened_by -= spd;
			break;
			
		case Directions.RIGHT:
			if (opened_by - spd > 0)
			{
				x -= spd;
			}
			else
			{
				x -= opened_by;	
			}
			opened_by -= spd;
			break;
			
		case Directions.UP:
			if (opened_by - spd > 0)
			{
				y += spd;
			}
			else
			{
				y += opened_by;	
			}
			opened_by -= spd;
			break;
			
		case Directions.DOWN:
			if (opened_by - spd > 0)
			{
				y -= spd;
			}
			else
			{
				y -= opened_by;	
			}
			opened_by -= spd;
			break;
		
		default:
			Raise("Problems with the direction of the door");
			break;	
	}
}


//Killing the player
if (place_meeting(x, y, oPlayer) && start)
{
	switch (direct)
	{
		case Directions.LEFT:
			oPlayer.x -= spd;
			
			with (oPlayer)
			{
				if (position_meeting(bbox_left, y, oBlock))
				{
					audio_play_sound(soDying, 1, 0);
					instance_destroy();
				}	
			}
			
			break;
			
		case Directions.RIGHT:
			oPlayer.x += spd;
			
			with (oPlayer)
			{
				if (position_meeting(bbox_right, y, oBlock))
				{
					audio_play_sound(soDying, 1, 0);
					instance_destroy();
				}	
			}
			
			break;
			
		case Directions.UP:
			oPlayer.y -= spd;
			
			with (oPlayer)
			{
				if (position_meeting(x, bbox_top, oBlock))
				{
					audio_play_sound(soDying, 1, 0);
					instance_destroy();
				}	
			}
			
			break;
			
		case Directions.DOWN:
			oPlayer.y += spd;
			
			with (oPlayer)
			{
				if (position_meeting(x, bbox_bottom, oBlock))
				{
					audio_play_sound(soDying, 1, 0);
					instance_destroy();
				}	
			}
			
			break;
		
		default:
			Raise("Unreachable code");
			break;
	}
}



