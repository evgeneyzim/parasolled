/// @description

// Inherit the parent event
event_inherited();

draw_self();

gpu_set_blendmode(bm_add);
shader_set(shdExtra);
shader_set_uniform_f(u_blur_vector, 1, 0);
shader_set_uniform_f(u_sigma, 0.5);
draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, 0.4);
shader_reset();
gpu_set_blendmode(bm_normal);