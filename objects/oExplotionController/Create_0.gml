CreatePartType = function(sprite, blend, min_life, max_life, min_scale, max_scale, scaling)
{
	var type = part_type_create();
	part_type_alpha2(type, .75, .0);	
	part_type_sprite(type, sprite, false, true, false);
	part_type_blend(type, blend);
	part_type_size(type, min_scale, max_scale, scaling, 0);
	part_type_life(type, min_life, max_life);
	part_type_orientation(type, 0, 360, 0, 0, 0);
	
	return type;
}


explotion_center_part = CreatePartType(sExplotionCenter, true, 30, 30, .6, .8, -.005);
explotion_particle_part = CreatePartType(sExplotion, true, 15, 20, 2, 4, -.01);
smoke_particle_part = CreatePartType(sExplotionSmoke, true, 15, 15, 2, 4, -.01);