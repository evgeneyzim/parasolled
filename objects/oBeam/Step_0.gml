/// @description
if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	
	trail = oPartTypes.beam_trail;
}


image_angle = direction;
image_xscale = 2;
image_yscale = 2;

if (place_meeting(x, y, oBlock) || OutsideRoom())
{
	instance_destroy();
}

