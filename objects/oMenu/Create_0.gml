//The alpha of title drawing


enum MenuPosition
{
	TITLE,
	MAIN,
	GAME,
	OPTIONS,
	LEVELS,
};

new_game_conf = false;
new_game_position = 0;

rect_width = 560;
rect_height = 300;

tmp_rect_width = 0;
tmp_rect_height = 0;


speedrun_message = false;

speedrun_result = false;
speedrun_result_position = 0;


position = MenuPosition.TITLE;
cursor = 0;

need_to_center = true;	//center the window on the next frame

//    0    1     2     3     4     5
xs = [640, 640,  1920, 640,  1920];
ys = [360, 1080, 1080, 1800, 1840, 1958];

//Title screen
strings_title = 
[
	0
];

title_alpha = 0; 
copy_alpha = 0;

//Main menu
strings_main = 
[
	1, 
	2,
	3
];


//Options
if (gamepad_is_connected(SelectGamepad(0)))
{
	strings_options = 
	[
		4,
		5,
		6,
		39,
		7,
		8
	];

	gamepad_option = 3;
	language_option = 4;
	back_option = 5;
}
else
{
	strings_options = 
	[
		4,
		5,
		6,
		7,
		8
	];

	gamepad_option = -1;
	language_option = 3;
	back_option = 4;	
}


languages = [9, 10];
language_cursor = 0;

//Set resolutions
resolutionW = [1024, 1280, 1920, "Fullscreen"];
resolutionH = [576, 720, 1080, "Fullscreen"];

//Set special cursors
cursor_resolution = 0;
cursor_levels = 0;


//Game Menu
strings_game = 
[
	11,
	12,
	13,
	48,
	8
];



//For options
angle = 0;
angle_max = 3.5;
scale = 1.2;
scale_avr = 1.2;
scale_bounce = 0.05;


GetTime = function(_time)
{
	var _milliseconds_total = _time % 1000;
	var _seconds_total = _time div 1000;
	var _minutes_total = _seconds_total div 60;

	_seconds_total %= 60;
	
	var _milliseconds_total_text;
	var _seconds_total_text;
	var _minutes_total_text;

	if (_milliseconds_total < 10)
	{
		_milliseconds_total_text = "00" + string(_milliseconds_total);
	}
	else if (_milliseconds_total < 100)
	{
		_milliseconds_total_text = "0" + string(_milliseconds_total);
	}
	else
	{
		_milliseconds_total_text = string(_milliseconds_total);
	}


	if (_seconds_total < 10)
	{
		_seconds_total_text = "0" + string(_seconds_total);
	}
	else
	{
		_seconds_total_text = string(_seconds_total);
	}

	if (_minutes_total < 10)
	{
		_minutes_total_text = "0" + string(_minutes_total);
	}
	else
	{
		_minutes_total_text = string(_minutes_total);
	}
	
	var _total_text = _minutes_total_text + ":" + _seconds_total_text + "." + _milliseconds_total_text;
	return _total_text;
}

DrawTime = function(_x, _y, _time)
{	
	var _total_text = GetTime(_time);
	
	draw_text(_x, _y, _total_text);
}