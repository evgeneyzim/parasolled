var _offset = 64; //space between lines

//Settings
draw_set_halign(1);
draw_set_valign(1);
draw_set_font(fntTextMenu);
draw_set_color(c_white);

//TITLE SCREEN
title_alpha = min(title_alpha + 0.002, 1);

var _title_sprite = sTitlePlaceholder;
draw_sprite_ext(_title_sprite, 0, xs[0], ys[0], 1, 1, sin(current_time/360), c_white, title_alpha);


draw_set_valign(fa_bottom);
draw_set_halign(fa_right);
draw_set_font(fntText);
draw_set_alpha(0.3 * copy_alpha);

var _x0 = xs[0] + oCamera.view_w_half - 5;
var _y0 = ys[0] + oCamera.view_h_half;
var _str = "©2020 Evgeney Zimin";

draw_set_color(c_black);
draw_roundrect(_x0 - string_width(_str) - 3, _y0 - string_height(_str) - 3, _x0, _y0, false);

draw_set_color(c_white);
draw_set_alpha(1.0 * copy_alpha);
draw_text(_x0, _y0, _str);
draw_set_halign(1);
draw_set_valign(1);
	
draw_set_alpha(1);
	
if (position == MenuPosition.TITLE)
{
	copy_alpha = min(1, copy_alpha + 0.005);
}
else
{
	copy_alpha = max(0, copy_alpha - 0.05);	
}

//Draw text
draw_set_font(fntMenu);
if (global.language == "Russian")	draw_set_font(fontTitle);
draw_text(xs[0], ys[0] + sprite_get_height(_title_sprite)/2 + _offset, GetText(strings_title[0]));


//MAIN MENU
draw_set_font(fontTitle);
var _strings_main = strings_main;

for (var i = 0; i < array_length(_strings_main); ++i)
{
	_strings_main[i] = GetText(_strings_main[i]);
}

DrawMenu(_strings_main, cursor, xs[1], ys[1], position == MenuPosition.MAIN);


//GAME MENU

var _strings_game = strings_game;

for (var i = 0; i < array_length(_strings_game); ++i)
{
	_strings_game[i] = GetText(_strings_game[i]);
}

DrawMenu(_strings_game, cursor, xs[2], ys[2], position == MenuPosition.GAME, new_game_conf, speedrun_message || speedrun_result);


//OPTIONS MENU
var _strings_options = strings_options;

for (var i = 0; i < array_length(_strings_options); ++i)
{
	_strings_options[i] = GetText(_strings_options[i]);
}

if (resolutionW[cursor_resolution] == "Fullscreen")
{
	_strings_options[0] += GetText(14);
}
else
{
	_strings_options[0] += "\n" + string(resolutionW[cursor_resolution]) + ":" + string(resolutionH[cursor_resolution]) + "\n";
}

_strings_options[1] += " " + string(round(global.music_volume * 200)) + "%";
_strings_options[2] += " " + string(round(global.sfx_volume * 100)) + "%";

var _num;
if (global.vibration)	_num = 40;
else					_num = 41;

if (gamepad_is_connected(SelectGamepad(0)))
	_strings_options[3] += " " + GetText(_num);
_strings_options[language_option] += " " + GetText(languages[language_cursor]);

DrawMenu(_strings_options, cursor, xs[3], ys[3] + 40, position == MenuPosition.OPTIONS);

//LEVELS
draw_set_color(c_white);
draw_set_font(fntTextMenu);
DrawLevelMenu(xs[MenuPosition.LEVELS] - 1280/2, ys[MenuPosition.LEVELS] - 720/2 + 40, cursor_levels);


//NEW GAME CONF
if (new_game_conf || speedrun_message || speedrun_result)
{
	draw_set_color(c_black);
	draw_set_alpha(0.75);
	
	draw_roundrect(xs[position] - tmp_rect_width/2, ys[position] - tmp_rect_height/2, xs[position] + tmp_rect_width/2, ys[position] + tmp_rect_height/2, false);
	
	draw_set_alpha(1.0);
	
	if (new_game_conf)
	{
		draw_set_font(fontTitle);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_set_color(c_white);
		
		draw_text(xs[2], ys[2] - 60, GetText(43));
	
		if (new_game_position == 0)
		{
			draw_set_color(c_white);
			draw_text_ext_transformed(xs[2] - 80, ys[2] + 80, GetText(44), font_get_size(fontTitle) * 1.5, 2000, scale, scale, angle);	
		}
		else
		{
			draw_set_color(merge_color(c_white, c_gray, 0.9));
			draw_text(xs[2] - 80, ys[2] + 80, GetText(44));
		}
	
		if (new_game_position == 1)
		{
			draw_set_color(c_white);
			draw_text_ext_transformed(xs[2] + 80, ys[2] + 80, GetText(45), font_get_size(fontTitle) * 1.5, 2000, scale, scale, angle);
		}
		else
		{
			draw_set_color(merge_color(c_white, c_gray, 0.9));
			draw_text(xs[2] + 80, ys[2] + 80, GetText(45));	
		}
		
	}
	
	else if (speedrun_result)
	{
		draw_set_font(fontTitle);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_set_color(c_white);
		
		draw_text(xs[2], ys[2] - 80, GetText(60));
		if (global.speedrun_highscore > 0)  DrawTime(xs[2], ys[2] - 40, global.speedrun_highscore);
		else								draw_text(xs[2], ys[2] - 40, "??:??.???");
		draw_text(xs[2], ys[2], GetText(61));
	
		if (speedrun_result_position == 0)
		{
			draw_set_color(c_white);
			draw_text_ext_transformed(xs[2] - 80, ys[2] + 80, GetText(44), font_get_size(fontTitle) * 1.5, 2000, scale, scale, angle);	
		}
		else
		{
			draw_set_color(merge_color(c_white, c_gray, 0.9));
			draw_text(xs[2] - 80, ys[2] + 80, GetText(44));
		}
	
		if (speedrun_result_position == 1)
		{
			draw_set_color(c_white);
			draw_text_ext_transformed(xs[2] + 80, ys[2] + 80, GetText(45), font_get_size(fontTitle) * 1.5, 2000, scale, scale, angle);
		}
		else
		{
			draw_set_color(merge_color(c_white, c_gray, 0.9));
			draw_text(xs[2] + 80, ys[2] + 80, GetText(45));	
		}
		
	}
	
	else if (speedrun_message)
	{
		draw_set_font(fontTitle);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_set_color(c_white);
		
		draw_text(xs[2], ys[2] - 40, GetText(49));
		
		draw_text_ext_transformed(xs[2], ys[2] + 60, GetText(50), 1.5, 2000, scale, scale, angle);
	}
}


