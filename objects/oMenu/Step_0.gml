//Music
with (oDeeJay)
{
	bgm = musSpacey;	
}

//Rect
if (new_game_conf || speedrun_message || speedrun_result)
{
	tmp_rect_width = lerp(tmp_rect_width, rect_width, 0.5);	
	tmp_rect_height = lerp(tmp_rect_height, rect_height, 0.5);	
}
else
{
	tmp_rect_width = lerp(tmp_rect_width, 0, 0.5);	
	tmp_rect_height = lerp(tmp_rect_height, 0, 0.5);		
}

//Switch options
//Options
if (gamepad_is_connected(SelectGamepad(0)))
{
	strings_options = 
	[
		4,
		5,
		6,
		39,
		7,
		8
	];

	gamepad_option = 3;
	language_option = 4;
	back_option = 5;
}
else
{
	strings_options = 
	[
		4,
		5,
		6,
		7,
		8
	];

	gamepad_option = -1;
	language_option = 3;
	back_option = 4;	
}

//Input
var _up = keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padu);
var _down = keyboard_check_pressed(vk_down) || keyboard_check_pressed(ord("S")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padd);
var _left = keyboard_check_pressed(vk_left) || keyboard_check_pressed(ord("A")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padl);
var _right = keyboard_check_pressed(vk_right) || keyboard_check_pressed(ord("D")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padr);
var _select = keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(SelectGamepad(0), gp_face1);


var _gam = SelectGamepad(0);
if (gamepad_is_connected(_gam))
{
	input = new GamepadInput(_gam);
	
	_left |= input.left;
	_right |= input.right;
	_up |= input.up;
	_down |= input.down;
	
	delete input;
}

//Positioning
x = xs[position];
y = ys[position];

if (position == MenuPosition.LEVELS && cursor_levels > 19)
{
	y = ys[position + 1];	
}

//Centering the window
if (need_to_center)
{
	window_center();	
	need_to_center = false;
}

//Actions
switch (position)
{
	case MenuPosition.TITLE:
		if ((keyboard_check_pressed(vk_anykey) && keyboard_key != vk_escape) || GamepadAnykey(SelectGamepad(0)))
		{
			position++;
			cursor = 0;
		}
		if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_select))
		{
			instance_create_layer(0, 0, "Control", oInvokeGameEnd);	
		}
		break;
		
	case MenuPosition.MAIN:
	
		if (_up || _down)	audio_play_sound(soMenuClick, 1, 0);	
	
		cursor += (_down - _up);
		
		if (cursor < 0)									cursor = array_length(strings_main) - 1;
		if (cursor > array_length(strings_main) - 1)	cursor = 0;
		
		if (_select)
		{
			audio_play_sound(soMenuClick, 1, 0);
			switch (cursor)
			{
				case 0:
					position = MenuPosition.GAME;
					cursor = 1;
					if (global.new_game)
					{
						cursor = 0;	
					}
					break;
					
				case 1:
					position = MenuPosition.OPTIONS;
					cursor = 0;
					
					if (global.language == "English")
						language_cursor = 0;
					else
						language_cursor = 1;
					
					if (window_get_fullscreen())
					{
						cursor_resolution = 3;	
					}
					else
					{
						switch (display_get_gui_width())
						{
							case 1024:
								cursor_resolution = 0;
								break;
							case 1280:
								cursor_resolution = 1;
								break;
							case 1920:
								cursor_resolution = 2;
								break;
							default:
								Raise("Cursor_resolution error");
								break;
						}
					}
					break;
					
				case 2:
					game_end();
					break;
				
				default:
					Raise("Cursor error at Main section");
					break;
			}
		}
		
		//Step back
		if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_select) || gamepad_button_check_pressed(SelectGamepad(0), gp_face2))
			position = MenuPosition.TITLE;
		
		break;
		
	case MenuPosition.GAME:
		
		if (!speedrun_result && !speedrun_message && !new_game_conf && (_up || _down))		audio_play_sound(soMenuClick, 1, 0);	
		
		if (!speedrun_result && !new_game_conf && !speedrun_message)						cursor += (_down - _up);
		else if (new_game_conf)											new_game_position = clamp(new_game_position + _right - _left, 0, 1);
		else if (speedrun_result)										speedrun_result_position = clamp(speedrun_result_position + _right - _left, 0, 1);
		
		
		if (cursor < 0)									cursor = array_length(strings_game) - 1;
		if (cursor > array_length(strings_game) - 1)	cursor = 0;
		
		
		if (_select && !new_game_conf && !speedrun_result)
		{
			audio_play_sound(soMenuClick, 1, 0);
			switch(cursor)
			{
				case 0:	
					if (global.new_game)
					{
						global.progress = 0;
						SaveGame();
						RestartParticles();
						room_goto(ROOM_START);
					}
					else
					{
						new_game_conf = true;
						new_game_position = 1;
					}
					exit;
					break;
				
				case 1:
					var _room = LevelMap(global.progress);
					RestartParticles();
					if (GetNumberFromLevel(_room) > 3)
					{
						with (oDeeJay)
						{
							bgm = musMus;	
						}
					}
					room_goto(_room);
					break;
					
				case 2:
					position = MenuPosition.LEVELS;
					cursor = 0;
					cursor_levels = 0;
					break;
					
				case 3:
				
					if (global.progress < NUMBER_OF_LEVELS)
					{
						speedrun_message = speedrun_message xor 1;	
					}
					else
					{
						speedrun_result = true;
						speedrun_result_position = 0;
					}
					exit;
					break;
					
				case 4:
					position = MenuPosition.MAIN;
					cursor = 0;
					break;
				
				default:
					Raise("Cursor error at Game section");
					break;
			}
		}
		
		if (_select && new_game_conf)
		{
			switch (new_game_position)
			{
				case 0:
					global.progress = 0;
					SaveGame();
					RestartParticles();
					room_goto(ROOM_START);
					break;
				
				case 1:
					new_game_conf = false;
					break;
				
				default:
					Raise("New game problem google it\n");
					break;
			}
		}
		
		if (_select && speedrun_result)
		{
			switch (speedrun_result_position)
			{
				case 0:
					RestartParticles();
					instance_create_layer(0, 0, "Control", oSpeedrunner);
					room_goto(ROOM_START);
					break;
				
				case 1:
					speedrun_result = false;
					break;
				
				default:
					Raise("Speedrun result problem google it\n");
					break;
			}
		}
		
		//Step back
		if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_select) || gamepad_button_check_pressed(SelectGamepad(0), gp_face2))
		{
			new_game_conf = false;
			speedrun_message = false;
			speedrun_result = false;
			position = MenuPosition.MAIN;
			cursor = 0;
		}
		
		break;	
		
	case MenuPosition.OPTIONS:
		
		if (_up || _down)	audio_play_sound(soMenuClick, 1, 0);	
		
		cursor += (_down - _up);
		
		if (cursor < 0)									cursor = array_length(strings_options) - 1;
		if (cursor > array_length(strings_options) - 1)	cursor = 0;
		
		if (cursor == 0)
		{
			cursor_resolution = clamp(cursor_resolution + _right - _left, 0, array_length(resolutionW) - 1);
			if (_left || _right)
				audio_play_sound(soMenuClick, 1, 0);	
		}
		else
		{
			if (window_get_fullscreen())
			{
				cursor_resolution = 3;	
			}
			else
			{
				switch (display_get_gui_width())
				{
					case 1024:
						cursor_resolution = 0;
						break;
					case 1280:
						cursor_resolution = 1;
						break;
					case 1920:
						cursor_resolution = 2;
						break;
					default:
						Raise("Cursor_resolution error");
						break;
				}
			}
		}
		
		
		
		if (cursor == 1)
		{			
			if (_left || _right)	audio_play_sound(soMenuClick, 1, 0);		
			global.music_volume = clamp(global.music_volume + (_right - _left) * 0.05, 0, 0.5);
			SetGain(GetMusic(), global.music_volume);
		}
		
		if (cursor == 2)
		{			
			if (_left || _right)	audio_play_sound(soMenuClick, 1, 0);		
			global.sfx_volume = clamp(global.sfx_volume + (_right - _left) * 0.1, 0, 1);
			SetGain(GetSFX(), global.sfx_volume);
		}
				
		if (cursor == language_option)
		{
			if (_left || _right)	audio_play_sound(soMenuClick, 1, 0);
			
			language_cursor += (_right - _left);
			
			if (language_cursor >= 2)	language_cursor = 0;
			if (language_cursor < 0)	language_cursor = 1;
		}
		else
		{
			if (global.language == "English")
				language_cursor = 0;
			else
				language_cursor = 1;
		}
		
		if (_select)
		{
			switch (cursor)
			{
				case 0:	
				
					audio_play_sound(soMenuClick, 1, 0);	
				
					var _res_w = resolutionW[cursor_resolution];
					var _res_h = resolutionH[cursor_resolution];
						
					global.resolution_width = _res_w;
					global.resolution_height = _res_h;
						
					if (_res_w == "Fullscreen")
					{
						var _w = display_get_width();
						var _h = display_get_height();
						
						display_set_gui_size(_w, _h);	
						window_set_size(_w, _h);
						surface_resize(application_surface, _w, _h);
						window_set_fullscreen(true);	
					}
					else
					{
						if (_res_w == 1920)
						{
							global.invoke_fullscreen = 10;
							global.invoke_1920 = 15;	
						}
						
						window_set_fullscreen(false);
						display_set_gui_size(_res_w, _res_h);	
						window_set_size(_res_w, _res_h);
						surface_resize(application_surface, _res_w, _res_h);
					
						need_to_center = true;
					}
									
					//Add saving
					break;
					
				//Sounds, do nothing
				case 1:
					break;
				case 2:
					break;
					
				case gamepad_option:
					//Gamepad vibration
					global.vibration = global.vibration xor 1;
					if (global.vibration && gamepad_is_connected(SelectGamepad(0)))
					{
						gamepad_set_vibration(SelectGamepad(0), 10, 10);
						alarm[0] = room_speed/4;
					}
					break;
					
				case language_option:
					// Confirm language selection	
					
					var _lang;
					switch (language_cursor)
					{
						case 0:
							_lang = "English";
							break;
						case 1:
							_lang = "Russian";
							break;
						default:
							Raise("Language error");
					}
					
					if (global.language == _lang) break;
					global.language = _lang;
					SaveGame();
					
					break;
					
				case back_option:
					audio_play_sound(soMenuClick, 1, 0);	
					position = MenuPosition.MAIN;
					cursor = 0;
					SaveGame();
					break;
			}
		}
		
		//Step back
		if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_select) || gamepad_button_check_pressed(SelectGamepad(0), gp_face2))
		{
			position = MenuPosition.MAIN;
			cursor = 0;
			SaveGame();
		}
		
		break;
		
	case MenuPosition.LEVELS:
		if (_up || _down)	audio_play_sound(soMenuClick, 1, 0);
		if (_left || _right)	audio_play_sound(soMenuClick, 1, 0);
		
		if (_select) audio_play_sound(soMenuClick, 1, 0);
		
		cursor_levels += (_right - _left) + _down * 5 - _up * 5;
		cursor_levels = clamp(clamp(cursor_levels, 0, global.progress + 1), 0, NUMBER_OF_LEVELS);
		
		
		if (_select)
		{
			if (cursor_levels == 0)
			{
				position = MenuPosition.GAME;
				cursor = 1;
				if (global.new_game)
				{
					cursor = 0;	
				}
			}
			else
			{
				if (cursor_levels > 3)
				{
					with (oDeeJay)
					{
						bgm = musMus;	
					}
				}
				RestartParticles();
				room_goto(LevelMap(cursor_levels - 1));
			}
		}
		
		//Step back
		if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_select) || gamepad_button_check_pressed(SelectGamepad(0), gp_face2))
		{
			position = MenuPosition.GAME;
			cursor = 1;
			if (global.new_game)
			{
				cursor = 0;	
			}
		}
		break;
		
	default:
		Raise("Something is wrong with menu");
		break;
}

//Waving
angle = angle_max * sin(get_timer()/250000);
scale = scale_avr + scale_bounce * sin(get_timer()/500000);	