{
  "spriteId": {
    "name": "sSpecial",
    "path": "sprites/sSpecial/sSpecial.yy",
  },
  "solid": true,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "oWallType",
    "path": "objects/oWallType/oWallType.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 0,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"oSpecialWall","path":"objects/oSpecialWall/oSpecialWall.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":8,"collisionObjectId":null,"parent":{"name":"oSpecialWall","path":"objects/oSpecialWall/oSpecialWall.yy",},"resourceVersion":"1.0","name":null,"tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Static Objects",
    "path": "folders/Objects/Static Objects.yy",
  },
  "resourceVersion": "1.0",
  "name": "oSpecialWall",
  "tags": [],
  "resourceType": "GMObject",
}