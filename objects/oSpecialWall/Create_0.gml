/// @description Set parametres
event_inherited();

randomize();

magnitude = 500000;
phase = get_timer();
u_strength = shader_get_uniform(shdBrightness, "strength");

sprite_index = sBlock;