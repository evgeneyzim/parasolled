
draw_set_alpha(alpha_screen);
draw_set_color(c_black);
draw_rectangle(0, 0, room_width, room_height, false);
draw_set_alpha(1);


if (left < room_width/4 && right > 3*room_width/4)
{
	
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_alpha(the_end_alpha);
	
	var _center_x = (right + left)/2;
	
	switch (index)
	{
		case 0:
			draw_set_font(fntFinal);
			draw_set_color(c_white);
			
			draw_text(_center_x, room_height/2 - 60, GetText(66));
			
			break;
		
		case 1:
			draw_set_font(fontTitle);
			draw_set_color(c_white);
			
			draw_text(_center_x, room_height/2 - 60, GetText(67));
			
			draw_set_color(c_gray);
			
			var _text;
			if (global.language == "Russian")
			{
				_text = "Евгений Зимин";	
			}
			else
			{
				_text = "Evgeney Zimin";
			}
			
			draw_text(_center_x, room_height/2 + 20, _text);
			
			draw_set_color(merge_color(c_gray, c_black, 0.5));
			draw_text(_center_x, room_height/2 + 80, "@EvgeneyZimin");
			
			break;
		
		case 2:
			draw_set_font(fontTitle);
			draw_set_color(c_white);
			
			draw_text(_center_x, room_height/2 - 60, GetText(68));
			
			draw_set_color(c_gray);
			draw_text(_center_x, room_height/2 + 20, "Kyron Finder");
			
			draw_set_color(merge_color(c_gray, c_black, 0.5));
			draw_text(_center_x, room_height/2 + 80, "@kyronfinder");
			
			break;
			
		case 3:
			draw_set_font(fontTitle);
			draw_set_color(c_white);
			
			draw_text(_center_x, room_height/2 - 60, GetText(69));
			
			var _text1;
			if (global.language == "Russian")
			{
				_text1 = "Никита Покусаев";	
			}
			else
			{
				_text1 = "Nikita Pokusaev";
			}
			
			var _text2;
			if (global.language == "Russian")
			{
				_text2 = "Даниил Белый";	
			}
			else
			{
				_text2 = "Daniil Belyy";
			}
			
			draw_set_color(c_gray);
			draw_text(_center_x, room_height/2 + 20, "Maxwell Cuddlesworth");
			draw_text(_center_x, room_height/2 + 70, _text1);
			draw_text(_center_x, room_height/2 + 120, _text2);
			
			break;
			
		case 4:
		
			draw_set_font(fontTitle);
			draw_set_color(c_white);
			
			draw_text(_center_x, room_height/2 - 60, GetText(70));
			
			draw_set_color(c_gray);
			draw_text(_center_x, room_height/2 + 20, "Shaun Spalding");
			
			draw_set_color(merge_color(c_gray, c_black, 0.5));
			draw_text(_center_x, room_height/2 + 80, "@shaunspalding");
			
			break;
			
			
		
		default:
			break;
	}
	
	draw_set_alpha(1);
	

}
