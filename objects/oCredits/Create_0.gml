alpha = 0.8;

left = room_width/2;
right = room_width/2;

left_max = room_width/6;
right_max = 5 * room_width/6;

the_end_alpha = 0;
the_end_alpha_inc = 0.005;

reached_one = false;

index = 0;

index_max = 5;

alpha_screen = 1;
alpha_screen_dec = 0.001;

with (oDeeJay)
{
	bgm = musSpacey;	
}