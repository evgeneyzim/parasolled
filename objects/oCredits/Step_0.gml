if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_start))
{
	RestartParticles();
	room_goto(rMenu);	
}

left = lerp(left, left_max, 0.1);
right = lerp(right, right_max, 0.1);

if (!reached_one && left < room_width/4 && right > 3*room_width/4)
{	
	the_end_alpha = min(1, the_end_alpha + the_end_alpha_inc);
	if (the_end_alpha == 1) reached_one = true;
}

if (reached_one)
{
	the_end_alpha = max(0, the_end_alpha - the_end_alpha_inc);
	if (the_end_alpha == 0)
	{
		index++;
		reached_one = false;
	}
}

with(oPlayer) blocked = true;
oCamera.cursor_block = true;
if (index >= index_max)
{
	alpha_screen = max(0, alpha_screen - alpha_screen_dec);	
	if (alpha_screen < 0.8)
	{
		oCamera.cursor_block = false;
		with(oPlayer) blocked = false;
		with (inst_15E2D425)
		{
			active = true;	
		}
	}
	
	with (oDeeJay)
	{
		bgm = musSilence;	
		musNext = -1;
	}
}