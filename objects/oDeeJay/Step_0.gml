if ((bgm == musSilence || bgm == musBossIntro) && musNext != -1)
{
	silence_timer--;
	if (silence_timer <= 0)
	{
		bgm = musNext;	
	}
}

if(audio_get_name(bgm_playing) != audio_get_name(bgm))
{
	if (last_bgm != -1)
	{
		audio_stop_sound(last_bgm);		
	}
	
	audio_sound_gain(bgm_playing, 0, 1000);
	last_bgm = bgm_playing;
	
	var _music_stuff = GetMusicInfo(bgm);
	bgm_playing = audio_play_sound(bgm, 100, 0);
	intro_point = _music_stuff[0];
	end_point = _music_stuff[1];
	
	
	
	if (audio_get_name(bgm) != musSpacey)
		audio_sound_gain(bgm, global.music_volume, 0);
	else
		audio_sound_gain(bgm, global.music_volume * 0.7, 0);
}

if (last_bgm != -1)
{
	if (audio_sound_get_gain(last_bgm) == 0)
	{
		audio_stop_sound(last_bgm);
		last_bgm = -1;
	}
}

if (audio_sound_get_track_position(bgm_playing) >= end_point)
	audio_sound_set_track_position(bgm_playing, audio_sound_get_track_position(bgm_playing) - (end_point - intro_point));