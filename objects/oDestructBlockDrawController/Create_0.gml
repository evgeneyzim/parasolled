surface_init = false;

surface_mask = -1;
surface_cut = -1;
surface = -1;

draw_x = -1;
draw_y = -1;



magnitude = 1600000;
phase = get_timer();
u_strength = shader_get_uniform(shdBrightness, "strength");

if (room != rGuards)
{
	TestFunc = function(_y)
	{
		return false;
	}
}
else
{
	if (y < room_height/2)
	{
		TestFunc = function(_y)
		{
			return (_y < room_height/2);	
		}
	}
	else
	{
		TestFunc = function(_y)
		{
			return (_y > room_height/2);	
		}	
	}
}