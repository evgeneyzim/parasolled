if (!surface_init || !surface_exists(surface))
{
	surface_init = true;
	
	if (surface != -1 && surface_exists(surface))
	{
		surface_free(surface);	
	}
	
	
	var _left = noone;
	var _right = noone;
	var _up = noone;
	var _down = noone;
	
	with (oDestructBlock)
	{
		if (room != rGuards || other.TestFunc(y))
		{
			if (_left == noone || _left > bbox_left)
			{
				_left = bbox_left;
			}
			if (_right == noone || _right < bbox_right)
			{
				_right = bbox_right;
			}
			if (_up == noone || _up > bbox_top)
			{
				_up = bbox_top;
			}
			if (_down == noone || _down < bbox_bottom)
			{
				_down = bbox_bottom;
			}
		}
		
	}
	
	_left -= 8;
	_up -= 8;
	_down += 8;
	_right += 8;
	
	draw_x = _left;
	draw_y = _up;
	
	var _w = _right - _left;
	var _h = _down - _up;
	
	surface_mask = surface_create(_w, _h);
	
	//Draw body
	surface_set_target(surface_mask);

	
	with (oDestructBlock)
	{
		draw_sprite_ext(sDestructBlockOld, 0, x - other.draw_x, y - other.draw_y, image_xscale, image_yscale, 0, c_white, 1);
	}
	
	surface_reset_target();
	
	//Draw cut mask
	surface_cut = surface_create(_w, _h);
	
	surface_set_target(surface_cut);
	
	draw_set_color(c_white);
	
	draw_rectangle(0, 0, _w, _h, false);
	
	gpu_set_blendmode(bm_subtract);
	
	draw_surface(surface_mask, 0, 0);
		
	gpu_set_blendmode(bm_normal);
	
	surface_reset_target();
	
	//draw surface
	
	surface = surface_create(_w, _h);
	
	surface_set_target(surface);
	
	var _scale = 2;
	
	var _spr_w = sprite_get_width(sGroundTexture) * _scale;
	var _spr_h = sprite_get_height(sGroundTexture) * _scale;
	
	var _x_off = draw_x % _spr_w;
	var _y_off = draw_y % _spr_h;
	
	for (var i = -_x_off; i < _w; i += _spr_w)
	{
		for (var j = -_y_off; j < _h; j += _spr_h)
		{
			draw_sprite_ext(sGroundTexture, 0, i, j, _scale, _scale, 0, c_white, 1);
		}
	}
	
	gpu_set_blendmode(bm_subtract);
	
	draw_surface(surface_cut, 0, 0);
		
	gpu_set_blendmode(bm_normal);
	
	surface_reset_target();
	
	
	surface_free(surface_mask);
	surface_free(surface_cut);
	
}



/*if (!init)
{
  if (surface_exists(block_surface))  surface_free(block_surface);
  init = true;
  
  if (left == undefined)
  {
    left = room_width;
    right = 0;
    top = room_height;
    bottom = 0;

    for (var i = 0; i < instance_number(oDestructBlock); ++i)
    {
      var _block = instance_find(oDestructBlock, i);
      if (_block.bbox_left < left)  left = _block.bbox_left;
      if (_block.bbox_right > right)  right = _block.bbox_right;
      if (_block.bbox_top < top)  top = _block.bbox_top;
      if (_block.bbox_bottom > bottom)  bottom = _block.bbox_bottom;
    }

	left -= 7;
	right += 7;
	top -= 7;
	bottom += 7;

    w = right - left;
    h = bottom - top;
  }

  block_surface = surface_create(w, h); 
}