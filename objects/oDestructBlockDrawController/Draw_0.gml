if (surface != -1 && surface_exists(surface))
{
	var _strength = 0.15 * abs(sin((get_timer() - phase)/magnitude));
	shader_set(shdBrightness);
	shader_set_uniform_f(u_strength, _strength);
	
	draw_surface(surface, draw_x, draw_y);	
	
	shader_reset();
	
	with (oDestructBlock)
	{
		if (hp < maxhp)   draw_sprite(sScratch, 0, x + draw_x, y + draw_y);  
	}
}

