/// @description

if (!part_init && oPartTypes.part_init)
{
	part_init = false;
	
	trail_smoke = oPartTypes.parasol_trail;
	death = oPartTypes.parasol_death;
}

//Slow Motion
thrown_speed = 30 * global.slow_motion_effect;


draw_alpha = 0.5 + 0.5 * (state == ParasolStates.CLOSED);


recoil += sign(0 - recoil);
switch (state)
{
	
	case ParasolStates.START:
		//Waiting to be picked up
		
		if (!init_start)
		{
			x_start = x + lengthdir_x(PARASOL_WIDTH, image_angle);	
			y_start = y + lengthdir_y(PARASOL_WIDTH, image_angle);
			start_angle = image_angle;
			init_start = true;
		}
		
		sprite_index = sParasolMask;
		image_speed = 0;
		
		if (trying_to_pick)
		{
			trying_to_pick = false;
			amplitude_start = 10;
			phase = get_timer();
		}
		
		image_angle = start_angle + sin((phase - get_timer())/10000) * amplitude_start;
		
		//Position the parasol
		x = x_start - lengthdir_x(PARASOL_WIDTH + sin((phase - get_timer())/10000) * amplitude_start/5, image_angle);
		y = y_start - lengthdir_y(PARASOL_WIDTH + sin((phase - get_timer())/10000) * amplitude_start/5, image_angle);
		
		amplitude_start = max(0, amplitude_start - 0.5);
		
		
		break;
	
	case ParasolStates.CLOSED:
		//Following mouse
		
		immortal = false;
		
		if (recoil == 0)
		{
			if (sprite_index == sParasolOpen || sprite_index == sParasolOpenMask)
			{
				image_speed = -1;
				if (image_index + image_speed <= 0)
				{
					image_speed = 0;
					image_index = 0;
					sprite_index = sParasolClosed;
				}
			}
		}
		else          
		{
			sprite_index = sParasolMask;
			
			if (recoil >= 2 * recoil_max/4 && place_meeting(x, y, oButton))
			{
				var _button = instance_place(x, y, oButton);
				with (_button)
				{
					if (image_index == ButtonState.DISABLED && (!repeatable || timer_active_button == 0))
					{
						image_index = ButtonState.ENABLED;
						audio_play_sound(soButtonActivate, 1, 0);
						for (var i = 0; i < array_length(functions); ++i)
						{
							var _func = functions[i]
							_func();	
						}
					}
				}
			}
		}
		
		x = host.xprevious;
		y = host.yprevious;
		
		if (global.input_type == InputType.KEYBOARD)  desired_angle = point_direction(x, y, mouse_x, mouse_y);
		else										  
		{
			var _hor = gamepad_axis_value(SelectGamepad(0), gp_axisrh);
			var _ver = gamepad_axis_value(SelectGamepad(0), gp_axisrv);
				
			if (abs(_hor) > 0.2 || abs(_ver) > 0.2)
			{
				desired_angle = point_direction(0, 0, _hor, _ver);	
				desired_angle_prev = desired_angle;
				with (oGun)
				{
					desired_angle = other.desired_angle;	
				}
			}
			else
			{
				desired_angle = desired_angle_prev;
			}
		}
		
		
		var _dumping = desired_angle - image_angle;
		if (_dumping > 0)
		{
			if (360 - _dumping < _dumping)  _dumping = -(360 - _dumping);	
		}
		else
		{
			var _tmp_dumping = -_dumping;
			if (360 - _tmp_dumping < _tmp_dumping)  _tmp_dumping = -(360 - _tmp_dumping);	
			_dumping = -_tmp_dumping;
		}
			
		image_angle += _dumping / 3 * global.slow_motion_effect;
		
		x = x + lengthdir_x(recoil, image_angle);
		y = y + lengthdir_y(recoil, image_angle);
		
		break;
			
	case ParasolStates.OPEN:
		//Rotation depends on player`s movements
		sprite_index = sParasolOpen;
		
		image_speed = 1;
		if (image_index + image_speed >= image_number)
		{
			image_speed = 0;
			image_index = image_number - 1;
		}
		
		x = host.x;
		y = host.y;
		
		res_xspeed += host.xspeed;
		res_yspeed += host.yspeed;
		
		if (res_xspeed == 0 && res_yspeed == 0)  
		{
			desired_angle = round(image_angle / 180) * 180;
		}
		else  desired_angle = (darctan2(res_yspeed, -res_xspeed) + 360) mod 360;
			
		var _dumping = desired_angle - image_angle;
		if (_dumping > 0)
		{
			if (360 - _dumping < _dumping)  _dumping = -(360 - _dumping);	
		}
		else
		{
			var _tmp_dumping = -_dumping;
			if (360 - _tmp_dumping < _tmp_dumping)  _tmp_dumping = -(360 - _tmp_dumping);	
			_dumping = -_tmp_dumping;
		}
			
		image_angle += _dumping / 5 * global.slow_motion_effect;
		
		break;
		
	case ParasolStates.THROWN:
		//Move as spear
		ParasolThrown();	
		break;
		
	case ParasolStates.RETURNING:
		//Return to the player
		ParasolReturn();
		break;
		
	
	case ParasolStates.SHIELD:
		sprite_index = sParasolOpenMask;
		
		image_speed = 1;
		if (image_index + image_speed >= image_number)
		{
			image_speed = 0;
			image_index = image_number - 1;
		}
		
		x = host.xprevious;
		y = host.yprevious;
		
		if (global.input_type == InputType.KEYBOARD)  desired_angle = point_direction(x, y, mouse_x, mouse_y);
		else										  
		{
			var _hor = gamepad_axis_value(SelectGamepad(0), gp_axislh);
			var _ver = gamepad_axis_value(SelectGamepad(0), gp_axislv);
				
			if (abs(_hor) > 0.2 || abs(_ver) > 0.2)
			{
				desired_angle = point_direction(0, 0, _hor, _ver);	
				desired_angle_prev = desired_angle;
			}
			else
			{
				desired_angle = desired_angle_prev;
			}
		}
		
		var _dumping = desired_angle - image_angle;
		if (_dumping > 0)
		{
			if (360 - _dumping < _dumping)  _dumping = -(360 - _dumping);	
		}
		else
		{
			var _tmp_dumping = -_dumping;
			if (360 - _tmp_dumping < _tmp_dumping)  _tmp_dumping = -(360 - _tmp_dumping);	
			_dumping = -_tmp_dumping;
		}
			
		image_angle += _dumping / 3 * global.slow_motion_effect;
		
	
		break;	
		
	default:
		Raise("Wrong parasol state\n");
		break;
}
	

//Angle & Scale
image_angle = (image_angle + 360) mod 360;
