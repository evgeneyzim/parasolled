/// @description

event_inherited();

enum ParasolStates
{
	START,
	OPEN,
	CLOSED,
	THROWN,
	RETURNING,
	SHIELD
};

desired_angle = 0;
desired_angle_prev = 0;
host = noone;

state = ParasolStates.START;

can_be_picked_up_max = 5;
can_be_picked_up = 0;

//Start
trying_to_pick = false;
start_angle = image_angle;
amplitude_start = 0;
phase = 0;

x_start = 0;
y_start = 0;

init_start = false;

//Open
air_res = 0.3;
res_xspeed = 0;
res_yspeed = 0;

//Stabbing
recoil = 0;
recoil_max = 32;

//Shooting
moving = false;
thrown_speed = 30;
xspeed = 0;
yspeed = 0;


linked_block = undefined;
additional = 0;

energy = 0;

damage_list = ds_list_create();

//Returning
rotation_speed = 100;

//Mouse locating
draw_alpha = 0.5;

//Appearing
appear_alpha = 1;
appear_alpha_dec = 0.02;

u_strength = shader_get_uniform(shdBrightness, "strength");

immortal = false;

part_init = false;

