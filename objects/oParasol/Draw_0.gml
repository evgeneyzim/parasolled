//Depth
if (state == ParasolStates.START || (state == ParasolStates.THROWN && (abs(xspeed) + abs(yspeed) == 0))) 
{	    
	if (layer_exists("DestructBlocks"))		depth = layer_get_depth("DestructBlocks") + 1;
	else									depth = layer_get_depth("Blocks") + 1;
}
else 
{						        
	depth = layer_get_depth("Parasol");
}

//Appearing
if (appear_alpha > 0)
{
	shader_set(shdBrightness);
	shader_set_uniform_f(u_strength, appear_alpha);

	
	appear_alpha -= appear_alpha_dec;
	draw_set_alpha(1 - appear_alpha);
}


draw_self();

if (appear_alpha > 0)
{
	draw_set_alpha(1);
	
	gpu_set_blendmode(bm_add);
	draw_sprite_ext(sAppear, 0, x + lengthdir_x(PARASOL_WIDTH/2, image_angle), y + lengthdir_y(PARASOL_WIDTH/2, image_angle), 1 + 2 * appear_alpha, 1 + 2 * appear_alpha, 0, oDrawer.c_parasol, 1);
	gpu_set_blendmode(bm_normal);
		
}




gpu_set_blendmode(bm_add);
shader_set(shdExtra);
shader_set_uniform_f(u_blur_vector, 1, 0);
shader_set_uniform_f(u_sigma, 0.1);
draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha - 0.5);
shader_reset();
gpu_set_blendmode(bm_normal);


