if (textNum == 23)
{
	with (oParasol)
	{
		if (state == ParasolStates.THROWN && !moving)
		{
			other.textNum = 64;	
		}
	}
}
else if (textNum == 64)
{
	with (oParasol)
	{
		if (state == ParasolStates.CLOSED)
		{
			other.textNum = 23;	
		}
	}
}