/// @description

if (gamepad_dependant)
{
	if (global.input_type == InputType.GAMEPAD)
		myText = GetText(textNum + 1);	
	else
		myText = GetText(textNum);	
}
else
{
	myText = GetText(textNum);	
}



var _strength = (abs(sin((get_timer() - phase)/magnitude)) + 0.5) * 0.1;
shader_set(shdBrightness);
shader_set_uniform_f(u_strength, _strength);

draw_set_halign(fa_center);
draw_set_valign(fa_top);
draw_set_font(myFont);
var orig = make_color_rgb(255,240,255);
var clr = merge_color(c_gray, c_black, 0.5);

var _orig = merge_color(orig, c_black, 0.5);
var _clr = merge_color(clr, c_black, 0.5);
draw_set_alpha(alpha/10);
draw_text_ext_color(leftSide + sprite_width/2 - 1,y,myText,2*fontHeight,rightSide - leftSide, _orig, _orig, _clr, _clr, alpha);
draw_text_ext_color(leftSide + sprite_width/2 + 1,y,myText,2*fontHeight,rightSide - leftSide, _orig, _orig, _clr, _clr, alpha);
draw_text_ext_color(leftSide + sprite_width/2,y,myText,2*fontHeight - 1,rightSide - leftSide, _orig, _orig, _clr, _clr, alpha);
draw_text_ext_color(leftSide + sprite_width/2,y,myText,2*fontHeight + 1,rightSide - leftSide, _orig, _orig, _clr, _clr, alpha);
draw_set_alpha(1);


draw_text_ext_color(leftSide + sprite_width/2,y,myText,2*fontHeight,rightSide - leftSide, orig, orig, clr, clr, alpha);
draw_set_color(c_white);  
alpha = lerp(alpha, 1, 0.01);

shader_reset();

