myFont = fntText;
myText = "You forgot to initialise the text";

if (global.language == "Russian")
{
	myFont = fontText;	
}

textNum = -1;
gamepad_dependant = false;

alpha = 0;

leftSide = bbox_left;
rightSide = bbox_right;

fontHeight = font_get_size(myFont);

magnitude = 1000000;
phase = get_timer();
u_strength = shader_get_uniform(shdBrightness, "strength");