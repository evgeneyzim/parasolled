
with (oDeeJay)
{
	bgm = musPrologue;	
}

if (dark_surface == -1)
{
	//Dark surface init
	dark_surface = surface_create(room_width + 2 * buffer, room_height + 2 * buffer);
}

if (surface_exists(dark_surface))
{
	//Fill the surface
	surface_set_target(dark_surface);

	draw_clear_alpha(c_black, alpha);

	gpu_set_blendmode(bm_subtract);

	with (oSpotlight)
	{
		var _xscale = image_xscale + sqr(dsin(current_time / 6)) / 6;
		var _yscale = image_yscale + sqr(dsin(current_time / 6)) / 6;
	
		draw_sprite_ext(sprite_index, 0, x + other.buffer, y + other.buffer, _xscale, _yscale, image_angle, image_blend, image_alpha);
	}

	with (oHeavenLight)
	{
		var _xscale = image_xscale + sqr(dsin(current_time / 6)) / 12;
		var _yscale = image_yscale;
	
		draw_sprite_ext(sprite_index, 0, x + other.buffer, y + other.buffer, _xscale, _yscale, image_angle, image_blend, image_alpha);
	}

	gpu_set_blendmode(bm_normal);

	surface_reset_target();	
}

