//Draw dark surface
if (dark_surface != -1)
{
	if (surface_exists(dark_surface))
		draw_surface(dark_surface, -buffer, -buffer);
	else
	{
		dark_surface = surface_create(room_width + 2 * buffer, room_height + 2 * buffer);		
	}
}