with (instance_place(x, y, oPlayer))
{	
	if (sqr(xspeed) > sqr(other.max_spd))
	{
		for (var i = 0; i < array_length(other.functions); ++i)
		{
			other.functions[i]();
		}
		other.alarm[0] = 1.5 * 60;
	}
}
