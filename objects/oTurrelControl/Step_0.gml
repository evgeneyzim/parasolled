if (active)
{
	timer_turrels--;

	if (timer_turrels == 0)
	{
		timer_turrels = timer_max_turrels;
		var _turrel = turrels[irandom_range(0, array_length(turrels) - 1)];
		with (_turrel)
		{
			instant_shoot = true;	
		}
	}
}