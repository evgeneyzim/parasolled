/// @description

if (!done)
{
	if (portal_summoner && squares_len == 0 && !instance_exists(oPortal))
	{
		instance_create_layer(portal_x, portal_y, "PortalBlocks", oPortal);
		oCamera.follow = oPortal;
		oCamera.timer_special = oCamera.timer_special_max;
		done = true;
	}
	else if (squares_len == 0)
	{
		functions[0]();	
		done = true;
	}
}