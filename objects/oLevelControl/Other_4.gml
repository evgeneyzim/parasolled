/// @description Set timers

//Set marks invisible
if (layer_exists(layer_get_id("Marks")))
	layer_set_visible("Marks", false);

if (room == rMenu)					instance_destroy(oPause);
else if (!instance_exists(oPause))  instance_create_layer(x, y, "Control", oPause);

//Timer till the restart
restart_room_max = 30;
restart_room = restart_room_max;

//Set objects position.
RestartLevel(false, true);