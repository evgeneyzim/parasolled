/// @description Level ending control

global.grav = 0.7 * sqr(global.slow_motion_effect);

if (need_to_center)
{
	need_to_center = false;
	window_center();
}

//Do not draw cursor
window_set_cursor(cr_none);

//Skip levels
if (keyboard_check_pressed(vk_alt)) 
{
	with (oSpeedrunner)	record = true;
	RestartParticles();
	room_goto_next();
}

//if (keyboard_check_pressed(vk_shift))
//{
//	screen_save("Parasol" + string(current_time) + ".png");
//}

//Restart the room
if (restart_room == 0)
{
	RestartLevel(true, false);
	exit;
}
restart_room--;

if (instance_exists(oPlayer))  restart_room = restart_room_max;

//Reduce the glitch intensity
global.glitch_intensity = max(0, global.glitch_intensity - 0.05);

//Invoke fullscreen
if (global.invoke_fullscreen > 0)
{
	global.invoke_fullscreen--;
	if (global.invoke_fullscreen == 0)
	{
		global.resolution_width = "Fullscreen";
		global.resolution_height = "Fullscreen";
		var _w = display_get_width();
		var _h = display_get_height();
						
		display_set_gui_size(_w, _h);	
		window_set_size(_w, _h);
		surface_resize(application_surface, _w, _h);
		window_set_fullscreen(true);	
	}
}

//Invoke 1920x1080
if (global.invoke_1920 > 0)
{
	global.invoke_1920--;
	if (global.invoke_1920 == 0)
	{
		global.resolution_width = 1920;
		global.resolution_height = 1080;
		
		var _res_w = 1920;
		var _res_h = 1080;
		
		window_set_fullscreen(false);
		display_set_gui_size(_res_w, _res_h);	
		window_set_size(_res_w, _res_h);
		surface_resize(application_surface, _res_w, _res_h);
					
		need_to_center = true;
	}
}

//Deactivating
with (oBlock)
{
	if (object_index != oCube && object_index != oDestructBlock && object_index != oDoor && object_index != oButtonPress && object_index != oGuard)
		instance_deactivate_object(id);
}

//Acivating
with (oCamera)
{
	instance_activate_region(x - view_w_half, y - view_h_half, 2 * view_w_half, 2 * view_h_half, true);
}

with (oParasol)
{
	instance_activate_region(x - 4 * PARASOL_WIDTH, y - 4 * PARASOL_WIDTH, 8 * PARASOL_WIDTH, 8 * PARASOL_WIDTH, true);
}

//Slow motion

global.slow_motion = max(0, global.slow_motion - 1);
if (global.slow_motion == 0)
{
	global.slow_motion_effect = min(NORMAL_EFFECT, global.slow_motion_effect * 1.2);
}
else
{
	global.slow_motion_effect = max(SLOW_EFFECT, global.slow_motion_effect * 0.8);
}

