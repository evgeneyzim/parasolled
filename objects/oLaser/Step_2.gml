var len = point_distance(x, y, xprevious, yprevious);
var ang = image_angle;
var _x = xprevious;
var _y = yprevious;
	
for (var i = 0; i < len; ++i)
{	
	part_particles_create(global.part_system, _x + lengthdir_x(i, ang), _y + lengthdir_y(i, ang), trail, 1);
}

//deactivation
if (distance_to_object(oCamera) > oCamera.view_w_half * 2) instance_deactivate_object(id);	