if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	trail = oPartTypes.beam_trail;
	explotion = oPartTypes.laser_explotion;
}

image_angle = direction;
speed = spd;

instance_activate_region(x - abs(sprite_width)/2, y - abs(sprite_height)/2, abs(sprite_width), abs(sprite_height), true);

with (instance_place(x, y, oDestructBlock))
{
	hp -= 1;	
	other.destroy_it = true;
}

with (instance_place(x, y, oBlock))
{
	if (other.banned_block != id)
		instance_destroy(other.id);
}

//If prevented by parasol-shield
with (instance_place(x, y, oParasol))
{
	if (state == ParasolStates.SHIELD)	
	{
		other.destroy_it = true;
	}
}