var c_boss = merge_color(c_red, c_purple, 0.3 + (1/2 - 1/2 * sin(get_timer()/1000000)) * 0.7);

c_boss = merge_color(c_boss, c_black, 0.3);

draw_sprite_ext(sFinalBossBeam, 0, x, y, __scale, scale, image_angle, oDrawer.c_spikes, image_alpha);
draw_sprite_ext(sFinalBossBeam, 1, x, y, __scale, scale, image_angle, c_boss, image_alpha);
gpu_set_blendmode(bm_subtract);
draw_sprite_ext(sFinalBoss, 3, x, y, scale, scale, 0, c_white, image_alpha);
gpu_set_blendmode(bm_normal);

draw_sprite_ext(sFinalBoss, 0, x, y, scale, scale, 0, c_boss, image_alpha);
draw_sprite_ext(sFinalBoss, 1, x, y, scale, scale, 0, c_boss, image_alpha);
draw_sprite_ext(sFinalBoss, 2, x, y, scale, scale, 0, c_white, image_alpha);

