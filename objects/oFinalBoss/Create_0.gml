enum FinalBossStates
{
	START,
	ROTATE,
	CHASE,
};

state = FinalBossStates.START;

rotation_speed = 0.5;

image_speed = 0.0;

image_index = 0;

spd = 1.4;

scale = 1.5;
__scale = 0;
acceleration = 0.05;