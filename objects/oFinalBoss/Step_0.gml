
if (state == FinalBossStates.ROTATE)
{
	image_angle += rotation_speed;
	__scale = lerp(__scale, scale, acceleration);
	image_xscale = __scale;
}
else
{
	__scale = lerp(__scale, 0, acceleration);
	if (__scale < 0.01)
	{
		image_angle = 0;	
	}
}	

if (instance_exists(oPlayer) && state == FinalBossStates.CHASE && oCamera.timer_special == 0)
{
	image_index = 2;
	var _dir = point_direction(x, y, oPlayer.x, oPlayer.y);
	
	x += lengthdir_x(spd, _dir);
	y += lengthdir_y(spd, _dir);
}