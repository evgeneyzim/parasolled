/// @description

instance_activate_region(bbox_left - sprite_width, bbox_top - sprite_height, sprite_width * 3, sprite_height * 3, true);

//Moving
with (place_meeting(x + lengthdir_x(spd,dir),y+lengthdir_y(spd,dir),oBlock))
{
	if (object_index != oButtonPress)
	{
		other.dir = (other.dir + 180) % 360; 	
	}
}

x += lengthdir_x(spd * global.slow_motion_effect,dir);
y += lengthdir_y(spd * global.slow_motion_effect,dir);


image_angle = (image_angle + 7 * global.slow_motion_effect) % 360;


//Saving position
if (time_offset <= 0)
{
	for (var i = amount - 1; i > 0; --i)
	{
		prev_positions_x[i] = prev_positions_x[i - 1];	
		prev_positions_y[i] = prev_positions_y[i - 1];	
		prev_rotations[i]   = prev_rotations[i - 1];	
	}
	
	prev_positions_x[0] = x;
	prev_positions_y[0] = y;
	prev_rotations[0] = image_angle;
	
	time_offset = time_offset_max;
} else time_offset -= 1 * global.slow_motion_effect;