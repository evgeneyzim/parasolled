/// @description

/*
0 - outline
1 - mask
2 - gradient
3 - normal
*/

image_speed = 0;
image_index = 3;

dir = 0;
spd = 0;


u_blur_vector = shader_get_uniform(shdExtra, "blur_vector");
u_sigma = shader_get_uniform(shdExtra, "sigma");

amount = 5;

prev_positions_x = [];
prev_positions_y = [];
prev_rotations   = [];

for (var i = 0; i < amount; ++i)
{
	prev_positions_x[i] = x;
	prev_positions_y[i] = y;
	prev_rotations[i] = image_angle;	
}

time_offset_max = 1;
time_offset = time_offset_max;

saved_spd = 0;
