/// @description

//Draw prevoius
if (spd > 0) for (var i = amount - 1; i > 0; --i)
{
	draw_sprite_ext(sprite_index, 3, prev_positions_x[i], prev_positions_y[i], image_xscale, image_yscale, prev_rotations[i], image_blend, (amount - i)/amount * 0.5);	
}

draw_self();

//event_inherited();

draw_self();
gpu_set_blendmode(bm_add);
shader_set(shdExtra);
shader_set_uniform_f(u_blur_vector, 1, 0);
shader_set_uniform_f(u_sigma, 0.5);
draw_sprite_ext(sprite_index, 3, x, y, image_xscale, image_yscale, image_angle, image_blend, 0.4);
shader_reset();
gpu_set_blendmode(bm_normal);
