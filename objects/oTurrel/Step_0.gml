if (state == TurrelStates.ATTACK)
{
	not_so_long = true;
}
else
{
	not_so_long = false;	
}

just_shoot = max(0, just_shoot - 1);

state = TurrelStates.FREE;

my_x = x - sprite_width/2;
my_y = y - sprite_height/2;

//Searching for player
if (OnView(id) && instance_exists(oPlayer) && start)
{
	
	var _xdiff = oPlayer.x - my_x;
	var _ydiff = oPlayer.y - my_y;
	
	if (predict_)
	{
		var n = 25;
		var _list = predict(oPlayer, n);
		
		var _pl_x = ds_list_find_value(_list, n * 2 - 2);
		var _pl_y = ds_list_find_value(_list, n * 2 - 1);
		
		
		_xdiff = _pl_x - my_x;
		_ydiff = _pl_y - my_y;
		
		ds_list_destroy(_list);
	}
		
	_xdiff *= -image_xscale;
	_ydiff *= image_yscale;

	if ((_xdiff > 0 && _ydiff > 0) || (_ydiff > 0 && sprite_index == sTurrelDouble))
	{
		target_angle = point_direction(oPlayer.x, oPlayer.y, x, y);
		state = TurrelStates.ATTACK;
		if (!not_so_long)
			timer_shoot = timer_max_shoot;		
	}
}

//Choose angle
switch (state)
{
	case TurrelStates.ATTACK:
		//Rotating
		if (sprite_index == sTurrelDouble)
		{
			if (image_yscale > 0)
			{
				target_angle = clamp(target_angle, 0, 180);
			}
			else
			{	
				target_angle = clamp(target_angle, 180, 360);	
			}
		}
		else if (image_xscale > 0 && image_yscale > 0)
		{
			target_angle = clamp(target_angle, 0, 90);
		}
		else if (image_xscale < 0 && image_yscale > 0)
		{
			target_angle = clamp(target_angle, 90, 180);
		}
		else if (image_xscale < 0 && image_yscale < 0)
		{
			target_angle = clamp(target_angle, 180, 270);
		}
		else if (image_xscale > 0 && image_yscale < 0)
		{
			if (target_angle < 0)	target_angle += 360;
			target_angle = clamp(target_angle, 270, 360);
		}
		
		//Attacking
		if (!dependant)	timer_shoot--;
		
		if (timer_shoot == 0 || instant_shoot)
		{
			instant_shoot = false;
			timer_shoot = timer_max_shoot;	
			just_shoot = just_shoot_max;
			with (instance_create_layer(x + lengthdir_x(sprite_get_width(sTurrelGun) - 12, 180 + gun_angle), y + lengthdir_y(sprite_get_width(sTurrelGun) - 12, 180 + gun_angle), "Bullets", oLaser))
			{
				audio_play_sound(soTurretShoot, 0, false);
				direction = 180 + other.gun_angle;
				image_xscale = 2;
				image_yscale = 2;
				image_angle = direction;
				spd = other.laser_speed;
			}
		}
		break;
		
	case TurrelStates.FREE:
		if (sprite_index == sTurrelDouble)
		{
			if (image_yscale > 0)
			{
				target_angle = 90 + 2 * amplitude * sin(get_timer()/period + phase);
			}
			else
			{	
				target_angle = 360 + 2 * amplitude * sin(get_timer()/period + phase);	
			}
		}
		else if (image_xscale > 0 && image_yscale > 0)
		{
			target_angle = 45 + amplitude * sin(get_timer()/period + phase);
		}
		else if (image_xscale < 0 && image_yscale > 0)
		{
			target_angle = 135 + amplitude * sin(get_timer()/period + phase);
		}
		else if (image_xscale < 0 && image_yscale < 0)
		{
			target_angle = 225 + amplitude * sin(get_timer()/period + phase);
		}
		else if (image_xscale > 0 && image_yscale < 0)
		{
			target_angle = 315 + amplitude * sin(get_timer()/period + phase);
		}
		break;
	
}

//Lerp the angle
if (just_shoot == 0)
	gun_angle = lerp(gun_angle, target_angle, acceleration);