draw_self();
draw_sprite_ext(sTurrelGun, 0, x, y, 1 - 0.3 * sin(180 * (1 - just_shoot / just_shoot_max)), 1, gun_angle, c_white, 1); //draw gun

if (state == TurrelStates.ATTACK)
{
	var normal_timer = timer_max_shoot div 2;

	if (timer_shoot < normal_timer)
	{
		var muzzle_x = x + lengthdir_x(sprite_get_width(sTurrelGun), 180 + gun_angle);
		var muzzle_y = y + lengthdir_y(sprite_get_width(sTurrelGun), 180 + gun_angle);
	
		draw_set_color(merge_color(c_red, c_white, 0.5));
		draw_set_alpha(0.75);
		draw_circle(muzzle_x, muzzle_y, 16 * timer_shoot / normal_timer, false);
		draw_set_color(c_white);
		draw_set_alpha(1);
	
	}
}