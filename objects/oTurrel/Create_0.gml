gun_angle = 0; //the angular distance to player

target_angle = 0; //the angle to aim

acceleration = 0.1; //the rotation speed

//Explotions
if (!instance_exists(oExplotionController))
{
	instance_create_layer(0, 0, "Control", oExplotionController);	
}


//For free rotation
period = 500000; 
phase = irandom_range(0, period);
amplitude = 45;

//For attack
timer_max_shoot = 1.5 * 60;
timer_shoot = timer_max_shoot;
laser_speed = 40;
predict_ = true;

not_so_long = true;

enum TurrelStates
{
	FREE,
	ATTACK,
};

state = TurrelStates.FREE;

start = true;

just_shoot_max = 12;
just_shoot = 0;

dependant = false; //if controlled by controller :)
instant_shoot = false;

predict = function(instance, steps)
{
    var dsid,posx,posy,hspd,vspd,grvx,grvy,spd;
    dsid = ds_list_create();
    with (instance) {
        posx = instance.x;
        posy = instance.y;
        hspd = instance.xspeed;
        vspd = instance.yspeed;
        grvx = 0;
        grvy = 0;//global.grav;
        repeat (steps) {
            vspd += grvy;
            
			with (instance)
			{
				if (place_meeting(posx + hspd, posy, oBlock))
				{
					repeat (abs(hspd))
					{
						if (place_meeting(posx + sign(hspd), posy, oBlock))	break;
						posx += sign(hspd);
					}
				}
				hspd = 0;
			}
			posx += hspd;
            
			with (instance)
			{
				if (place_meeting(posx, posy + vspd, oBlock))
				{
					repeat (abs(vspd))
					{
						if (place_meeting(posx, posy + sign(vspd), oBlock))	break;
						posy += sign(vspd);
					}
				}
				vspd = 0;
			}
			posy += vspd;
			
			
            ds_list_add(dsid,posx);
            ds_list_add(dsid,posy);
        }
    }
    return dsid;
}
