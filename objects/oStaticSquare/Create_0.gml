/// @description
timer = 1.5;
damage_timer = 0;
hp = 3;

scale = random_range(0.95, 1.05);
max_scale = 1.1;
min_scale = 0.9;
der = choose(-1, 1);

amplitude = 0;
amplitude_max = 20;

last_parasol = noone;

//set particles
death = part_type_create();
part_type_sprite(death, sEnemy, false, false, false)
part_type_size(death, 0.3, 0.5, -0.005, 0);
part_type_color1(death, merge_color(oDrawer.c_squares, c_red, 0.5));
part_type_alpha3(death, 0.1, 0.3, 0.5);
part_type_speed(death, 10, 20, 0, 0);
part_type_direction(death, 0, 359, 0, 0);
part_type_life(death, 60, 60);
part_type_gravity(death, global.grav, 270);


intensity_phase = random_range(0, 360);
seed = random(1);

//shader
u_time = shader_get_uniform(shdGreyScale, "time");