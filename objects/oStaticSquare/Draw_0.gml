/// @description

intensity = 0;


shader_set(shdGreyScale);

var _times;
switch (hp)
{
	case 3:
		_times = 0;
		break;
	case 2:
		_times = 10;
		break;
	case 1:
		_times = 15;
		break;
	default:
		_times = 20;
		break;
}

shader_set_uniform_f(u_time, _times);

draw_sprite_ext(sprite_index, 0, x, y, image_xscale, image_yscale, 0, image_blend, image_alpha);


var _out_width = 4;

draw_set_color(merge_color(c_red, c_black, 0.25));
draw_rectangle(x - sprite_width/2, y - sprite_height/2, x + sprite_width/2, y - sprite_height/2 + _out_width, false);
draw_rectangle(x - sprite_width/2, y + sprite_height/2 + 1, x + sprite_width/2, y + sprite_height/2 - _out_width, false);
draw_rectangle(x - sprite_width/2, y - sprite_height/2, x - sprite_width/2 + _out_width, y + sprite_height/2, false);
draw_rectangle(x + sprite_width/2 - _out_width, y - sprite_height/2, x + sprite_width/2, y + sprite_height/2, false);

draw_set_color(c_red);
draw_rectangle(x - sprite_width/2 + _out_width, y - sprite_height/2 + _out_width, x + sprite_width/2 - _out_width - 1, y - sprite_height/2 + 2 *_out_width, false);
draw_rectangle(x - sprite_width/2 + _out_width, y + sprite_height/2 - _out_width + 1, x + sprite_width/2 - _out_width - 1, y + sprite_height/2 - 2 * _out_width, false);
draw_rectangle(x - sprite_width/2 + _out_width, y - sprite_height/2 + _out_width, x - sprite_width/2 + 2 * _out_width, y + sprite_height/2 - _out_width, false);
draw_rectangle(x + sprite_width/2 - _out_width, y - sprite_height/2 + _out_width, x + sprite_width/2 - 2 *_out_width, y + sprite_height/2 - _out_width, false);

shader_reset();