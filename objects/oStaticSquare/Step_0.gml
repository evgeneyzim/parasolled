/// @description

//Scaling
switch (hp)
{
	case 3:
		min_scale = 1.0;
		max_scale = 1.2;
		break;
	case 2:
		min_scale = 0.8;
		max_scale = 1.0;
		break;
	case 1:
		min_scale = 0.6;
		max_scale = 0.8;
		break;	
		
	default:
		break;
}



//Shaking
x = xstart + irandom_range(-amplitude, amplitude);
y = ystart + irandom_range(-amplitude, amplitude);
amplitude = max(0, amplitude - 1);

//Collision with parasol
var _parasol = instance_place(x, y, oParasol);
with (_parasol)
{
	if (moving && (other.damage_timer == 0 || other.last_parasol != id))
	{
		other.hp--;
		other.damage_timer = other.timer * 60
		other.scale -= 0.5;
		other.amplitude = other.amplitude_max;
		other.last_parasol = id;
		oDrawer.seed = random(1);
				
		if (!audio_is_playing(soSquareHit))
			audio_play_sound(soSquareHit, 1, 0);
		if (!audio_is_playing(soSquareKilled))
			audio_play_sound(soSquareKilled, 1, 0);
	}
}

with (instance_place(x, y, oLaser))
{
	instance_destroy(id);
	other.hp--;
	other.damage_timer = other.timer * 60
	other.scale -= 0.5;
	other.amplitude = other.amplitude_max;
	
	if (!audio_is_playing(soSquareHit))
		audio_play_sound(soSquareHit, 1, 0);
	if (!audio_is_playing(soSquareKilled))
		audio_play_sound(soSquareKilled, 1, 0);
}

//Timer
damage_timer = max(0, damage_timer - 1);

//Squezng
scale += der * 0.01;

if ((der > 0 && scale > max_scale) || (der < 0 && scale < min_scale))  der *= -1;

image_xscale = scale;
image_yscale = scale;


//Dying
if (hp <= 0)  
{
	with (oClusterControl)
	{
		for (var i = 0; i < squares_len; ++i)
		{
			if (squares[i] == other.id)
			{
				for (var j = i; j < squares_len - 1; ++j)
				{
					squares[j] = squares[j + 1];	
				}
			}
		}
		squares_len--;
	}
	
	part_particles_create(global.enemy_part, x, y, death, 10);
	
	instance_destroy();
}