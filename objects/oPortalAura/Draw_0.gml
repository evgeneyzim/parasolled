/// @description
event_inherited();


if (!start)  draw_set_alpha(0.5 + sin((get_timer() - phase)/500000) * 0.2);
else
{
	draw_set_alpha(image_alpha);
	image_alpha += 0.005;
	
	if (image_alpha >= 0.7)
	{
		start = false;
		oCamera.zoom = 1;
		if (instance_exists(oPlayer))  oCamera.follow = oPlayer;
		phase = get_timer() - 500000;
	}
}
draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, c_white, draw_get_alpha());
draw_set_alpha(1);