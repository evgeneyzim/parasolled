/// @description
event_inherited();

phase = get_timer();

start = false;

//Focus camera on portal
if (instance_exists(oClusterControl))
{
	start = true;
	image_alpha = 0;
	with (oCamera)  
	{
		follow = other.id;
		zoom = 0.9;
	}
}