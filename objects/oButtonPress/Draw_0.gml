if (special)
{
	var _alpha;
	
	if (state == ButtonPressStates.ACTIVE)
	{
		_alpha = 0.7;	
	}
	else
	{
		_alpha = 0.3;
	}
	draw_sprite_ext(sArrow, 0, xstart, ystart - 56 + sin(get_timer()/200000) * 14, 3, 3 * sin(get_timer()/100000), -90, merge_color(oDrawer.c_spikes, c_white, 0.5 + 0.3 * sin(get_timer()/100000)), _alpha);	
}

//Draw button
draw_sprite_ext(sprite_index, 1, xstart, y, image_xscale, image_yscale, 0, c_white, image_alpha);
	
//Draw healthbar
draw_healthbar(xstart - 40 * image_xscale, ystart - 13, xstart + 42 * image_xscale, ystart - 3, b_progress, c_blue, c_orange, c_orange, 0, true, false);
	
//Draw body
draw_sprite_ext(sprite_index, 0, xstart, ystart, image_xscale, image_yscale, 0, c_white, image_alpha);
