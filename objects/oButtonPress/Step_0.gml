
switch (state)
{
	case ButtonPressStates.ACTIVE:
		if (place_meeting(xstart, y - 1, oPlayer))
		{
			y = min(ystart, y + spd);
			
			if (y == ystart)
			{
				state = ButtonPressStates.PRESSED;	
			}
		}
		else
		{
			y = max(ystart - dist_max, y - spd);		
		}
		break;

	case ButtonPressStates.PRESSED:
		y = ystart;	
		
		if (place_meeting(xstart, y - 1, oPlayer))
		{
			b_progress = min(b_progress_max, b_progress + load_spd);
			
			if (b_progress == b_progress_max)
			{
				state = ButtonPressStates.INACTIVE;
				
				for (var i = 0; i < array_length(functions); ++i)
				{
					functions[i]();	
				}
				
			}	
		}
		else
		{
			b_progress = max(0, b_progress - unload_spd);
			if (b_progress == 0)
			{
				state = ButtonPressStates.ACTIVE;	
			}
		}
		
		break;
		
	case ButtonPressStates.INACTIVE:
		y = ystart;	
		
		if (state_changer && !place_meeting(xstart, y - 1, oPlayer))
		{
			state = ButtonPressStates.PRESSED;
			functions[0] = function()
			{
				state = ButtonPressStates.PRESSED;	
			}
			state_changer = false;
		}
		
		break;
		
	default:
		Raise("Wrong button press state\n");	
		break;

}
//y = ystart - 18;



//if (!active)
//{
//	image_index = image_number - 1;
//	exit;	
//}



//if (image_index + image_speed <= 0)
//{
//	image_index = 0
//	start = false;
//	image_speed = 0;
//}
//else if (image_index + image_speed >= image_number - 1)
//{
//	start = false;
//	image_speed = 0;
//	active = false;
	
//	for (var i = 0; i < array_length(functions); ++i)
//	{
//		functions[i]();	
//	}
//}

//if (place_meeting(x, y - 20, oPlayer))
//{
//	start = true;
//	image_speed = 1;
//}
//else if (start)
//{
//	image_speed = -1;	
//}


//if (image_speed > 0)
//{
//	if (image_index >= 3)
//	{
//		image_speed = sign(image_speed) * image_speed_start;	
//	}
//	else
//	{
//		image_speed = sign(image_speed);	
//	}
//}
