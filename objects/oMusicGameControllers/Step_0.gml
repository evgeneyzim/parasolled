if (inactive)
{
	read = false;
	exit;
}

if (play)
{	
	if (index < 5)
	{
		play_timer = max(0, play_timer - 1);
		
		if (play_timer == 0)
		{
			play_timer = play_timer_max;
			
			with (buttons[order[index]])
			{
				animate = true;	
			}
			
			PlayNote(index)
			
			index++;
		}
	}
	else
	{
		index = 0;
		play = false;
		with (oPlayer)
		{
			blocked = false;	
		}
	}
}
else
{
	if (read)
	{
		if (data == order[index])
		{
			PlayNote(index);
			index++;
			
			if (index >= 5)
			{
				functions[0]();
				inactive = true;
			}
		}
		else
		{
			audio_play_sound(soMusicError, 0, false);
			with (inst_445E1E92)
			{
				functions[0]();
			}
		}
		read = false;
	}
}