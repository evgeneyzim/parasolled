order = [];

play = false;

play_timer_max = 30;
play_timer = play_timer_max;

index = 0;

buttons[0] = inst_7E045769;
buttons[1] = inst_762169A3;
buttons[2] = inst_FE5AF49;

read = false;
data = 0;

inactive = true;

GetNewOrder = function()
{
	index = 0;
	for (var i = 0; i < 5; ++i)
	{
		do
		{
			order[i] = choose(0, 1, 2);	
		} until (i == 0 || order[i] != order[i - 1]);
	}
}	

PlayNote = function(index)
{
	var _so;
	switch (index)
	{
		default:
		case 0:
		case 4:
			_so = soFirstNote;
			break;
					
		case 1:
		case 3:
			_so = soSecondNote;
			break;
					
		case 2:
			_so = soThirdNote;
			break;
	}
			
	audio_play_sound(_so, 0, false);
}	