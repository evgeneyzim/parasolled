/// @description

with (spotlight)
{
	destroy_it = true;	
}

if (explode)
{
	ShakeScreen(40, 30);
	global.glitch_intensity = 1;
	
	repeat (40)
	{
		with (instance_create_layer(x, y, "Bullets", oPlayerSparkle))
		{
			color = oDrawer.c_player;
		}
	}

	with (instance_create_layer(x, y, "Bullets", oPlayerSparkle))
	{
		speed = 0;
		color = oDrawer.c_player;
		size = 30;
	}

	if (global.input_type == InputType.GAMEPAD && global.vibration)
	{
		gamepad_set_vibration(SelectGamepad(0), 15, 15);
		alarm[0] = room_speed/2;
	}
}

if (parasol != noone)  instance_destroy(parasol);	



audio_stop_sound(slide_sound);	