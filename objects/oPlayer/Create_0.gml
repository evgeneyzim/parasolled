/// @description Initialization

event_inherited();

enum PlayerStates
{
	NORMAL,
	VICTORY,
}

state = PlayerStates.NORMAL;

walkspeed = SHESTE;  
jumpspeed = 14;


xspeed = 0;
yspeed = 0;

prev_xspeed = 0;
prev_yspeed = 0;

//For camera
xspeed_saved = 0;
yspeed_saved = 0;

double_jump = true;

//Unused
max_height = 99999999999;

//The speed of changind the player`s xspeed
acceleration = 0.3;

//If >0, then player is grounded
grounded_counter = 0;
//If >0, then player will jump if able
jump_counter = 0;  
grounded_counter_max = 15;

//Time after parasol was called
parasol_call_counter = 0;
parasol_call_counter_max = 60;


//Weapons
parasol = noone;
gun = noone;

prev_parasol = noone;

just_taken = false;

//For gamepad
pointed_angle = 0;
target_x = 0;
target_y = 0;

//Required for the parasol
mass = 1; 

explode = true;

spotlight = noone;

stagger = 0;
stagger_max = 10;

droplets_timer_max = 0;//delete this
droplets_timer = 0;

//Appearing
appear_alpha = 1;
appear_alpha_dec = 0.02;

//Blocked
blocked = false;

u_strength = shader_get_uniform(shdBrightness, "strength");

instance_create_layer(x, y, "Particles", oPlayerCreateCircle);


//Particles
part_init = false;



//Slide sound
slide_loop_length = 0.25;
slide_sound = audio_play_sound(soSlide, 1, true);
audio_stop_sound(slide_sound);
slide_gain = 0.3;

//Menu
if (room == rMenu)
{
	var _parasol = instance_create_layer(x, y, layer, oParasol);
	_parasol.host = id;
	_parasol.state = ParasolStates.OPEN;
	parasol = _parasol;
}


DrawDuringTheJump = function()
{
	var _angle = darctan2(-yspeed, xspeed);
	var _xscale = 1;
	
	_angle = (_angle + 360) % 360;
	
	
	//if (_angle == 270)	_angle = 90;
	
	if (_angle > 90 && _angle < 270)
	{
		_xscale = -1;
		_angle -= 180;
	}
	draw_sprite_ext(sPlayerJump, image_index, x, y, _xscale, image_yscale, _angle, c_white, image_alpha);
}