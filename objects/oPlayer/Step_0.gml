/// @description Move the player

if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	trail = oPartTypes.player_trail;
}


//activate region around the player
instance_activate_region(x - abs(sprite_width), y - abs(sprite_height), 2 * abs(sprite_width), 2 * abs(sprite_height), true);
instance_activate_region(x - abs(sprite_width) + xspeed, y - abs(sprite_height), 2 * abs(sprite_width), 2 * abs(sprite_height) + yspeed, true);

stagger = max(0, stagger - 1);

if (abs(xspeed) < 0.1 && (parasol == noone || parasol.state != ParasolStates.OPEN))
	xspeed = 0;
	
if (parasol != noone)
{
	prev_parasol = parasol;	
}

//When the player has entered the portal
if (state == PlayerStates.VICTORY)
{
	with (spotlight)
	{
		x = other.x;
		y = other.y;
	}
	
	if (place_meeting(x, y, oParasol) && gun == noone && parasol == noone)
	{
		with (instance_place(x, y, oParasol))
		{
			host = other.id;
			other.parasol = id;
			state = ParasolStates.CLOSED;
			linked_block = undefined;
			audio_play_sound(soParasolReturn, 0, false);
		}
	}
	
	audio_stop_sound(soSlide);
	audio_stop_sound(soDying);
	
	sprite_index = sPlayerIdle;
	image_index = 0;
	image_speed = 0;
	
	x = lerp(x, oPortalAura.x, 0.1 * global.slow_motion_effect);
	y -= 1.3 * global.slow_motion_effect;
	
	image_alpha -= 0.02;
	if (parasol != noone)  parasol.image_alpha = image_alpha;
	if (gun != noone)	   gun.image_alpha = image_alpha;
	
	if (image_alpha == 0) 
	{	
		oDrawer.level_end = true;
	}
	
	if (place_meeting(x, bbox_top, oWall))  y = yprevious;
	
	exit;	
}
	
//Check buttons
var _left = keyboard_check(vk_left) || keyboard_check(ord("A")) || gamepad_axis_value(SelectGamepad(0), gp_axislh) < -0.7;
var _right = keyboard_check(vk_right) || keyboard_check(ord("D")) || gamepad_axis_value(SelectGamepad(0), gp_axislh) > 0.7;
var _up = keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(SelectGamepad(0), gp_face1) || gamepad_button_check_pressed(SelectGamepad(0), gp_stickl);
var _up_press = keyboard_check(vk_up) || keyboard_check(ord("W")) || keyboard_check(vk_space) || gamepad_button_check(SelectGamepad(0), gp_face1) || gamepad_button_check(SelectGamepad(0), gp_stickl);
var _open = mouse_check_button(mb_right) || gamepad_button_check_pressed(SelectGamepad(0), gp_face3) || gamepad_button_check_pressed(SelectGamepad(0), gp_shoulderlb);
var _return = mouse_check_button_pressed(mb_right) || gamepad_button_check_pressed(SelectGamepad(0), gp_face3) || gamepad_button_check_pressed(SelectGamepad(0), gp_shoulderlb);
var _shoot = mouse_check_button_released(mb_left) || gamepad_button_check_pressed(SelectGamepad(0), gp_stickr) || gamepad_button_check_pressed(SelectGamepad(0), gp_shoulderrb); 
var _lock = keyboard_check(vk_down) || keyboard_check(ord("S")) || gamepad_button_check(SelectGamepad(0), gp_face2);
var _close = mouse_check_button_released(mb_right) || gamepad_button_check_released(SelectGamepad(0), gp_face3) || gamepad_button_check_released(SelectGamepad(0), gp_shoulderlb);

if (room == rMenu || blocked)
{
	_left = false;
	_right = false;
	_up = false;
	_up_press = false;
	_open = false;
	_shoot = false;
	_lock = false;
	_close = false;
	_return = false;	
}


//Blocks
if (room != rMenu && room != rEnd)
{
	if (GetNumberFromLevel(room) < GetNumberFromLevel(THROW_ACTIVATE_ROOM) && 
		global.progress < GetNumberFromLevel(THROW_ACTIVATE_ROOM) - 1	)
		{
			_shoot = false;
			_return = false;
		}
	if (GetNumberFromLevel(room) < GetNumberFromLevel(SHIELD_ACTIVATE_ROOM) && 
		global.progress < GetNumberFromLevel(SHIELD_ACTIVATE_ROOM) - 1)
		_lock = false;
}

//(Un)used ability
var _stab = false;

//Set xspeed
var _move = _right - _left;

var _just_opened = false;

if (_return)
{
	if (parasol == noone && prev_parasol != noone && gun == noone)
	{
		parasol_call_counter = parasol_call_counter_max;	
	}
	else if (room != rMenu)
	{
		with (oParasol)
		{
			if (OnView(id))
				audio_play_sound(soUnableToReturnParasol, 0, false);
			trying_to_pick = true;	
		}
	}
}

if (global.input_type == InputType.GAMEPAD)
{
	if (parasol != noone)
	{
		if (_open)	just_taken = true;
		if (_lock)
		{
			var _lh = abs(gamepad_axis_value(SelectGamepad(0), gp_axislh));
			var _lv = abs(gamepad_axis_value(SelectGamepad(0), gp_axislv));
		
			if (_lh > 0.2 || _lv > 0.2)
			{
				just_taken = false;	
			}
		}	
	}
	
	if ((parasol != noone && parasol.state == ParasolStates.CLOSED) || gun != noone)
	{
		var _rh = abs(gamepad_axis_value(SelectGamepad(0), gp_axisrh));
		var _rv = abs(gamepad_axis_value(SelectGamepad(0), gp_axisrv));
		
		if (_rh > 0.2 || _rv > 0.2)
		{
			just_taken = false;	
		}
	}	
}


//Lock the commands
if (_lock && parasol != noone  && grounded_counter == grounded_counter_max)  
{
	_move = 0;
	_up = 0;
	_up_press = 0;
	_open = 0;
	_stab = 0;
	_shoot = 0;
	_close = 0;
	if (parasol.state != ParasolStates.SHIELD)
		audio_play_sound(soParasolOpenShield, 1, false);
	parasol.state = ParasolStates.SHIELD;

}

//Throw gun
if (gun != noone && (_lock || gamepad_button_check_pressed(SelectGamepad(0), gp_shoulderlb)))
{
	gun.throw_gun = true;	
}

xspeed = lerp(xspeed, _move * walkspeed, acceleration);
yspeed += global.grav;

yspeed = sign(yspeed) * min(abs(yspeed), 25);

//Parasol actions
if (parasol != noone)
{
	parasol_call_counter = 0;
	
	//Switch parasol state
	if (_open)
	{
		if (parasol.state == ParasolStates.CLOSED)  
		{
			parasol.state = ParasolStates.OPEN;	
			_just_opened = true;
			audio_play_sound(soParasolOpenAir, 1, false);
		}
	}
	if (_close)
	{
		if (parasol.state == ParasolStates.OPEN)
		{
			parasol.state = ParasolStates.CLOSED;
			audio_play_sound(soParasolClose, 1, false);
		}
	}
	
	//Add air resistance force
	if (parasol.state == ParasolStates.OPEN)
	{		
		var _force, _force_x, _force_y;
		with (parasol)
		{
			_force = air_res * sqrt(res_xspeed * res_xspeed + res_yspeed * res_yspeed);
			_force_x = -_force * dcos(desired_angle);
			_force_y = _force * dsin(desired_angle);
		}
	
		var _xspeed_difference = _force_x / mass;
		var _yspeed_difference = _force_y / mass;
		
		xspeed = xspeed -_xspeed_difference;
		yspeed = yspeed -_yspeed_difference;
		
	}
	
	//Shield
	if (parasol.state == ParasolStates.SHIELD)
	{
		if (!_lock)  
		{
			parasol.state = ParasolStates.CLOSED;
			audio_play_sound(soParasolClose, 1, false);
		}
	}
	
	//Pick up energy
	if (place_meeting(x, y, oEnergy))
	{
		with (instance_place(x, y, oEnergy))
		{
			other.parasol.energy += energy;
			instance_destroy();
		}
	}
	
	//Throw
	if (parasol.state == ParasolStates.CLOSED && _shoot)
	{
		with (parasol) 
		{
			
			var _close_col = place_meeting(x, y, oBlock);
			var _close_index = undefined;
			if (_close_col)	
			{
				var _object = instance_place(x, y, oBlock);
				_close_index = _object.object_index;
			}
			
			var _far_col = true;
			if (_close_col && _close_index != oDestructBlock && _close_index != oDoor)  _far_col = place_meeting(x + lengthdir_x(sprite_width/2, image_angle), y + lengthdir_y(sprite_width/2, image_angle), oBlock)
			
			var _allowed = !_close_col | !_far_col;
			
			if (_allowed)
			{
				
				if (place_meeting(x, y, oButton) || place_meeting(x + lengthdir_x(recoil_max, image_angle), y + lengthdir_y(recoil_max, image_angle), oButton))  _stab = true;  //if button is close, then stab
				else
				{
				
					if (energy > 0)
					{
						if (recoil == 0)
						{
							recoil = -8;
							ShakeScreen(5, 15);
							with (instance_create_layer(x + lengthdir_x(PARASOL_WIDTH/2, image_angle), y + lengthdir_y(PARASOL_WIDTH/2, image_angle), "Bullets", oBeam))
							{
								direction = other.desired_angle;
							}
							energy--;
						}
					}
					else
					{
						if (!_far_col)
						{
							x += lengthdir_x(sprite_width/2, image_angle);
							y += lengthdir_y(sprite_width/2, image_angle);
						}
					
						if (OutsideRoom())
						{
							x -= lengthdir_x(sprite_width/2, image_angle);
						    y -= lengthdir_y(sprite_width/2, image_angle);
							exit;
						}
					
						ShakeScreen(3, 10);
						state = ParasolStates.THROWN;	
						moving = true;
						other.parasol = noone;
					}
				}
			} else  _stab = true;
		}
	}
		
	//Stab
	if (_stab)
	{
		with (parasol)
		{
			recoil = recoil_max;
			
			var _blocks = ds_list_create();
			var _count = instance_place_list(x + lengthdir_x(recoil, image_angle), y + lengthdir_y(recoil, image_angle), oDestructBlock, _blocks, true);
			
			repeat(_count)
			{
				var _block = ds_list_find_value(_blocks, 0);
				_block.hp -= 1;
				_block.draw_x = x + lengthdir_x(PARASOL_WIDTH + recoil, image_angle) - _block.x;
				_block.draw_y = y + lengthdir_y(PARASOL_WIDTH + recoil, image_angle) - _block.y;
					
				var _scratch_width = sprite_get_width(sScratch);
				var _scratch_height = sprite_get_height(sScratch);
					
				_block.draw_x = clamp(_block.draw_x, _scratch_width/2, _block.sprite_width - _scratch_width/2);
				_block.draw_y = clamp(_block.draw_y, _scratch_height/2, _block.sprite_height - _scratch_height/2);
				ds_list_delete(_blocks, 0);
				break;
			}
			
			ds_list_destroy(_blocks);
			

		}
	}
	
}
else
{
	//Get parasol back
	if (gun == noone && prev_parasol != noone && instance_exists(prev_parasol))
	{
		if (parasol_call_counter > 0 && prev_parasol.state == ParasolStates.THROWN && ((!prev_parasol.moving && prev_parasol.amplitude < 10) || (prev_parasol.x < 0 || prev_parasol.y < 0 || prev_parasol.x > room_width || prev_parasol.y > room_height)))
		{
			prev_parasol.state = ParasolStates.RETURNING;	
		}
	}
	
	parasol_call_counter = max(0, parasol_call_counter - 1);
	
	//Pickup parasol
	if (place_meeting(x, y, oParasol) && gun == noone)
	{
		
		//dont mind this
		if (room == rParasolIntro)
		{
		
			if (instance_exists(inst_E82D8C4))
			{
				instance_destroy(oSpotlightController);
				with (oSpotlight)	instance_destroy();
	
				with (inst_634C8BB5)	active = true;
				with (inst_3D9EFABE)	active = true;
	
	
				ShakeScreen(30, 50);
	
				with (inst_4703B513)	visible = true;
	
				instance_destroy(inst_E82D8C4);	
				
				with (oDeeJay)
				{
					bgm = musSilence;
					silence_timer = silence_timer_max;
					musNext = musMus;
				}
				
				audio_play_sound(soParasolPickUp, 0, false);
			}
			
		}
		
		
		
		just_taken = true;
		var _parasol = instance_place(x, y, oParasol);
	
		if ((prev_parasol == noone || !instance_exists(prev_parasol) || prev_parasol.state != ParasolStates.RETURNING || prev_parasol == _parasol) &&
			(_parasol.state == ParasolStates.START || _parasol.state == ParasolStates.RETURNING || 
				(_parasol.state == ParasolStates.THROWN && !_parasol.moving && _parasol.can_be_picked_up == 0)))
		{
			with (_parasol)
			{
				ds_list_clear(damage_list);
				
				host = other.id;	
				linked_block = undefined;
				
				if (state == ParasolStates.RETURNING)
					audio_play_sound(soParasolReturn, 0, false);
					
				state = ParasolStates.CLOSED;
			}
			parasol = _parasol;
		}
	}

}

//Guard
if (place_meeting(x + xspeed, y + yspeed, oGuard))  
{
	var _height = abs(max_height - y);		
	audio_sound_pitch(soDying, choose(0.8, 0.9, 1.0));
	audio_sound_gain(soDying, (0.8 + _height/room_height) * global.sfx_volume, 0);
	audio_play_sound(soDying, 1, false);
	instance_destroy();	
}

//Horizontal collision
if (place_meeting(x + xspeed, y, oBlock))
{
	repeat (abs(xspeed))
	{
		if (place_meeting(x + sign(xspeed), y, oBlock))  break;
		x += sign(xspeed);
	}
	xspeed = 0;
}

x += xspeed;

if (instance_exists(oPortalAura))
{
	if (place_meeting(x, y, oPortalAura))
	{
		state = PlayerStates.VICTORY;
		with (oSpeedrunner)
		{
			record = true;
		}	
	}
}

//Avioding the cube
if (place_meeting(x, y, oCube))
{
	var _flag = false;
	with (instance_place(x, y, oCube))	
	{
		if (yspeed > 0)
		{
			_flag = true;
		}
	}
	if (_flag)
	{
		while (place_meeting(x, y, oCube)) y++;	
	}
}

//Jumping
if (_up)  jump_counter = 10;

droplets_timer = max(0, droplets_timer - 1);

if ((grounded_counter > 0 || double_jump) && jump_counter > 0)
{
	if (grounded_counter == 0)  
	{
		double_jump = false;
		audio_sound_pitch(soJump, 1.1);
	}
	else  audio_sound_pitch(soJump, 1.0);
	
	
	yspeed = -jumpspeed;
	
	//if (global.slow_motion > 0)
	//	y -= yspeed;
	
	grounded_counter = 0;
	jump_counter = 0;

	
	audio_play_sound(soJump, 0, false);
	
	//Particles
	if (droplets_timer == 0)
	{
		instance_create_layer(x, bbox_bottom, "Items", oPlayerDroplets);
		droplets_timer = droplets_timer_max;
	}
}


//Stop the jump

if (yspeed < 0 && !_up_press && (parasol == noone || parasol.state != ParasolStates.OPEN))
{
	yspeed = max(yspeed, -jumpspeed / 4);
}

jump_counter = max(0, jump_counter - 1);
grounded_counter = max(0, grounded_counter - 1);


//Vertical collisions
if (place_meeting(x, y + yspeed, oBlock))
{	
	repeat (abs(yspeed))
	{
		if (place_meeting(x, y + sign(yspeed), oBlock))  break;
		y += sign(yspeed);
	}
	
	if (grounded_counter == 0 && !place_meeting(x, y, oFan))
	{		
		if (droplets_timer == 0)
		{
			instance_create_layer(x, bbox_bottom, "Items", oPlayerDroplets);
			audio_play_sound(soSlimeLanding, 0, false);
		}
	}
		
	yspeed = 0;
}

y += yspeed// * global.slow_motion_effect;

if (y < max_height)  max_height = y;

if (place_meeting(x, y + 1, oBlock))
{
	if (grounded_counter == 0) 
	{
		//audio_sound_pitch(soLanding, choose(0.8, 1.0, 1.2));
		//audio_sound_gain(soLanding, 0.1, 0);
		//audio_play_sound(soLanding, 1, false);
	}
	
	grounded_counter = grounded_counter_max;
	double_jump = true;
	max_height = y;
}


//image_xscale = sign(_move) == 0 ? image_xscale : sign(_move) * abs(image_xscale);

//Particles
if (xspeed != 0 || yspeed != 0)
{
	var _len = point_distance(x, y, xprevious, yprevious);
	var _ang = point_direction(xprevious, yprevious, x, y);
	var _x = xprevious;
	var _y = yprevious;
	
	for (var i = 0; i < _len; ++i)
	{	
		part_particles_create(global.part_system, _x + lengthdir_x(i, _ang), _y + lengthdir_y(i, _ang), trail, 1);
	}
}

//Sprite & Sounds
if (grounded_counter > 0)
{
	if (_lock && parasol != noone)
	{
		sprite_index = sPlayerShielding;
		image_speed = 1;
		if (image_index + image_speed >= image_number)
		{
			image_speed = 0;
			image_index = image_number - 1;
		}	
		audio_stop_sound(slide_sound);
	}
	else if (abs(xspeed) < 0.5) 
	{
		image_speed = 1;
		sprite_index = sPlayerIdle;
		audio_stop_sound(slide_sound);	
		
	}
	else
	{
		image_speed = 1;
		sprite_index = sPlayerMove;	
		if (!audio_is_playing(slide_sound) || sign(prev_xspeed) != sign(xspeed))
		{
			audio_stop_sound(soSlide);
			slide_sound = audio_play_sound(soSlide, 0, true);
			audio_sound_pitch(slide_sound, 0.8);
			audio_sound_set_track_position(slide_sound, random(slide_loop_length));
			audio_sound_gain(slide_sound, 0, 0);
			audio_sound_gain(slide_sound, slide_gain * global.sfx_volume, 1000);
		}
		
		var _position = audio_sound_get_track_position(slide_sound);
		if (_position > slide_loop_length)
		{
			audio_sound_set_track_position(slide_sound, _position - slide_loop_length);
		}	
	}
}
else
{
	image_speed = 1;
	audio_stop_sound(slide_sound);
}


//Collision with enemies
if (place_meeting(x, y, oEnemy))  
{
	var _height = abs(max_height - y);		
	audio_sound_pitch(soDying, choose(0.8, 0.9, 1.0));
	audio_sound_gain(soDying, (0.8 + _height/room_height) * global.sfx_volume, 0);
	audio_play_sound(soDying, 1, false);
	instance_destroy();	
}

//Collision with the boss
with (instance_place(x, y, oBoss))  
{
	if (state != BossStates.TOSTART)
	{
		var _height = abs(other.max_height - y);		
		audio_sound_pitch(soDying, choose(0.8, 0.9, 1.0));
		audio_sound_gain(soDying, (0.8 + _height/room_height) * global.sfx_volume, 0);
		audio_play_sound(soDying, 1, false);
		instance_destroy(other.id);
	}
}

//Collision with laser
if (place_meeting(x, y, oLaser))  
{
	var _foo = true;
	with (instance_place(x, y, oLaser))
	{
		if (hero_proof)
			_foo = false;
		else
			instance_destroy();
	}
	
	if (_foo)
	{
		var _height = abs(max_height - y);		
		audio_sound_pitch(soDying, choose(0.8, 0.9, 1.0));
		audio_sound_gain(soDying, (0.8 + _height/room_height) * global.sfx_volume, 0);
		audio_play_sound(soDying, 1, false);
		instance_destroy();	
	}
}


//Chaser
if (place_meeting(x, y, oChaser))
{
	if (oChaser.image_index > 7)
	{
		var _height = abs(max_height - y);		
		audio_sound_pitch(soDying, choose(0.8, 0.9, 1.0));
		audio_sound_gain(soDying, (0.8 + _height/room_height) * global.sfx_volume, 0);
		audio_play_sound(soDying, 1, false);
		instance_destroy();	
	}
}


if (instance_exists(oBound) && y > oBound.y)
{
	var _height = abs(max_height - y);		
	audio_sound_pitch(soDying, choose(0.8, 0.9, 1.0));
	audio_sound_gain(soDying, (0.8 + _height/room_height) * global.sfx_volume, 0);
	audio_play_sound(soDying, 1, false);
	instance_destroy();		
}

prev_xspeed = xspeed;
prev_yspeed = yspeed;

if (abs(xspeed) > abs(xspeed_saved) || xspeed * xspeed_saved < 0)
{
	xspeed_saved = xspeed;	
}

yspeed_saved = yspeed;	

//Spotlight
with (spotlight)
{
	x = other.x;
	y = other.y;
}	