var _strength = (abs(sin((get_timer() - phase)/magnitude)) + 0.2) * 0.05;
shader_set(shdBrightness);
shader_set_uniform_f(u_strength, _strength);
draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, 0, c_white, 1);
shader_reset();