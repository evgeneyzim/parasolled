//Activate region
instance_activate_region(x - abs(sprite_width)/2, y - abs(sprite_height)/2, abs(sprite_width), abs(sprite_height), true);

spin_speed = 10 * global.slow_motion_effect;
laser_speed = 30 * global.slow_motion_effect;
diff_angle_speed = 3 * global.slow_motion_effect;
throw_speed = 25 * global.slow_motion_effect;

if (infinity_gun)
{
	total_shots = 5;
}


switch (state)
{
	case GunStates.FREE:
		
		hosted = noone;
		shoot = false;
		
		image_angle = 0;
		
		shot_number = shot_number_max;	
			
		if (abs(xspeed) + abs(yspeed) > 0)
		{
			im_angle += spin_speed;
			sprite_index = sGun;
			start_timer = false;
		}
		else if (!start_timer && sprite_index != sGunThrown)
		{
			start_timer = true;
			alarm[0] = 60;
		}
		
		var _saved_xspeed = xspeed;
		if (place_meeting(x + xspeed, y, oBlock) && !place_meeting(x + xspeed, y, banned_block))
		{
			gravity_off = false;
			_saved_xspeed *= -koeff;
			repeat (abs(xspeed))
			{
				if (place_meeting(x + sign(xspeed), y, oBlock))
				{
					break;	
				}
				x += sign(xspeed);
			}
			xspeed = 0;
			banned_block = noone;
		}
		x += xspeed;
		xspeed = _saved_xspeed;
		
		
		if (yspeed == 0 && place_meeting(x, y + 1, oBlock))
		{
			xspeed = 0;
			if (total_shots == 0)
				fade_start = true;
		}
		
		if (!gravity_off)
			yspeed += global.grav;
		
		var _saved_yspeed = yspeed;
		if (place_meeting(x, y + yspeed, oBlock) && !place_meeting(x, y + yspeed, banned_block))
		{
			gravity_off = false;
			_saved_yspeed *= -koeff;
			if (abs(_saved_yspeed) < 1)
				_saved_yspeed = 0;
				
			repeat (abs(yspeed))
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))
				{
					break;	
				}
				y += sign(yspeed);
			}
			yspeed = 0;
			banned_block = noone;
		}
		y += yspeed;
		yspeed = _saved_yspeed;
		
		if (fade_start)
		{
			image_alpha -= 0.02;
			if (image_alpha <= 0)
			{
				instance_destroy();
			}
		}
		
		with (instance_place(x, y, oPlayer))
		{
			
			if (stagger == 0 && gun == noone && parasol == noone && other.total_shots > 0 && 
				(prev_parasol == noone || !instance_exists(prev_parasol) || prev_parasol.state != ParasolStates.RETURNING))
			{
				just_taken = true;
				other.host = id;
				other.state = GunStates.LOAD;
				audio_play_sound(soGunReload, 0, false);
				gun = other.id;
			}
		}
		
		break;
		
	case GunStates.LOAD:
		
		sprite_index = sGun;
		
		im_angle = image_angle;
		
		if (!instance_exists(host))
		{
			state = GunStates.FREE;
			var _dir = point_direction(xOld, yOld, x, y);
			xspeed = lengthdir_x(throw_speed, _dir);
			yspeed = lengthdir_y(throw_speed, _dir);
			break;
		}
		else
		{
			xOld = x;
			yOld = y;
			x = host.xprevious;
			y = host.yprevious; 
		
			if (host.object_index == oPlayer)
			{
				if (global.input_type == InputType.KEYBOARD)  image_angle = point_direction(x, y, mouse_x, mouse_y);
				else										  
				{
					var _hor = gamepad_axis_value(SelectGamepad(0), gp_axisrh);
					var _ver = gamepad_axis_value(SelectGamepad(0), gp_axisrv);
				
					if (abs(_hor) > 0.2 || abs(_ver) > 0.2)
					{
						desired_angle = point_direction(0, 0, _hor, _ver);	
					}
					image_angle = desired_angle;
				}
			
				if (image_angle > 90 && image_angle < 270)	
				{
					image_xscale = -1;
					image_angle -= 180;
				}
				else			
				{
					image_xscale = 1;
				}
			}
		
			
		
			if (host.object_index == oPlayer)
			{
				if (diff_angle == 0)
				{
					if (mouse_check_button_released(mb_left) || gamepad_button_check_pressed(SelectGamepad(0), gp_stickr) || gamepad_button_check_pressed(SelectGamepad(0), gp_shoulderrb) || throw_gun)
					{
						var _angle;
						if (image_xscale > 0) _angle = image_angle;
						else				  _angle = image_angle + 180;
						if (total_shots != 0 && !throw_gun) 
						{
							audio_play_sound(soGunShoot, 0, false);
							with (instance_create_layer(x + lengthdir_x(15 * abs(image_xscale), _angle), y + lengthdir_y(15 * abs(image_xscale), _angle), "Bullets", oLaser))
							{
								explode = false;
								direction = _angle;
								image_xscale = 2;
								image_yscale = 2;
								image_angle = direction;
								spd = other.laser_speed;
								hero_proof = true;
							}
							shot_number--;
							total_shots--;
							diff_angle = diff_angle_max;
						}
						else
						{
							host.stagger = host.stagger_max;
							host.gun = noone;
							host = noone;
							state = GunStates.FREE;
							xspeed = lengthdir_x(throw_speed, _angle);
							yspeed = lengthdir_y(throw_speed, _angle);
							banned_block = instance_place(x, y, oBlock);
							gravity_off = true;
							throw_gun = false;
						}
					}
				}
			}
			
			if (shoot && diff_angle == 0)
			{
				shoot = false;
				var _angle;
				if (image_xscale > 0) _angle = image_angle;
				else				  _angle = 180 + image_angle;
				if (total_shots != 0) 
				{
					var _w = sprite_get_width(sBeam);
					audio_play_sound(soGunShoot, 0, false);
					with (instance_create_layer(x + lengthdir_x(_w, _angle), y + lengthdir_y(_w, _angle), "Bullets", oLaser))
					{
						banned_block = instance_place(x, y, oBlock);
						
						if (other.hosted != noone)
						{
							hosted = other.hosted;	
						}
						explode = false;
						direction = _angle;
						image_xscale = 2;
						image_yscale = 2;
						image_angle = direction;
						spd = other.laser_speed;
					}
					shot_number--;
					diff_angle = diff_angle_max;
				}
			}
		
			diff_angle = max(0, diff_angle - diff_angle_speed);
		
			if (shot_number == 0)
			{
				shot_number = shot_number_max;
				state = GunStates.RELOADING;
				audio_play_sound(soGunReload, 0, false);
				local_angle = image_angle;
				start_angle = image_angle;
			}
		}
		break;
		
	case GunStates.RELOADING:
	
		sprite_index = sGun;
		diff_angle =  0;
		im_angle = image_angle;
	
		if (!instance_exists(host))
		{
			state = GunStates.FREE;
			var _dir = point_direction(xOld, yOld, x, y);
			xspeed = lengthdir_x(throw_speed, _dir);
			yspeed = lengthdir_y(throw_speed, _dir);
			break;
		}
	
		x = host.xprevious;
		y = host.yprevious; 
		
	
		image_angle = local_angle;
		local_angle += spin_speed * 2/3 * sign(image_xscale);
		
		if (abs(local_angle - start_angle) >= 360)
		{
			state = GunStates.LOAD;
		}
	
		break;
}
