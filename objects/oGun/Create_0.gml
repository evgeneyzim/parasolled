enum GunStates
{
	FREE,
	LOAD,
	RELOADING,
};

xspeed = 0;
yspeed = 0;

koeff = 0.1;

host = noone;

state = GunStates.FREE;

spin_speed = 10;

fade_start = false;
start_timer = false;

shot_number_max = 3;
shot_number = shot_number_max;
total_shots = 5;

shoot = false;
throw_gun = false;

laser_speed = 30;

diff_angle = 0;
diff_angle_max = 30;
diff_angle_speed = 3;

desired_angle = 0;

throw_speed = 25;

banned_block = noone;

gravity_off = false;

hosted = noone;

im_angle = 0;

xOld = 0;
yOld = 0;

immortal = false;

infinity_gun = false;