/// @description 

if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	trail = oPartTypes.slime_trail;
}

var _jumped = false;

instance_activate_region(x - abs(sprite_width/2), y, abs(sprite_width), abs(sprite_height), true);
instance_activate_region(x - abs(sprite_width/2) + xspeed, y + yspeed, abs(sprite_width), abs(sprite_height), true);

//Push by others
//var _list = ds_list_create();

//var _count = instance_place_list(x, y, oSlimeLike, _list, true);

//repeat (_count)
//{
//	var _inst = ds_list_find_value(_list, 0);
	
//	if (_inst != id)
//	{
//		var _dir = point_direction(_inst.x, _inst.y, x, y);
	
//		xspeed_push += lengthdir_x(push_value, _dir);
//		yspeed_push += lengthdir_y(push_value, _dir);
//	}
		
//	ds_list_delete(_list, 0);
//}

//ds_list_destroy(_list);

////If noone is near
//if (!place_meeting(x, y, oSlimeLike))
//{
//	xspeed_push = lerp(xspeed_push, 0, 0.1);	
//	yspeed_push = lerp(yspeed_push, 0, 0.1);	
//}


switch (state)
{
	case SlimeState.IDLE:
		//Idle state
		current_speed = charge_speed;
		yspeed += global.grav + yspeed_push;
		xspeed = 0.1;
		
		if (place_meeting(x + current_speed * direct, y, oSpike))
		{
			direct *= -1;
		}

		//If sees a hole
		if (place_free(x + (current_speed - sprite_width/2) * direct, y + 1))
		{
			direct *= -1;	
		}

		//if sees a obstacle
		if (place_meeting(x + current_speed * direct, y, oBlock))
		{
			direct *= -1;	
		}
	
		x += direct * current_speed + xspeed_push;
		
		//Vertical collision
		if (place_meeting(x, y + yspeed, oBlock))
		{
			repeat(yspeed)	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))  break;
				y += sign(yspeed);
			}
			yspeed = 0;
		}

		y += yspeed;
		
	
		break;
	
	
	case SlimeState.FIGHT:
		//Fight state
		current_speed = charge_speed;

		var _jumpspeed = 4 * jumpspeed/5;
		if (instance_exists(target))	_jumpspeed += ((target.y - y) < 32) * jumpspeed/5;

		if (instance_exists(target)) direct = sign(target.x - x);

		yspeed += global.grav + yspeed_push;


		//Standart movements
		xspeed = lerp(xspeed, current_speed * direct, acceleration) + xspeed_push;


		//Avioding the cube
		if (place_meeting(x, y, oCube))
		{
			var _flag = false;
			with (instance_place(x, y, oCube))	
			{
				if (yspeed > 0)
				{
					_flag = true;
				}
			}
			if (_flag)
			{
				while (place_meeting(x, y, oCube)) y++;	
			}
		}

		//Horizontal collision
		if (place_meeting(x + xspeed, y, oBlock))
		{
			repeat(xspeed)	
			{
				if (place_meeting(x + sign(xspeed), y, oBlock))  break;
				x += sign(xspeed);
			}
			xspeed = 0;
		}

		x += xspeed;

		//If grounded
		if (place_meeting(x, y + 1, oBlock))
		{
			//Jumps if sees a spike
			if (place_meeting(x + current_speed * direct, y, oSpike))
			{
				yspeed = -_jumpspeed;
				_jumped = true;
			}

			//If sees a hole
			if (place_free(x + current_speed * direct, y + 1))
			{
				yspeed = -_jumpspeed;	
				_jumped = true;
			}

			//if sees a obstacle
			if (place_meeting(x + current_speed * direct, y, oBlock))
			{
				yspeed = -_jumpspeed;
				_jumped = true;
			}
		}

		//Vertical collision
		if (place_meeting(x, y + yspeed, oBlock))
		{
			repeat(yspeed)	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))  break;
				y += sign(yspeed);
			}
			yspeed = 0;
		}

		y += yspeed;
		break;
		
	default:
		Raise("Error in oSlime states");
		break;
}

//Graphics
image_xscale = sign(direct) * abs(image_xscale);

timer_save--;
if (timer_save == 0)
{
	timer_save = timer_save_max;
	for (var i = amount - 1; i > 0; --i)
	{
		prev_x[i] = prev_x[i - 1];	
		prev_y[i] = prev_y[i - 1];	
	}

	prev_x[0] = x;
	prev_y[0] = y;
}


//Dying
if (place_meeting(x, y, oSpike))
{
	if (instance_exists(oPlayer))	explode = true;
	else							explode = false;
	instance_destroy();	
}

if (_jumped && OnView(id))
{
	var _s = audio_play_sound(soJump, 0, 0);
	audio_sound_pitch(_s, 0.7);
}

with (oParasol)
{
	if (linked_block == other.id)
	{
		if (abs(y + lengthdir_y(PARASOL_WIDTH, image_angle) - other.y) < 56)
			additional += other.amplitude * sin(get_timer()/50000);	
	}
}	