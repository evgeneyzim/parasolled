/// @description 

image_index = 1;

if (appear_alpha > 0)
{

	appear_alpha -= appear_alpha_dec;
	draw_set_alpha(1 - appear_alpha);
}
if (OnView(id))
{
	for (var i = amount - 1; i > 0; --i)
	{
		var _x = prev_x[i];
		var _y = prev_y[i];
	
		draw_set_color(merge_color(c_red, c_yellow, 1/(amount + 1) * i));
		draw_set_alpha(1 - 1/(amount + 1) * i);
		draw_roundrect(_x - sprite_width/2 + image_xscale * amplitude, _y + amplitude + sprite_height/2 * (i / amount), _x + sprite_width/2 - image_xscale * amplitude, _y + sprite_height - amplitude, false);
		
	}
	draw_set_alpha(1);

	DrawIt();
}

if (appear_alpha > 0)
{
	draw_set_alpha(1);
	
	gpu_set_blendmode(bm_add);
	draw_sprite_ext(sAppear, 0, x, y + sprite_height/2, 1 + 2 * appear_alpha, 1 + 2 * appear_alpha, 0, c_red, 1);
	gpu_set_blendmode(bm_normal);
		
}

