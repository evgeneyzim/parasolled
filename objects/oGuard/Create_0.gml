/// @description 

event_inherited();

randomize();

xspeed = 0;
yspeed = 0;

xspeed_push = 0;
yspeed_push = 0;
push_value = 0.25;

state = SlimeState.FIGHT;

//Speed params
walkspeed = 3;
charge_speed = 4;
current_speed = walkspeed;

jumpspeed = 12;

direct = choose(-1, 1);
acceleration = 0.2;


//Timer for changing direction in idle mode
idle_timer_module = 150;
idle_timer = idle_timer_module * random_range(0.7, 1.3);

explode = true;

_image_blend = c_red;
eye_color = 0x8484db;
back_color = 0x0c0c36;

amplitude = 5;

//target
target = oPlayer;

//functions after death
functions =  [];

//Prev positions
amount = 16;
prev_x = [];
prev_y = [];

for (var i = 0; i < amount; ++i)
{
	prev_x[i] = x;
	prev_y[i] = y;
}

timer_save_max = 1;
timer_save = timer_save_max;

//Appearing
appear_alpha = 1;
appear_alpha_dec = 0.02;

u_strength = shader_get_uniform(shdBrightness, "strength");

with (instance_create_layer(x, y + sprite_height/2, "Particles", oPlayerCreateCircle))
{
	color = c_red;
}

part_init = false;

u_blur_vector = shader_get_uniform(shdExtra, "blur_vector");
u_sigma = shader_get_uniform(shdExtra, "sigma");

DrawEye = function(x_add, y_add, _timer)
{
	var _eye_x = x - x_add;
	var _eye_y = y + y_add;

	var _tar_x, _tar_y;

	var _dir;

	if (!instance_exists(target) || state == SlimeState.IDLE)
	{
		_dir = point_direction(xprevious, yprevious, x, y);
	}
	else
	{
		_tar_x = target.x;	
		_tar_y = target.y;	
		_dir = point_direction(_eye_x, _eye_y, _tar_x, _tar_y);
	}

	var _new_x = _eye_x + lengthdir_x(7, _dir);
	var _new_y = _eye_y + lengthdir_y(7, _dir);

	var _add = amplitude * sin(_timer/50000);
	draw_circle(_new_x, _new_y + _add, 7, false);	
}


DrawIt = function()
{
	//draw back
	draw_set_color(back_color);
	draw_rectangle(bbox_left + 2, bbox_top + 5, bbox_right - 2, bbox_bottom - 5, false);

	draw_self();

	var _add = amplitude * sin(get_timer()/50000);

	DrawEyes();
	
	//gpu_set_blendmode(bm_add);
	//shader_set(shdExtra);
	//shader_set_uniform_f(u_blur_vector, 1, 1);
	//shader_set_uniform_f(u_sigma, 0.1);
	//DrawEyes();
	//shader_reset();
	//gpu_set_blendmode(bm_normal);

	//Head
	draw_sprite(sGuard, 0, x, y + _add);
}

DrawEyes = function()
{
	draw_set_color(eye_color);
	//Left eye
	DrawEye(-26, 22, get_timer());
	//Right eye
	DrawEye(26, 22, get_timer());	
}