enum ChaserStates
{
	INTRO,
	ACTION,
};

state = ChaserStates.INTRO;

spd = 4.1;

var _seq = seqChaser;
var _layer = "Boss";

_seqElem = layer_sequence_create(_layer, x, y, _seq);
_seqInst = layer_sequence_get_instance(_seqElem);

sequence_instance_override_object(_seqInst, oChaser, id);

