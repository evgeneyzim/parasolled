if (layer_sequence_is_finished(_seqElem))
{
	state = ChaserStates.ACTION;
}

if (state == ChaserStates.ACTION)
{
	x += spd;

	if (instance_exists(oPlayer) && oPlayer.state != PlayerStates.VICTORY)
	{
		y = lerp(y, oPlayer.y, 0.05);
		var _desired_angle = point_direction(x, y, oPlayer.x, oPlayer.y);
	
		if (_desired_angle > 360 - _desired_angle)
		{
			_desired_angle -= 360;	
		}
	
		var _difference = _desired_angle - image_angle;
		image_angle += _difference/5;
	}


	//Sound
	if (image_index == 9)
		audio_play_sound(soClink, 1, 0);
}