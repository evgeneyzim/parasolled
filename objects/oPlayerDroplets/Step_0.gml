if (action)
{
	action = false;
	
	if (sprite_index == sPlayerParticles)
	{
		with (oPlayerDroplets)
		{
			if (id != other.id && image_index < 1)
			{
				instance_destroy();	
			}
		}	
	}
}