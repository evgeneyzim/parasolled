/// @description


draw_set_color(color);
draw_set_alpha(alpha);

draw_circle(x, y, size, false);

draw_set_alpha(1);