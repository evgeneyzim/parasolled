/// @description
if (!trail_init)
{
	trail_init = true;
	dead_sparkle_trail = part_type_create();
	part_type_shape(dead_sparkle_trail, pt_shape_sphere);
	part_type_size(dead_sparkle_trail, size/10, size/10, -0.005, 0);
	part_type_color1(dead_sparkle_trail, color);
	part_type_alpha1(dead_sparkle_trail, alpha);
	part_type_speed(dead_sparkle_trail, 0, 0, 0, 0);
	part_type_direction(dead_sparkle_trail, 0, 0, 0, 0);
	part_type_life(dead_sparkle_trail, 30, 50);
	
	if (size < 10)
	{
		xspeed = lengthdir_x(spd * global.slow_motion_effect, dir);
		yspeed = lengthdir_y(spd * global.slow_motion_effect, dir);
	}
	else
	{
		xspeed = 0;
		yspeed = 0;
	}
}



if (size < 10)  yspeed += global.grav;
else            image_alpha -= 0.5 * global.slow_motion_effect;

x += xspeed;
y += yspeed;

image_alpha -= 0.01 * global.slow_motion_effect;
if (image_alpha <= 0)  instance_destroy();

if (size < 10 && position_meeting(x, y, oBlock))  instance_destroy();

part_particles_create(global.part_system_const, x, y, dead_sparkle_trail, 1);

//Add linear particles