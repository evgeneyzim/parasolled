if (appear_alpha > 0)
{
	shader_set(shdBrightness);
	shader_set_uniform_f(u_strength, appear_alpha);

	
	appear_alpha -= appear_alpha_dec;
	draw_set_alpha(1 - appear_alpha);
}

draw_self();

if (appear_alpha > 0)
{
	draw_set_alpha(1);
	shader_reset();
	
	gpu_set_blendmode(bm_add);
	draw_sprite_ext(sAppear, 0, x, y, 1 + 2 * appear_alpha, 1 + 2 * appear_alpha, 0, c_red, 1);
	gpu_set_blendmode(bm_normal);
		
}


//if (instance_exists(target) && state == GoonStates.CHASE) with (target)
//{
//	draw_set_color(c_red);
//	draw_circle(x, y, 20, false);
//}

//if (instance_exists(target) && state == GoonStates.CHASE && !idle && target.y > y)
//{
//	var _left = FindLowest(-walkspeed, target.y);
//	var _right = FindLowest(walkspeed, target.y);
	
//	draw_set_color(c_green);
//	draw_circle(_left[0], _left[1], 10, false);
	
//	draw_set_color(c_blue);
//	draw_circle(_right[0], _right[1], 10, false);
	

//}

//if (idle)
