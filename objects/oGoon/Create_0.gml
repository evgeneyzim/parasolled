event_inherited();

enum GoonStates
{
	WANDER, 
	SHOOTING,
	CHASE,
};

state = GoonStates.CHASE;

//velocity
xspeed = 0;
yspeed = 0;

//Push values
xspeed_push = 0;
yspeed_push = 0;
push_value = 0.25;

//speed params
walkspeed = 4;
jumpspeed = 14;
acceleration = 0.1;

//Appearing
appear_alpha = 1;
appear_alpha_dec = 0.02;

u_strength = shader_get_uniform(shdBrightness, "strength");

with (instance_create_layer(x, y, "Particles", oPlayerCreateCircle))
{
	color = c_red;
}

//For pathfinding
min_offset_y = 40;

target = oPlayer;
target_init = false;

jump_height = sqr(jumpspeed) / (2 * global.grav);

action_time_max = 4;
action_time = 0;

//If sees no target
idle = false;
idle_direction = choose(1, -1);


//Gun things
gun = noone;

timer_shoot_max = 30;
timer_shoot = timer_shoot_max;

shoked_max = 10;
shoked = 0;


//Death
explode = true;
_image_blend = c_red;

//After death
functions = [];
deactivators = [];




marked = false;

direct = 0;





//Particles
part_init = false;

u_blur_vector = shader_get_uniform(shdExtra, "blur_vector");
u_sigma = shader_get_uniform(shdExtra, "sigma");




//FUNCTIONS

FindLowest = function(x_add, y_target)
{
	var _y = y;
	var _x = x;
	
	while (!place_meeting(_x, _y, oBlock))
	{
		_x += x_add;
		
		while (!place_meeting(_x, _y + 1, oBlock))
		{
			_y++;
			if (abs(_y - y) > room_height)
			{
				return [-1, 1];	
			}
		}
		
		if (abs(_x - x) > room_width)
		{
			return [-1.5, 1];	
		}
		
		if (abs(_x - target.x) < abs(x_add) && abs(_y - target.y) < min_offset_y)
			break;
	}
	
	return [_x, _y];
	
}

FindTarget = function()
{
	//Get all the guns
	var _list = ds_list_create();
		
	with (oGun)
	{
		//If gun is available
		if (host == noone && total_shots > 0)
		{
			var _s = sprite_index;
			sprite_index = sGunThrown;
			
			if (!place_meeting(x, y, oSpike))	
				ds_list_add(_list, id);	
				
			sprite_index = _s;
		}
	}
	
		
	
	//Sort guns by distance
	for (var i = 0; i < ds_list_size(_list); ++i)
	{
		for (var j = 0; j < ds_list_size(_list); ++j)
		{
			if (i == j)	continue;
			var _gun_i = ds_list_find_value(_list, i);
			var _gun_j = ds_list_find_value(_list, j);

			if (distance_to_object(_gun_j) < distance_to_object(_gun_i))
			{
				ds_list_replace(_list, i, _gun_j);
				ds_list_replace(_list, j, _gun_i);
			}
		}	
	}
		
		
	//Search for the closest gun
	var _found = false;
		
	repeat(ds_list_size(_list))
	{
		var _current_gun = ds_list_find_value(_list, 0);
			
		if (distance_to_object(_current_gun) < distance_to_object(oPlayer))
		{
			if (_current_gun.y - y > -min_offset_y * 4)
			{
				target = _current_gun;
				_found = true;
				break;
			}
		}
			
		ds_list_delete(_list, 0);
	}
		
	ds_list_destroy(_list);
		
		
	//If no guns in sign, chase player
	if (!_found)
	{
		target = oPlayer;	
	}	
}
