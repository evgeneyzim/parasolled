if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	trail = oPartTypes.slime_trail;
}

//Activate region
instance_activate_region(x - abs(sprite_width), y - abs(sprite_height), 2 * abs(sprite_width), 2 * abs(sprite_height), true);

//Sounds
var _jumped = false;

//Pushed by others
var _list = ds_list_create();

var _count = instance_place_list(x, y, oSlimeLike, _list, true);

repeat (_count)
{
	var _inst = ds_list_find_value(_list, 0);
	
	if (_inst != id)
	{
		var _dir = point_direction(_inst.x, _inst.y, x, y);
	
		xspeed_push += lengthdir_x(push_value, _dir);
		yspeed_push += lengthdir_y(push_value, _dir);
	}
		
	ds_list_delete(_list, 0);
}

ds_list_destroy(_list);

//If noone is near
if (!place_meeting(x, y, oSlimeLike))
{
	xspeed_push = lerp(xspeed_push, 0, 0.1);	
	yspeed_push = lerp(yspeed_push, 0, 0.1);	
}


//Actions
switch (state)
{
	
	case GoonStates.CHASE:
	//No gun, tries to find one. If there's no gun near, search for the player	
		
		
		//REFRESH SHOOTING PARAMS
		
		//Shootin timer
		timer_shoot = timer_shoot_max;

		//shoked timer
		shoked = max(0, shoked-1);



		
	
		//If target isn't found
		if (!target_init || true)
		{
			FindTarget();
			target_init = true;
		}
	
		

		//Choose the way of chasing
		if (action_time == 0)
		{
			//Deactivate idle state
			idle = false;
			
			if (instance_exists(target)) 
			{
				if (abs(target.y - y) < min_offset_y)
				{
					//On the same level as target, just move along
					var _direct = sign(target.x - x);
					direct = (_direct != 0)? _direct : direct;
				}
				else if (target.y > y)
				{
					//Above the target	
					
					var _self_area = instance_place(x, y, oGoonArea);
					var _target_area = instance_place(target.x, target.y, oGoonArea);
					
					
					var _left = FindLowest(-walkspeed, target.y);
					var _right = FindLowest(walkspeed, target.y);
				
					var _left_value;
					var _right_value;
					
					
					if (_left[0] < 0)	
					{
						_left_value = 1;
						_right_value = 0;
					}
					else if (_right[0] < 0)
					{
						_left_value = 0;
						_right_value = 1;	
					}
					else
					{
						if (_self_area == noone || _self_area != _target_area)
						{
							with (target)
							{
								_left_value = y - _left[1];//distance_to_point(_left[0], _left[1]);
								_right_value = y - _right[1];//distance_to_point(_right[0], _right[1]);
							}
						}
						else
						{
							with (target)
							{
								_left_value = distance_to_point(_left[0], _left[1]);
								_right_value = distance_to_point(_right[0], _right[1]);
							}
						}
					}
					
					if (_left_value < _right_value)	direct = -1;
					else							direct = 1;
					
				}
				else
				{
					//Below the target
								
					if (y - target.y < min_offset_y * 4)	//if target is reachible
						direct = sign(target.x - x);
					else
					{
						idle = true;				//otherwise idle
						target_init = false;
					}
				}
			}
			
			action_time = action_time_max;
		}
		else
		{
			action_time--;	
		}

		idle_direction = direct;
		
		//idle movements
		if (place_meeting(x, y + 1, oBlock))
		{
			if (idle || !instance_exists(target))
			{
				if (place_meeting(x + idle_direction * walkspeed, y, oBlock) || 
					!place_meeting(x + idle_direction * walkspeed, y + 1, oBlock) || 
						place_meeting(x + idle_direction * walkspeed, y, oSpike))
				{
					idle_direction *= -1;	
				}
				direct = idle_direction;
			}
		}

		//Standart movements
		xspeed = lerp(xspeed, walkspeed * direct, acceleration + (idle) * 4 * acceleration); //React faster in idle mode
		xspeed += xspeed_push;

		//Moved by parasol
		with (instance_place(x, y, oParasol))
		{
			if ((state == ParasolStates.THROWN) && moving)
			{
				other.xspeed = xspeed;
			}
		}
		
		//Interaction with gun
		with (instance_place(x, y, oGun))
		{
			if (state == GunStates.FREE && abs(xspeed) + abs(yspeed) > 0.1 && host == noone)
			{
				//Pushed by a gun
				other.xspeed = xspeed;
			}
			else if (state == GunStates.FREE && host == noone && other.gun == noone && total_shots > 0 && other.shoked == 0)
			{
				//Take a gun
				other.gun = id;
				host = other.id;
				other.state = GoonStates.WANDER;
				state = GunStates.LOAD;
				if (OnView(id))
					audio_play_sound(soGunReload, 0, false);
			}
		}
		
		
		//Avioding the cube
		if (place_meeting(x, y, oCube))
		{
			var _flag = false;
			with (instance_place(x, y, oCube))	
			{
				if (yspeed > 0)
				{
					_flag = true;
				}
			}
			if (_flag)
			{
				while (place_meeting(x, y, oCube)) y++;	
			}
		}

		//xscale
		image_xscale = (direct != 0)? direct : image_xscale;

		//Horizontal collision
		if (place_meeting(x + xspeed, y, oBlock))
		{
			repeat(abs(xspeed))	
			{
				if (place_meeting(x + sign(xspeed), y, oBlock))  break;
				x += sign(xspeed);
			}
			xspeed = 0;
		}

		x += xspeed;

		//If grounded
		if (place_meeting(x, y + 1, oBlock))
		{
			//Jumps if sees a spike
			if (!idle && place_meeting(x + 1.5 * walkspeed * direct, y, oSpike))
			{
				
				if (place_meeting(x + 1.5 * walkspeed * direct, y - jump_height, oSpike))
				{
					yspeed = -jumpspeed * 3/4;	
				}
				else
				{			
					yspeed = -jumpspeed;
				}
				
				_jumped = true;
			}

			//If sees a hole
			if (!idle && place_free(x + walkspeed * direct, y + 1))
			{
				yspeed = -jumpspeed;	
				_jumped = true;
			}

			//if sees a obstacle
			if (!idle && place_meeting(x + walkspeed * direct, y, oBlock))
			{
				yspeed = -jumpspeed;
				_jumped = true;
			}
		}

		yspeed += global.grav;
		yspeed += yspeed_push;
		
		//Vertical collision
		if (place_meeting(x, y + yspeed, oBlock))
		{
			repeat(abs(yspeed))	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))  break;
				y += sign(yspeed);
			}
			yspeed = 0;
		}

		y += yspeed;
				
		break;
		
	case GoonStates.WANDER:
		
		//Shoked timer 
		shoked = max(0, shoked - 1);
		
		//Rotate the gun
		if (gun != noone && gun.state == GunStates.LOAD)
		{
			with (gun)
			{
				image_xscale = other.idle_direction;
				if (image_xscale > 0)	image_angle = 30;
				else					image_angle = -30;
			}
		}
		
		//If sees a block
		if (place_meeting(x + idle_direction * walkspeed, y, oBlock))
		{
			idle_direction *= -1;	
		}
		//If sees a spike
		if (place_meeting(x + idle_direction * walkspeed, y, oSpike))
		{
			idle_direction *= -1;	
		}
		//If sees a hole
		if (!place_meeting((idle_direction < 0 ? bbox_left : bbox_right) + idle_direction * walkspeed, y + 1, oBlock))
		{
			idle_direction *= -1;	
		}
		
		//Standart movements
		xspeed = lerp(xspeed, walkspeed * idle_direction, 5 * acceleration);
		xspeed += xspeed_push;
		
		//xscale
		image_xscale = idle_direction;
		
		//Moved by parasol
		with (instance_place(x, y, oParasol))
		{
			if ((state == ParasolStates.THROWN) && moving)
			{
				other.xspeed = xspeed * 2/3;
				other.state = GoonStates.CHASE;
				other.target_init = false;
				other.shoked = other.shoked_max * global.slow_motion_effect;
				if (other.gun != noone)
				{
					//Drop the gun
					StartSlowMotion(SLOW_MOTION_LENGTH);
					other.gun.state = GunStates.FREE;
					other.gun.host = noone;
					other.gun.yspeed = -other.jumpspeed;
					other.gun.y -= other.jumpspeed;
					other.gun = noone;
					break;
				}
			}
		}
		
		//Interaction with the gun
		var _list = ds_list_create();
		var _count = instance_place_list(x, y, oGun, _list, true);
		
		repeat(_count)
		{
			var _gun = ds_list_find_value(_list, 0);
			with (_gun)
			{
				if (id != other.gun && state == GunStates.FREE && abs(xspeed) + abs(yspeed) > 0.1 && host == noone)
				{
					other.xspeed = xspeed * 2/3;
					other.state = GoonStates.CHASE;
					other.target_init = false;
					other.shoked = other.shoked_max * global.slow_motion_effect;
					if (other.gun != noone)
					{
						//Drop the gun
						StartSlowMotion(SLOW_MOTION_LENGTH);
						other.gun.state = GunStates.FREE;
						other.gun.host = noone;
						other.gun.yspeed = -other.jumpspeed;
						other.gun.y -= other.jumpspeed;
						other.gun = noone;
						break;
					}
				}
			}
			ds_list_delete(_list, 0);
		}
		
		ds_list_destroy(_list);
		
		//Horizontal collision
		if (place_meeting(x + xspeed, y, oBlock))
		{
			repeat(abs(xspeed))	
			{
				if (place_meeting(x + sign(xspeed), y, oBlock))  break;
				x += sign(xspeed);
			}
			xspeed = 0;
		}
		x += xspeed;
		
		yspeed += global.grav;
		yspeed += yspeed_push;
		
		//Vertical collision
		if (place_meeting(x, y + yspeed, oBlock))
		{
			repeat(abs(yspeed))	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))  break;
				y += sign(yspeed);
			}
			yspeed = 0;
		}

		y += yspeed;
		
		//If sees a player
		if (gun != noone && instance_exists(oPlayer) && !collision_line(gun.x, gun.y, oPlayer.x, oPlayer.y, oBlock, true, true))
		{
			state = GoonStates.SHOOTING;	
		}
	
		break;
	
	case GoonStates.SHOOTING:
		
		if (!OnView(id))
		{
			state = GoonStates.WANDER;
			exit;
		}
		
		if (!instance_exists(oPlayer))
		{
			state = GoonStates.WANDER;
			exit;
		}
		
		//If doesn't see a player
		if (gun != noone && !instance_exists(oPlayer) || collision_line(gun.x, gun.y, oPlayer.x, oPlayer.y, oBlock, true, true))
		{
			state = GoonStates.WANDER;	
			exit;
		}
		
		
		//Slow down
		xspeed = sign(oPlayer.x - x) * abs(xspeed);
		xspeed = lerp(xspeed, 0, 0.1);
		
		if (abs(oPlayer.x - x) < 4)
			xspeed = 0;
		xspeed += xspeed_push;
		
			
		if (gun != noone && gun.state == GunStates.LOAD)
		{
			//Rotate the gun
			with (gun)
			{
				var _angle = point_direction(x, y, oPlayer.x, oPlayer.y);
				if (_angle > 90 && _angle < 270)	image_xscale = -1;
				else								image_xscale = 1;
				
				if (image_xscale > 0)	image_angle = point_direction(x, y, oPlayer.x, oPlayer.y);
				else					image_angle = 180 + point_direction(x, y, oPlayer.x, oPlayer.y);
				
			}
			image_xscale = gun.image_xscale;
		}
		
		timer_shoot--;
		if (timer_shoot == 0)
		{
			//Shoot
			timer_shoot = timer_shoot_max;
			gun.shoot = true;
			gun.hosted = id;
		
			
			if (gun.shot_number == 1)
			{
				//If reloading
				timer_shoot = timer_shoot_max + 60;
			}
		}
		
		//Moved by parasol
		with (instance_place(x, y, oParasol))
		{
			if ((state == ParasolStates.THROWN) && moving)
			{
				other.xspeed = xspeed * 2/3;
				other.state = GoonStates.CHASE;
				other.target_init = false;
				other.shoked = other.shoked_max * global.slow_motion_effect;
				if (other.gun != noone)
				{
					//Drop the gun
					StartSlowMotion(SLOW_MOTION_LENGTH);
					other.gun.state = GunStates.FREE;
					other.gun.host = noone;
					other.gun.yspeed = -other.jumpspeed;
					other.gun.y -= other.jumpspeed;
					other.gun = noone;
					break;
				}	
			}
		}
		
		//Interaction with the gun
		var _list = ds_list_create();
		var _count = instance_place_list(x, y, oGun, _list, true);
		
		repeat(_count)
		{
			var _gun = ds_list_find_value(_list, 0);
			with (_gun)
			{
				if (!other.shoked && id != other.gun && state == GunStates.FREE && abs(xspeed) + abs(yspeed) > 0.1 && host == noone)
				{
					other.xspeed = xspeed * 2/3;
					other.state = GoonStates.CHASE;
					other.target_init = false;
					other.shoked = other.shoked_max * global.slow_motion_effect;
					if (other.gun != noone)
					{
						//Drop the gun
						StartSlowMotion(SLOW_MOTION_LENGTH);
						other.gun.state = GunStates.FREE;
						other.gun.host = noone;
						other.gun.yspeed = -other.jumpspeed;
						other.gun.y -= other.jumpspeed;
						other.gun = noone;
						break;
					}
				}
			}
			ds_list_delete(_list, 0);
		}
		
		ds_list_destroy(_list);
		
		//Horizontal collision
		if (place_meeting(x + xspeed, y, oBlock))
		{
			repeat(abs(xspeed))	
			{
				if (place_meeting(x + sign(xspeed), y, oBlock))  break;
				x += sign(xspeed);
			}
			xspeed = 0;
		}
		x += xspeed;
		
		yspeed += global.grav;
		yspeed += yspeed_push;

		//Vertical collision
		if (place_meeting(x, y + yspeed, oBlock))
		{
			repeat(abs(yspeed))	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))  break;
				y += sign(yspeed);
			}
			yspeed = 0;
		}

		y += yspeed;
		
		break;
	
	default:
		Raise("Error in goon states\n");
		break;
}

//Dying
with (instance_place(x, y, oLaser))
{
	if (hosted != other.id)
	{
		if (OnView(other.id) && instance_number(oGoon) == 1)
		{
			if (instance_exists(oGoonsControl))
			{
				oGoonsControl.start_zoom = true;
				oGoonsControl.zoom_x = other.x;
				oGoonsControl.zoom_y = other.y;
				for (var i = 0; i < array_length(oGoonsControl.functions); ++i)
				{
					oGoonsControl.functions[i]();	
				}
			}
				
			//StartSlowMotion(SLOW_MOTION_LENGTH/3);
		}
		instance_destroy(other.id);
		destroy_it = true;
	}
}


//Dying
if (place_meeting(x, y, oSpike))
{
	instance_destroy();	
}

if (place_meeting(x, y, oGear))
{
	instance_destroy();	
}

//Jump sound
if (_jumped && OnView(id))
{
	var _s = audio_play_sound(soJump, 0, 0);
	audio_sound_pitch(_s, 0.7);
}

//Animation
if (place_meeting(x, y + 1, oBlock))
{
	//if grounded
	if (abs(xspeed) > 0.5)	sprite_index = sGoonWalk;
	else					sprite_index = sGoonIdle;
}
else
{
	sprite_index = sGoonJump;
	
	if (sign(yspeed) <= 0)
	{
		image_index = 0;
	}
	else
	{	
		image_index = 2;
	}
}

//Particles
if (xspeed != 0 || yspeed != 0)
{
	var _len = point_distance(x, y, xprevious, yprevious);
	var _ang = point_direction(xprevious, yprevious, x, y);
	var _x = xprevious;
	var _y = yprevious;
	
	for (var i = 0; i < _len; ++i)
	{	
		part_particles_create(global.part_system, _x + lengthdir_x(i, _ang), _y + lengthdir_y(i, _ang), trail, 1);
	}
}