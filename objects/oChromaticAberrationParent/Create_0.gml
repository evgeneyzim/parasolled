/// @description Set uniforms
u_blur_vector = shader_get_uniform(shdExtra, "blur_vector");
u_sigma = shader_get_uniform(shdExtra, "sigma");