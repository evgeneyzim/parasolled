draw_set_alpha(alpha);
draw_set_color(color);

for (var i = 0; i < 360; ++i)
{
	var _x1 = x + lengthdir_x(radius, i);
	var _y1 = y + lengthdir_y(radius, i);
	var _x2 = x + lengthdir_x(radius + thickness, i);
	var _y2 = y + lengthdir_y(radius + thickness, i);
	
	draw_line_width(_x1, _y1, _x2, _y2, radius / 8);	
}


//draw_circle(x, y, radius, true);



draw_set_alpha(1);