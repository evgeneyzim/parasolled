/// @description Set parametres
event_inherited();

randomize();

magnitude = 500000;
phase = irandom(2 * magnitude) - magnitude;

u_strength = shader_get_uniform(shdBrightness, "strength");
