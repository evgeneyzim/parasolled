///@decs Draw on surface

event_inherited();

if (surface_exists(oDrawer.block_surface))
{
	surface_set_target(oDrawer.block_surface);

	var _strength = abs(sin((get_timer() - phase)/magnitude)) * 0.05;
	shader_set(shdBrightness);
	shader_set_uniform_f(u_strength, _strength);

	//draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, 0, oDrawer.c_block, 1);

	DrawBlock(sprite_index, oDrawer.c_block);

	shader_reset();

	surface_reset_target();
}