/// @description Insert description here
// You can write your code in this editor

var _sprite_width = sprite_width;

if (start)
{
	_sprite_width = max(0, _sprite_width - 1);	
}
else
{
	_sprite_width = min(start_w, _sprite_width + 16);
	radius = 0;
	_score = 0;
}

image_xscale = _sprite_width / sprite_get_width(sprite_index);


radius -= spd;

if (spd == fast_spd)
{
	pivot += rotation_speed_max;	
}
else
{
	pivot += rotation_speed_min;	
}

if (radius <= 0 and bbox_right - bbox_left > 2 * offset)
{
	spd = normal_speed;
	radius = radius_max;
	xx = irandom_range(bbox_left + offset, bbox_right - offset);
	yy = irandom_range(bbox_top + offset, bbox_bottom - offset);
}

if (start && spd != fast_spd)
{
	if (instance_exists(oPlayer))
	{
		with (oPlayer)
		{
			if (distance_to_point(other.xx, other.yy) < other.radius)
			{
				other._score++;
				other.spd = other.fast_spd;
				other.radius = 1.4 * other.radius_max;
				audio_play_sound(soMinigame, 0, false);
			}
		}
	}
}

if (_score >= score_max)
{
	start = false;
	with (inst_A930FF4)  spd = 0;
	with (inst_6526476E) spd = 0;
	
	with (inst_426E61C6) start = true;
	with (inst_36F7DCA9) start = true;
	
	with (inst_36939824)	speed_bonus = 45;
}