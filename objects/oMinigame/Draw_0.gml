if (start)
{
	draw_set_color(oDrawer.c_spikes);
	draw_set_alpha(0.5);
	draw_circle(xx, yy, radius, false);
	
	
	draw_set_alpha(1);
	draw_circle(xx, yy, radius * 1.2, true);
	
	//drawing arrows
	if (spd != fast_spd)
	{
		for (var i = 0; i < amount_of_arrows; ++i)
		{
			var _angle = pivot + i / amount_of_arrows * 360;
			var _x = xx + lengthdir_x(radius * 1.2, _angle);
			var _y = yy + lengthdir_y(radius * 1.2, _angle);
		
			draw_sprite_ext(sArrow, 0, _x, _y, 1, 1, 180 + _angle, oDrawer.c_spikes, 0.7);
		
		}
	}
	
}