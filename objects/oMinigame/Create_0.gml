/// @description Insert description here
// You can write your code in this editor
start = false;

start_w = sprite_width;


xx = 0;
yy = 0;

radius_max = 50;
radius = 0;

offset = radius_max;
normal_speed = 0.5;
fast_spd = 2;

spd = normal_speed;

_score = 0;

score_max = 9;


amount_of_arrows = 5;
pivot = 0;
rotation_speed_max = 5;
rotation_speed_min = -3;

deactivator = function()
{
	with (inst_426E61C6) start = false;
	
	with (inst_36939824)	speed_bonus = 30;	
}