enum InputType
{
	KEYBOARD,
	GAMEPAD,
};

global.input_type = InputType.KEYBOARD;

prev_mouse_x = 0;
prev_mouse_y = 0;