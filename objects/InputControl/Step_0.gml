//Check what device is being used right now
if (keyboard_check_pressed(vk_anykey) || abs(prev_mouse_x - display_mouse_get_x()) + abs(prev_mouse_y - display_mouse_get_y()) > 2 || mouse_check_button_pressed(mb_any))
{
	global.input_type = InputType.KEYBOARD;	
}
if (GamepadAnykey(SelectGamepad(0)) || abs(gamepad_axis_value(SelectGamepad(0), gp_axislh)) > 0.3 || abs(gamepad_axis_value(SelectGamepad(0), gp_axislv)) > 0.3 || abs(gamepad_axis_value(SelectGamepad(0), gp_axisrh)) > 0.3 || abs(gamepad_axis_value(SelectGamepad(0), gp_axisrv)) > 0.3)
{
	global.input_type = InputType.GAMEPAD;	
}


prev_mouse_x = display_mouse_get_x();
prev_mouse_y = display_mouse_get_y();