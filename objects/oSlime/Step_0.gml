/// @description 

if (!part_init && oPartTypes.part_init)
{
	trail = oPartTypes.slime_trail;
}

//For sounds
var _jumped = false;

//Activate blocks around
instance_activate_region(x - abs(sprite_width)/2, y - abs(sprite_height)/2, abs(sprite_width), abs(sprite_height), true);

//Pushed by others
var _list = ds_list_create();

var _count = instance_place_list(x, y, oSlimeLike, _list, true);

repeat (_count)
{
	var _inst = ds_list_find_value(_list, 0);
	
	if (_inst != id)
	{
		var _dir = point_direction(_inst.x, _inst.y, x, y);
	
		xspeed_push += lengthdir_x(push_value, _dir);
		yspeed_push += lengthdir_y(push_value, _dir);
	}
		
	ds_list_delete(_list, 0);
}

ds_list_destroy(_list);

//If noone is near
if (!place_meeting(x, y, oSlimeLike))
{
	xspeed_push = lerp(xspeed_push, 0, 0.1);	
	yspeed_push = lerp(yspeed_push, 0, 0.1);	
}



//Actions
switch (state)
{
	case SlimeState.IDLE:
		//Idle state
		current_speed = charge_speed;
		yspeed += global.grav;
		xspeed = 0.001;
		
		//Moved by parasol
		with (instance_place(x, y, oParasol))
		{
			if ((state == ParasolStates.THROWN) && moving)
			{
				other.xspeed = xspeed;
			}
			else
			{
				other.xspeed = 0.001;	
			}
		}
		
		
		if (place_meeting(x + current_speed * direct, y, oSpike))
		{
			direct *= -1;
		}

		//If sees a hole
		if (place_free(x + (current_speed - sprite_width/2) * direct, y + 1))
		{
			direct *= -1;	
		}

		//if sees a obstacle
		if (place_meeting(x + current_speed * direct, y, oBlock))
		{
			direct *= -1;	
		}
	
		x += direct * current_speed + xspeed;
		
		//Vertical collision
		if (place_meeting(x, y + yspeed, oBlock))
		{
			repeat(yspeed)	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))  break;
				y += sign(yspeed);
			}
			yspeed = 0;
		}

		y += yspeed;
		
	
		break;
	
	
	case SlimeState.FIGHT:
		//Fight state
		current_speed = charge_speed;

		var _jumpspeed = 4 * jumpspeed/5;
		if (instance_exists(target))	_jumpspeed += ((target.y - y) < 32) * jumpspeed/5;

		if (instance_exists(target)) direct = sign(target.x - x);

		yspeed += global.grav;


		//Standart movements
		xspeed = lerp(xspeed, current_speed * direct, acceleration);
		xspeed += xspeed_push;



		//Moved by parasol
		with (instance_place(x, y, oParasol))
		{
			if ((state == ParasolStates.THROWN) && moving)
			{
				other.xspeed = xspeed;
			}
		}


		//Avioding the cube
		if (place_meeting(x, y, oCube))
		{
			var _flag = false;
			with (instance_place(x, y, oCube))	
			{
				if (yspeed > 0)
				{
					_flag = true;
				}
			}
			if (_flag)
			{
				while (place_meeting(x, y, oCube)) y++;	
			}
		}

		//Horizontal collision
		if (place_meeting(x + xspeed, y, oBlock))
		{
			repeat(abs(xspeed))	
			{
				if (place_meeting(x + sign(xspeed), y, oBlock))  break;
				x += sign(xspeed);
			}
			xspeed = 0;
		}

		x += xspeed;

		//If grounded
		if (place_meeting(x, y + 1, oBlock))
		{
			//Jumps if sees a spike
			if (place_meeting(x + 1.5 * current_speed * direct, y, oSpike))
			{
				yspeed = -_jumpspeed;
				_jumped = true;
			}

			//If sees a hole
			if (place_free(x + current_speed * direct, y + 1))
			{
				yspeed = -_jumpspeed;	
				_jumped = true;
			}

			//if sees a obstacle
			if (place_meeting(x + current_speed * direct, y, oBlock))
			{
				yspeed = -_jumpspeed;
				_jumped = true;
			}
		}

		//Vertical collision
		if (place_meeting(x, y + yspeed, oBlock))
		{
			repeat(abs(yspeed))	
			{
				if (place_meeting(x, y + sign(yspeed), oBlock))  break;
				y += sign(yspeed);
			}
		
			if (yspeed != global.grav)
			{
				with (instance_create_layer(x, bbox_bottom, "Items", oPlayerDroplets))
				{
					sprite_index = sSlimeParticles;	
				}
				audio_play_sound(soSlimeLanding, 0, false);
			}
			
			yspeed = 0;
			
		}

		y += yspeed;
		
		
		break;
		
	default:
		Raise("Error in oSlime states");
		break;
}

//Graphics
//image_xscale = sign(direct) * abs(image_xscale);
sprite_index = sSlimeWalk;


//Particles
if (xspeed != 0 || yspeed != 0)
{
	var _len = point_distance(x, y, xprevious, yprevious);
	var _ang = point_direction(xprevious, yprevious, x, y);
	var _x = xprevious;
	var _y = yprevious;
	
	for (var i = 0; i < _len; ++i)
	{	
		part_particles_create(global.part_system, _x + lengthdir_x(i, _ang), _y + lengthdir_y(i, _ang), trail, 1);
	}
}


//Dying
if (place_meeting(x, y, oSpike))
{
	if (instance_exists(oPlayer))	explode = true;
	else							explode = false;
	instance_destroy();	
}

with (instance_place(x, y, oLaser))
{
	if (instance_exists(oPlayer))	other.explode = true;
	else							other.explode = false;
	instance_destroy(other.id);
	destroy_it = true;
}

with (oBound)
{
	if (other.y > y)
	{
		instance_destroy(other.id);	
	}
}

if (_jumped && OnView(id))
{
	with (instance_create_layer(x, bbox_bottom, "Items", oPlayerDroplets))
	{
		sprite_index = sSlimeParticles;	
	}
	var _s = audio_play_sound(soJump, 0, 0);
	audio_sound_pitch(_s, 0.7);
}
