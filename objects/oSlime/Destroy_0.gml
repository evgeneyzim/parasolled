/// @description 
if (explode)
{
	ShakeScreen(40, 30);
	
	repeat (40)
	{
		with (instance_create_layer(x, y, "Bullets", oPlayerSparkle))
		{
			color = other._image_blend;
		}
	}

	with (instance_create_layer(x, y, "Bullets", oPlayerSparkle))
	{
		speed = 0;
		color = other._image_blend;
		size = 30;
	}
	
	if (OnView(id))
	{
		var _s = audio_play_sound(soDying, 1, 0);	
		audio_sound_pitch(_s, 0.7);
	}

}

for (var i = 0; i < array_length(functions); ++i)
{
	functions[i]();	
}