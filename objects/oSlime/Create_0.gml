/// @description 

event_inherited();

randomize();

xspeed = 0;
yspeed = 0;

state = SlimeState.FIGHT;

//Speed params
walkspeed = 3;
charge_speed = 4;
current_speed = walkspeed;

jumpspeed = 14;

direct = choose(-1, 1);
acceleration = 0.1;



//Timer for changing direction in idle mode
idle_timer_module = 150;
idle_timer = idle_timer_module * random_range(0.7, 1.3);

explode = true;

//target
target = oPlayer;

//functions after death
functions =  [];

//Push
xspeed_push = 0;
yspeed_push = 0;

push_value = 0.25;

_image_blend = c_red;

//Appearing
appear_alpha = 1;
appear_alpha_dec = 0.02;

u_strength = shader_get_uniform(shdBrightness, "strength");

with (instance_create_layer(x, y, "Particles", oPlayerCreateCircle))
{
	color = c_red;	
}


//Particles

part_init = false;

u_blur_vector = shader_get_uniform(shdExtra, "blur_vector");
u_sigma = shader_get_uniform(shdExtra, "sigma");


DrawDuringTheJump = function()
{
	var _angle = darctan2(-yspeed, xspeed);
	var _xscale = 1;
	
	_angle = (_angle + 360) % 360;
	
	
	//if (_angle == 270)	_angle = 90;
	
	if (_angle > 90 && _angle < 270)
	{
		_xscale = -1;
		_angle -= 180;
	}
	draw_sprite_ext(sSlimeJump, image_index, x, y, _xscale, image_yscale, _angle, c_white, image_alpha);
}