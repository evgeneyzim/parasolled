/// @description 

if (appear_alpha > 0)
{
	shader_set(shdBrightness);
	shader_set_uniform_f(u_strength, appear_alpha);

	
	appear_alpha -= appear_alpha_dec;
	draw_set_alpha(1 - appear_alpha);
}


if (yspeed == 0)
	draw_self();
else
	DrawDuringTheJump();

if (appear_alpha > 0)
{
	draw_set_alpha(1);
	shader_reset();
	
	gpu_set_blendmode(bm_add);
	draw_sprite_ext(sAppear, 0, x, y, 1 + 2 * appear_alpha, 1 + 2 * appear_alpha, 0, c_red, 1);
	gpu_set_blendmode(bm_normal);
		
}

