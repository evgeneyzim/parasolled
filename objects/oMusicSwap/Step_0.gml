if (swap_to != -1 && !mus_change)
{
	mus_change = true;
	with (oDeeJay)
	{
		silence_timer = MUS_INTRO_LENGTH;
		bgm = other.swap_to;	
		musNext = musBoss1;
	}
}