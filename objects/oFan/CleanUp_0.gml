if (created)
{
	part_system_clear(global.part_system_const);	
}

if (sound != undefined)
{
	audio_stop_sound(sound);	
}

if (particles_init)
{
	part_type_destroy(air);	
}