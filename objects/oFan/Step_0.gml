/// @description
if (!active) 
{
	if (sound != undefined) 
		audio_sound_gain(sound, 0, 500);
	exit;
}

//Initialize size variables
var _width = sprite_width, _height = sprite_height;


//Set particles
if (!particles_init)
{
	particles_init = true;
	
	air = part_type_create();
	part_type_shape(air, pt_shape_line);
	part_type_size(air, 0.1, 0.5, -0.00, 0);
	part_type_color1(air, merge_color(c_blue, c_white, 0.7));
	part_type_alpha2(air, 0.5, 0.7);
	part_type_speed(air, min(4 * speed_bonus / 20, 15), min(8 * speed_bonus / 20, 30), 0, 0);
	part_type_direction(air, 90, 90, 0, 0);
	
	var _magic_number = (room == rTeapot) ? 8 : 6;
	var _life = _height / (_magic_number * speed_bonus/20);
	
	part_type_life(air, _life, _life);
	part_type_orientation(air, image_angle + 90, image_angle + 90, 0, 0, false);
	part_type_direction(air, image_angle + 90, image_angle + 90, 0, 0);
}


//Interaction with parasol

if (instance_exists(oPlayer) && oPlayer.parasol != noone)
{
	var _new_x = oPlayer.x - x;	
	var _new_y = y - oPlayer.y;	
	
	var _angle = image_angle + 90;
	
	var _final_x = -floor(_new_x * (-dsin(_angle)) + _new_y * dcos(_angle));
	var _final_y = floor(_new_x * (dcos(_angle)) + _new_y * dsin(_angle));
	
	var _final_angle = _angle;
	
	if (abs(image_angle) > 5)  _final_angle += _final_x/(_width/2) * _angle/5;
	
	
	if (_final_y >= 0 && abs(_final_x) <= _width/2)
	{
		var _phase = amplitude * sin(get_timer()/500000);
		with (oPlayer.parasol)
		{
			res_xspeed -= lengthdir_x((other.speed_bonus + _phase) * (_height - _final_y)/_height, _final_angle);
			res_yspeed -= lengthdir_y((other.speed_bonus +_phase) * (_height - _final_y)/_height, _final_angle);		
		}
	}
	
}

//Spawn particles
for (var i = 0; i < 3; ++i)
{
	
	var _coef = 2 + i;
	
	var _xx = -_width/_coef;
	var _yy = 0;
	
	var _angle = image_angle + 90;
	
	var _real_x = dsin(_angle) * _xx + dcos(_angle) * _yy + x;
	var _real_y = -dcos(_angle) * _xx + dsin(_angle) * _yy + y;
	
	var _min_x = _real_x;
	var _min_y = _real_y;
	
	_xx = _width/_coef;
	_yy = 0;
	
	_real_x = dsin(_angle) * _xx + dcos(_angle) * _yy + x;
	_real_y = -dcos(_angle) * _xx + dsin(_angle) * _yy + y;
	
	var _max_x = _real_x;
	var _max_y = _real_y;
		
	part_particles_create(global.part_system_const, irandom_range(min(_min_x, _max_x), max(_min_x, _max_x)), irandom_range(min(_min_y, _max_y), max(_min_y, _max_y)), air, 1);
}

if (room == rMenu)	exit;
//Sound
if (OnView(id) && (sound == undefined || audio_sound_get_gain(sound) == 0))
{
	if (sound != undefined)
	{
		audio_stop_sound(sound);	
	}
	sound = audio_play_sound(soFan, 1, 1);	
	var _gain = audio_sound_get_gain(sound);
	audio_sound_gain(sound, 0, 0);
	audio_sound_gain(sound, _gain, 0);
}

if (!OnView(id))
{
	if (sound != undefined)
	{
		audio_sound_gain(sound, 0, 500);	
	}
}