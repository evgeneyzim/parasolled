x += lengthdir_x(speed, dir);
y += lengthdir_x(speed, dir);

part_particles_create(global.part_system, x - 8 * random(16), y - 8 * random(16), oExplotionController.explotion_particle_part, 1); 
part_particles_create(global.part_system, x - 8 * random(16), y - 8 * random(16), oExplotionController.smoke_particle_part, 1); 

if (speed == 0)
{
	instance_destroy();	
}