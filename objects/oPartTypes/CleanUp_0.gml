if (part_init)
{
	part_type_destroy(block_death);
	part_type_destroy(slime_trail);
	part_type_destroy(aim_trail);
	part_type_destroy(beam_trail);
	part_type_destroy(player_trail);
	part_type_destroy(parasol_trail);
	part_type_destroy(boss_death);
	part_type_destroy(parasol_death);
	part_type_destroy(laser_explotion);
}