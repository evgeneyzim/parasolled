if (!part_init && room != rLanguage)
{
	part_init = true;
	
	block_death = part_type_create();
	part_type_sprite(block_death, sEnemy, false, false, false)
	part_type_size(block_death, 0.3, 0.5, -0.005, 0);
	part_type_color1(block_death, merge_color(oDrawer.c_destruct_blocks, c_white, 0.2));
	part_type_alpha3(block_death, 0.5, 0.7, 0.9);
	part_type_speed(block_death, 10, 20, 0, 0);
	part_type_direction(block_death, 0, 359, 0, 0);
	part_type_life(block_death, 60, 60);
	part_type_gravity(block_death, 0.7, 270);
	
	
	slime_trail = part_type_create();
	part_type_shape(slime_trail, pt_shape_sphere);
	part_type_size(slime_trail, 0.4, 0.4, -0.005, 0);
	part_type_color1(slime_trail, merge_color(c_red, c_white, 0.5));
	part_type_alpha1(slime_trail, 0.1);
	part_type_speed(slime_trail, 0, 0, -0.10, 0);
	part_type_direction(slime_trail, 0, 359, 0, 20);
	part_type_life(slime_trail, 60, 60);
	
	aim_trail = part_type_create();
	part_type_shape(aim_trail, pt_shape_sphere);
	part_type_size(aim_trail, 0.4, 0.4, -0.005, 0);
	part_type_color1(aim_trail, merge_color(merge_color(oDrawer.c_spikes, c_orange, 0.3), c_gray, 0.45));
	part_type_alpha3(aim_trail, 0.2, 0.1, 0);
	part_type_speed(aim_trail, 0, 0, -0.10, 0);
	part_type_direction(aim_trail, 0, 359, 0, 20);
	part_type_life(aim_trail, 1200, 1200);
	
	boss_death = part_type_create();
	part_type_sprite(boss_death, sEnemy, false, false, false)
	part_type_size(boss_death, 0.5, 0.8, -0.005, 0);
	part_type_color1(boss_death, merge_color(merge_color(merge_color(oDrawer.c_spikes, c_orange, 0.3), c_gray, 0.45), c_red, 0.75));
	part_type_alpha2(boss_death, 0.3, 0.5);
	part_type_speed(boss_death, 10, 20, 0, 0);
	part_type_direction(boss_death, 0, 359, 0, 0);
	part_type_life(boss_death, 60, 60);
	part_type_gravity(boss_death, global.grav, 270);
	
	beam_trail = part_type_create();
	part_type_shape(beam_trail, pt_shape_sphere);
	part_type_size(beam_trail, 0.15, 0.15, -0.001, 0);
	part_type_color1(beam_trail, merge_color(c_red, c_white, 0.5));
	part_type_alpha1(beam_trail, 0.15);
	part_type_speed(beam_trail, 0, 0, -0.10, 0);
	part_type_direction(beam_trail, 0, 359, 0, 20);
	part_type_life(beam_trail, 15, 15);
	
	player_trail = part_type_create();
	part_type_shape(player_trail, pt_shape_sphere);
	part_type_size(player_trail, 0.4, 0.4, -0.005, 0);
	part_type_color1(player_trail, merge_color(oDrawer.c_player, c_white, 0.5));
	part_type_alpha1(player_trail, 0.1);
	part_type_speed(player_trail, 0, 0, -0.10, 0);
	part_type_direction(player_trail, 0, 359, 0, 20);
	part_type_life(player_trail, 60, 60);	
	
	parasol_trail = part_type_create();
	part_type_shape(parasol_trail, pt_shape_sphere);
	part_type_size(parasol_trail, 0.3, 0.3, -0.005, 0);
	part_type_color1(parasol_trail, merge_color(oDrawer.c_parasol, c_white, 0.5));
	part_type_alpha1(parasol_trail, 0.15);
	part_type_speed(parasol_trail, 0, 0, -0.10, 0);
	part_type_direction(parasol_trail, 0, 359, 0, 20);
	part_type_life(parasol_trail, 60, 60);

	parasol_death = part_type_create();
	part_type_sprite(parasol_death, sEnemy, false, false, false)
	part_type_size(parasol_death, 0.2, 0.4, -0.005, 0);
	part_type_color1(parasol_death, merge_color(oDrawer.c_parasol, c_white, 0.6));
	part_type_alpha3(parasol_death, 0.5, 0.7, 0.9);
	part_type_speed(parasol_death, 10, 20, 0, 0);
	part_type_direction(parasol_death, 0, 359, 0, 0);
	part_type_life(parasol_death, 60, 60);
	part_type_gravity(parasol_death, 0.7, 270);


	laser_explotion = part_type_create();
	part_type_shape(laser_explotion, pt_shape_sphere);
	part_type_size(laser_explotion, 0.1, 0.3, 0, 0);
	part_type_color3(laser_explotion, c_red, merge_color(c_red, c_white, 0.3), merge_color(c_red, c_white, 0.5));
	part_type_alpha2(laser_explotion, 0.7, 0.4);
	part_type_speed(laser_explotion, 5, 8, 0, 0);
	part_type_direction(laser_explotion, 0, 359, 0, 0);
	part_type_life(laser_explotion, 30, 30);
	part_type_gravity(laser_explotion, global.grav, 270);
	
	
}