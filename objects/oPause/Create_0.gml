/// @description
paused = false;

saved_room_speed = 0;

//For greyscale
time = 0;
u_time = shader_get_uniform(shdGreyScale, "time");

//For box
border = 0;
sound_border = 0;
alpha = 0;
alpha_max = 0.85;

option = 0;


//For chosen option
angle = 0;
angle_max = 3.5;
scale = 1.2;
scale_avr = 1.2;
scale_bounce = 0.05;

//Options
text = [GetText(12), GetText(30), GetText(36), GetText(3)];

//Sound menu
sound_menu = false;
sound_text = [GetText(37), GetText(38), GetText(8)];
sound_option = 0;

length = array_length(text);

offset = display_get_gui_height() / 7;
box_width = display_get_gui_width() * 2 / 7;

//Extra
need_to_pause = false;