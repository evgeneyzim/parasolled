/// @description

if (paused) 
{
	//Draw screenshot
	shader_set(shdGreyScale);
	shader_set_uniform_f(u_time, time);
	if (!surface_exists(screen_surface))
	{
		screen_surface = surface_create(display_get_gui_width(), display_get_gui_height());
		surface_set_target(screen_surface);
		draw_sprite(sprite, 0, 0, 0);
		surface_reset_target();
	}
	draw_surface_stretched(screen_surface, 0, 0, display_get_gui_width(), display_get_gui_height());
	shader_reset();
	
	//Draw black box
	draw_set_color(c_black);
	draw_set_alpha(alpha);
	draw_rectangle(0, 0, border, display_get_gui_height(), false);
	draw_set_alpha(1);
	
	
	//Draw text
	draw_set_font(fontTitle);
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	
	var _y = offset * 2;
	var _x = box_width / 2;
	
	var _scale, _angle;
	
	for (var i = 0; i < length; ++i)
	{
		if (option == i && !sound_menu)
		{
			draw_set_color(c_white);
			_scale = scale;
			_angle = angle;
		}
		else
		{
			draw_set_color(merge_color(c_white, c_gray, 0.9));
			_scale = 1;
			_angle = 0;
		}
		var _k = display_get_gui_width() / 1280;
		draw_text_ext_transformed(_x, _y, text[i], 1, 300, _scale * _k, _scale * _k, _angle);
		_y += offset;
	}
	
	
	//Draw sounds box
	draw_set_color(c_black);
	draw_set_alpha(alpha);
	draw_rectangle(border + 1, 13/4 * offset, border + sound_border, 21/4 * offset, false);
	draw_set_alpha(1);
	
	if (sound_menu && abs(sound_border - box_width) < 0.1)
	{
		//Draw sound text
		draw_set_font(fontTitle);
		draw_set_color(c_white);
		draw_set_halign(fa_center);
	
		var _y = offset * 7/2;
		var _x = box_width * 3/2;
	
		var _scale, _angle;
	
		for (var i = 0; i < array_length(sound_text); ++i)
		{
			var _option = sound_text[i];
			if (i == 0)
			{
				_option += " " + string(round(200 * global.music_volume)) + "%";	
			}
			if (i == 1)
			{
				_option += " " + string(round(100 *global.sfx_volume)) + "%";	
			}
		
			if (sound_option == i)
			{
				draw_set_color(c_white);
				_scale = scale;
				_angle = angle;
			}
			else
			{
				draw_set_color(merge_color(c_white, c_gray, 0.9));
				_scale = 1;
				_angle = 0;
			}
			var _k = display_get_gui_width() / 1280;
			draw_text_ext_transformed(_x, _y, _option, 1, 300, _scale * _k, _scale * _k, _angle);
			_y += offset/2;
		}
	}
}


