/// @description

if (room == rThanksForPlaying || room == rSpeedrunResults || room == rEnd)
{
	exit;	
}

if (instance_exists(oChaser) && oChaser.in_sequence)
{
	exit;	
}
	

var _left = keyboard_check_pressed(vk_left) || keyboard_check_pressed(ord("A")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padl);
var _right = keyboard_check_pressed(vk_right) || keyboard_check_pressed(ord("D")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padr);
var _up = keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padu);
var _down = keyboard_check_pressed(vk_down) || keyboard_check_pressed(ord("S")) || gamepad_button_check_pressed(SelectGamepad(0), gp_padd);

var _gam = SelectGamepad(0);
if (gamepad_is_connected(_gam))
{
	input = new GamepadInput(_gam);
	
	_left |= input.left;
	_right |= input.right;
	_up |= input.up;
	_down |= input.down;
	
	delete input;
}


if (gamepad_is_connected(SelectGamepad(0)))
{
	text = [GetText(12), GetText(30), GetText(36), GetText(42), GetText(3)];
	
	var _num;
	if (global.vibration)	_num = 40;
	else					_num = 41;
	
	text[3] += " " + GetText(_num);
	
	length = array_length(text);

	offset = display_get_gui_height() / 8;
	
	vibration_option = 3;
	exit_option = 4;
}
else
{
	text = [GetText(12), GetText(30), GetText(36), GetText(3)];	
	length = array_length(text);

	offset = display_get_gui_height() / 7;
	
	vibration_option = -1;
	exit_option = 3;
}


switch (paused)
{
	case false:
			
		if (need_to_pause || keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_start))
		{
					
			saved_room_speed = room_speed;
			room_speed = NORMAL_SPEED;
			need_to_pause = false;
			if (global.glitch_intensity != 0)
			{
				need_to_pause = true;
				exit;
			}
			
			paused = true;
			
			if (instance_exists(oParasol))
			{
				with (oParasol)
				{
					if (state != ParasolStates.THROWN && state != ParasolStates.START)
					{
						state = ParasolStates.CLOSED;	
					}
				}
			}
			
			if (gamepad_is_connected(SelectGamepad(0)))
			{
				gamepad_set_vibration(SelectGamepad(0), 0, 0);	
			}
			
			//Take screenshot and save it as sprite
			screen_save("screenshot.png");
			sprite = sprite_add("screenshot.png", 0, false, false, 0, 0);
			
			//Create surface and draw sprite here
			screen_surface = surface_create(display_get_gui_width(), display_get_gui_height());
			
			surface_set_target(screen_surface);
			draw_sprite(sprite, 0, 0, 0);
			surface_reset_target();
			
			//Delete screenshot
			file_delete("screenshot.png");
	
			//Stop particles
			part_system_automatic_update(global.back_pattern, false);
			part_system_automatic_update(global.part_system, false);
			part_system_automatic_update(global.part_system_const, false);
			part_system_automatic_update(global.enemy_part, false);
	
			var _activate_speedrunner = false;
			if (instance_exists(oSpeedrunner)) _activate_speedrunner = true;
	
			instance_deactivate_all(true);
			instance_activate_object(oDeeJay);
			if (_activate_speedrunner)
				instance_activate_object(oSpeedrunner);
			
			//Stop sound effects
			audio_pause_sound(soSlide);
			audio_pause_sound(soFan);
	
			time = 0;
			option = 0;	
			sound_option = 0;	
		}
	
		break;
		
	case true:
	default:
	
		
		if (!sound_menu)
		{
			//Control
			if (_down)  
			{
				option = (option + 1) % length;
				audio_play_sound(soMenuClick, 1, 0);
			}
			if (_up) 
			{
				option = (option + length - 1) % length;
				audio_play_sound(soMenuClick, 1, 0);
			}
		}
		else
		{
			//Control
			if (_down)  
			{
				sound_option = (sound_option + 1) % array_length(sound_text);
				audio_play_sound(soMenuClick, 1, 0);
			}
			if (_up) 
			{
				sound_option = (sound_option + array_length(sound_text) - 1) % array_length(sound_text);
				audio_play_sound(soMenuClick, 1, 0);
			}	
		}
		
		
		if (!sound_menu)
		{
			var _var = false;
			if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_face2) || gamepad_button_check_pressed(SelectGamepad(0), gp_start))
			{
				option = 0;
				sound_option = 0;
				_var = true;
				sound_menu = false;
			}
		
			if (_var || keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(SelectGamepad(0), gp_face1))
			{
				audio_play_sound(soMenuClick, 1, 0);
			
				if (option != 2 && option != vibration_option)
				{
					room_speed = saved_room_speed;
					paused = false;
		
					//free surface
					surface_free(screen_surface);
		
					instance_activate_all();
			
					//Resume particles
					part_system_automatic_update(global.back_pattern, true);
					part_system_automatic_update(global.part_system, true);
					part_system_automatic_update(global.part_system_const, true);
					part_system_automatic_update(global.enemy_part, true);
						
					border = 0;
					sound_border = 0;
					alpha = 0;	
			
					sprite_delete(sprite);
					sound_menu = false;
				}
			
				switch (option)
				{
					case 0:
					default:
						//Resume sound effects
						audio_resume_sound(soSlide);
						audio_resume_sound(soFan);
												
						break;
					
					case 1:
						RestartLevel(false, false);	
						audio_resume_sound(soFan);
						break;
					
					case 2:
						sound_menu = true;
						break;
					
					case vibration_option:
						global.vibration = global.vibration xor 1;
						if (global.vibration && gamepad_is_connected(SelectGamepad(0)))
						{
							gamepad_set_vibration(SelectGamepad(0), 10, 10);
							alarm[0] = room_speed/4;
						}
						SaveGame();
						break;
				
					case exit_option:
						RestartParticles();
						room_goto(rMenu);
						break;
				}
			}
		}
		else
		{		
		
			if (sound_option == 0)
			{
				if (_left || _right)	audio_play_sound(soMenuClick, 1, 0);		
				global.music_volume = clamp(global.music_volume + (_right - _left) * 0.05, 0, 0.5);
				SetGain(GetMusic(), global.music_volume);
				SaveGame();
			}
			else if (sound_option == 1)
			{
				if (_left || _right)	audio_play_sound(soMenuClick, 1, 0);		
				global.sfx_volume = clamp(global.sfx_volume + (_right - _left) * 0.1, 0, 1);
				SetGain(GetSFX(), global.sfx_volume);	
				SaveGame();
			}
			
			
			
			if (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(SelectGamepad(0), gp_face2) || gamepad_button_check_pressed(SelectGamepad(0), gp_start))
			{
				paused = false;
				room_speed = saved_room_speed;
		
				//free surface
				surface_free(screen_surface);
		
				instance_activate_all();
			
				//Resume particles
				part_system_automatic_update(global.back_pattern, true);
				part_system_automatic_update(global.part_system, true);
				part_system_automatic_update(global.part_system_const, true);
				part_system_automatic_update(global.enemy_part, true);
			
				border = 0;
				sound_border = 0;
				alpha = 0;	
			
				sprite_delete(sprite);
				
				sound_menu = false;
				
				//Resume sound effects
				audio_resume_sound(soSlide);
				audio_resume_sound(soFan);
			}
		
			if (keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(SelectGamepad(0), gp_face1))
			{
				audio_play_sound(soMenuClick, 1, 0);
			
							
				switch (sound_option)
				{
					default:
						break;
										
					case 2:
						sound_menu = false;
						sound_option = 0;
						break;
				
				}
			}	
		}
		
		time = min(++time, 20);
	
		alpha = lerp(alpha, alpha_max, 0.3);	
		border = lerp(border, box_width, 0.3);
		
		if (sound_menu)
			sound_border = lerp(sound_border, box_width, 0.3);
		else
			sound_border = lerp(sound_border, 0, 0.3);
	
		angle = angle_max * sin(get_timer()/250000);
		scale = scale_avr + scale_bounce * sin(get_timer()/500000);	
		
	
		break;
}