/// @description

event_inherited();

target = oPlayer;
acceleration = 1;
speed = 2;

rotation_speed = 3.5;
dumpimg = 0.3;

absolute_speed = 15;
max_speed = 20;

//Time 'till it stops aiming the player
remain_turnes = 30;

_color = c_red;
with (oBoss)
{
	other._color = c_boss;	
}


//Smoke trail
part_init = false;

