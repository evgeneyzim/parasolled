if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	trail = oPartTypes.aim_trail;
}

//if player is dead
if (!instance_exists(target) || place_meeting(x, y, oBlock))
{
	instance_destroy();
	exit;
}

//If prevented by parasol-shield
with (instance_place(x, y, oParasol))
{
	if (state == ParasolStates.SHIELD)	
	{
		instance_destroy(other.id);
	}
}

//If it can rotate
if (remain_turnes > 0)
{
	var _target_direction = point_direction(x, y, (target.bbox_right + target.bbox_left)/2, target.bbox_top);
	direction -= clamp(angle_difference(direction, _target_direction) * dumpimg, -rotation_speed, rotation_speed);
	remain_turnes--;
}
else
{
	//else accelerate
	absolute_speed = min(absolute_speed + 1, max_speed);
}
	
//rotation
var _new_direction = darctan2(speed * dsin(direction) + remain_speed * dsin(started_direction), speed * dcos(direction) + remain_speed * dcos(started_direction));
speed = power(speed * dcos(direction) + remain_speed * dcos(started_direction), 2) + power(speed * dsin(direction) + remain_speed * dsin(started_direction), 2);
direction = _new_direction;
remain_speed = max(remain_speed - 1, 0);

image_angle = direction;
speed = min(speed + acceleration, absolute_speed);


//Smoke
//part_particles_create(global.part_system_const, x, y, trail, 1);
var len = speed;
var ang = direction;
var _x = xprevious;
var _y = yprevious;
	

for (var i = 0; i < len; ++i)
{	
	part_particles_create(global.part_system_const, _x + lengthdir_x(i, ang), _y + lengthdir_y(i, ang), trail, 1);
}

