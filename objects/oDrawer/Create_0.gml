/// @description Work with colors

//Particles
InitializeParticles();

randomize();

//Set colors
c_block = merge_color(merge_color(0xada3c0, c_black, 0.4), c_purple, 0.15);//merge_color(0xada3c0, c_black, 0.4);
c_outline = merge_color(c_block, c_orange, 0.9);
c_special_block = merge_color(merge_color(c_outline, c_orange, 0.3), c_orange, 0.3); //merge_color(c_outline, c_orange, 0.3);
c_outline = merge_color(c_special_block, c_gray, 0.3)//c_special_block;
c_background =  merge_color(0x685272, c_black, 0.4);
c_player = merge_color(c_green, c_aqua, 0.1);
c_parasol = make_color_rgb(141, 90, 157);
c_spikes = merge_color(merge_color(c_outline, c_special_block, 0.4), c_red, 0.3);//merge_color(c_outline, c_special_block, 0.4);
c_gear = c_spikes;
c_squares = merge_color(c_blue, c_black, 0.85);
c_destruct_blocks = merge_color(merge_color(merge_color(merge_color(c_special_block, c_olive, 0.75), c_black, 0.5), c_white, 0.3), c_yellow, 0.5);
c_box = merge_color(merge_color(c_lime, c_aqua, 0.75), c_black, 0.3);
c_turrel = merge_color(c_white, c_black, 0.75);

//Give colors
with (oSpike)			image_blend = oDrawer.c_spikes;
with (oParasol)			image_blend = oDrawer.c_parasol;
with (oGear)			image_blend = oDrawer.c_gear;
with (oStaticSquare)    image_blend = oDrawer.c_squares;
with (oTurrel)			image_blend = oDrawer.c_turrel;

//Set uniforms
u_blur_vector = shader_get_uniform(shdExtra, "blur_vector");
u_sigma = shader_get_uniform(shdExtra, "sigma");
u_strength = shader_get_uniform(shdBrightness, "strength");

//Set transition variables
level_end = false;
max_bright = 0.6;
bright = max_bright;

//Set background
layer_set_visible(layer_get_id("Background"), true);
layer_background_blend(layer_background_get_id("Background"), c_background );

//Draw outlines for normal walls
outline_width = 6;
outline_surface = surface_create(room_width, room_height);
surface_set_target(outline_surface);
DrawOutlines(oWall, c_outline, outline_width);

//Draw outlines for special walls
DrawOutlinesSpecial(oSpecialWall, c_outline, outline_width);
surface_reset_target();

//Surfaces for blocks
block_surface = surface_create(room_width, room_height);

//Set background particles

//Circle
part_type_backPattern_circle = part_type_create();
part_type_shape(part_type_backPattern_circle, pt_shape_circle);
part_type_color1(part_type_backPattern_circle, merge_color(oDrawer.c_background, c_white, 0.03) );
part_type_life(part_type_backPattern_circle, 45000, 45000);
part_type_direction(part_type_backPattern_circle, 10, 80, 0, 0 );
part_type_speed(part_type_backPattern_circle, 0.2, 0.4, 0, 0 );
part_type_size(part_type_backPattern_circle, 1, 1, 0, 0);

//Square
part_type_backPattern_square = part_type_create();
part_type_sprite(part_type_backPattern_square, sSquarePart, false, false, false)
part_type_color1(part_type_backPattern_square, merge_color(oDrawer.c_background, c_white, 0.03) );
part_type_life(part_type_backPattern_square, 45000, 45000);
part_type_direction(part_type_backPattern_square, 10, 80, 0, 0 );
part_type_speed(part_type_backPattern_square, 0.2, 0.4, 0, 0 );
part_type_size(part_type_backPattern_square, 1, 1, 0, 0);
part_type_orientation(part_type_backPattern_square, 0, 359, 0.1, 0.05, 0);

//Star
part_type_backPattern_star = part_type_create();
part_type_shape(part_type_backPattern_star, pt_shape_star);
part_type_color1(part_type_backPattern_star, merge_color(oDrawer.c_background, c_white, 0.03) );
part_type_life(part_type_backPattern_star, 45000, 45000);
part_type_direction(part_type_backPattern_star, 10, 80, 0, 0 );
part_type_speed(part_type_backPattern_star, 0.2, 0.4, 0, 0 );
part_type_size(part_type_backPattern_star, 1, 1, 0, 0);
part_type_orientation(part_type_backPattern_star, 0, 359, 0.05, 0.02, 0);

//Gear
part_type_backPattern_gear = part_type_create();
part_type_sprite(part_type_backPattern_gear, sGearAssebled, false, false, false);
part_type_color1(part_type_backPattern_gear, merge_color(oDrawer.c_background, c_white, 0.03) );
part_type_life(part_type_backPattern_gear, 45000, 45000);
part_type_direction(part_type_backPattern_gear, 10, 80, 0, 0 );
part_type_speed(part_type_backPattern_gear, 0.2, 0.4, 0, 0 );
part_type_size(part_type_backPattern_gear, 1, 1, 0, 0);
part_type_orientation(part_type_backPattern_gear, 0, 359, 0.2, 0.07, 0);

//Create particles
var _amount = (room_width * room_height) / (256 * 128);

repeat(_amount/3)  
{
	part_particles_create( global.back_pattern, random( room_width ), random( room_height ), part_type_backPattern_circle, 1 );
	part_particles_create( global.back_pattern, random( room_width ), random( room_height ), part_type_backPattern_square, 1 );
	part_particles_create( global.back_pattern, random( room_width ), random( room_height ), part_type_backPattern_star, 1 );
	//part_particles_create( global.back_pattern, random( room_width ), random( room_height ), part_type_backPattern_gear, 1 );
}

timer_max = 100;
timer = timer_max;

//Disable application surface
application_surface_draw_enable(false);

//Glitch
BktGlitch_init();
seed = random(1);

//Surfaces backup
outline_sprite = sprite_create_from_surface(outline_surface, 0, 0, room_width, room_height, false, false, 0, 0);

//Timer to save the block surface
invoke_blocks = 2;

blocks_sprite = sDemo;