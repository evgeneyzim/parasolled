/// @description Draw application surface

if (global.glitch_intensity > 0)
{
	shader_set(shdBktGlitch);
	SetGlitch(global.glitch_intensity);
}


if (bright > 0)
{
	shader_set(shdBrightness);
	shader_set_uniform_f(u_strength, bright);
}

bright = clamp(bright, 0, max_bright);

gpu_set_blendenable(false);
draw_surface_ext(application_surface, 0, 0, 1, 1, 0, c_white, 1);

//Increase or reduce the brightness of the application surface
if (level_end)  bright += 0.02;
else            bright -= 0.02;


shader_reset();
gpu_set_blendenable(true);

//If the brightness is maxed out, then go to the next room
if (level_end && bright >= max_bright)
{
	RestartParticles();
	if (room != rEnd)
	{
		if (global.progress < GetNumberFromLevel(room))  global.progress++;
		SaveGame();
		
		if (GetNumberFromLevel(room) > 2)
		{
			with (oDeeJay)
			{
				bgm = musMus;	
			}
		}
		room_goto_next();	
	}
	else
	{
		room_goto(rMenu);	
	}
}
