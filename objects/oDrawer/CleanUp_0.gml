/// @description Enable application surface drawings, free everything
application_surface_enable(true);

if (surface_exists(outline_surface))			surface_free(outline_surface);
if (surface_exists(block_surface))				surface_free(block_surface);

if (sprite_exists(outline_sprite))			sprite_delete(outline_sprite);
if (sprite_exists(blocks_sprite) && blocks_sprite != sDemo)
											sprite_delete(blocks_sprite);
											
part_type_destroy(part_type_backPattern_circle);
part_type_destroy(part_type_backPattern_square);
part_type_destroy(part_type_backPattern_star);
part_type_destroy(part_type_backPattern_gear);