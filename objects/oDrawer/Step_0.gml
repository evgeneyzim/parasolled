/// @description Spawn background pattern
if (!timer)
{
	var _type = choose(part_type_backPattern_circle, part_type_backPattern_star, part_type_backPattern_square)//, part_type_backPattern_gear);
	
	part_particles_create(global.back_pattern, random(room_width/3) - room_width/3, room_height -  random(room_height/2), _type, 1);
	part_particles_create(global.back_pattern, random(room_width/2), room_height + random(room_height/3), _type, 1);
	
	timer = timer_max;
}
else  timer--;

//Save blocks surface
if (invoke_blocks > 0)
{
	invoke_blocks--;
}
else if (invoke_blocks == 0 && surface_exists(block_surface))
{
	blocks_sprite = sprite_create_from_surface(block_surface, 0, 0, room_width, room_height, false, false, 0, 0);
	invoke_blocks--;
}

