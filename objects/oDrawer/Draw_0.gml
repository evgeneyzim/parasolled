/// @description Draw surfaces


//Draw destruct blocks
//with (oDestructBlock)
//{
//	if (self_surface != -1)
//	{
//		draw_surface(self_surface, bbox_left, bbox_top);	
//	}
//}

//Blocks
if (!surface_exists(block_surface) && invoke_blocks < 0)
{
	block_surface = surface_create(room_width, room_height);
	surface_set_target(block_surface);
	draw_sprite(blocks_sprite, 0, 0, 0);
	surface_reset_target();
}
if (invoke_blocks < 0) draw_surface(block_surface, 0, 0);

var _brightness;
//Outline
if (!surface_exists(outline_surface))
{
	outline_surface = surface_create(room_width, room_height);
	surface_set_target(outline_surface);
	draw_sprite(outline_sprite, 0, 0, 0);
	surface_reset_target();
}
_brightness = 0.3;
shader_set(shdBrightness);
shader_set_uniform_f(u_strength, _brightness);
draw_surface(outline_surface, 0, 0);
shader_reset();

//Blur
gpu_set_blendmode(bm_add);
shader_set(shdExtra);
shader_set_uniform_f(u_blur_vector, 1, 0);
shader_set_uniform_f(u_sigma, 0.7);
draw_surface_ext(outline_surface, 0, 0, 1, 1, 0, c_outline, 1);
shader_reset();
gpu_set_blendmode(bm_normal);




