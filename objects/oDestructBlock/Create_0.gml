if (!instance_exists(oDestructBlockDrawController))
{
	instance_create_layer(0, 0, "DestructBlocks", oDestructBlockDrawController);	
}

event_inherited();
maxhp = 2;
hp = maxhp;
prev_hp = 2;
draw_x = 0;
draw_y = 0;

image_speed = 0;

bright = 25;

sprite_index = sDestructBlockMask;

magnitude = 400000;
phase = get_timer();
u_strength = shader_get_uniform(shdBrightness, "strength");
u_time = shader_get_uniform(shdGreyScale, "time");

u_pixelWidth = shader_get_uniform(shdDestructBlocks, "offsetX");
u_pixelHeight = shader_get_uniform(shdDestructBlocks, "offsetY");

texture_ptr = sprite_get_texture(sDestructBlockMask, 0);

// Get texel width and height
texelWidth = texture_get_texel_width(texture_ptr);
texelHeight = texture_get_texel_height(texture_ptr);

//set particles
part_init = false;
death = -1;

width = 4;

explode = true;

self_surface = -1;


DrawIt = function()
{
	var _size = sprite_get_width(sDestruct) / 3;
	var _x1 = x;
	var _y1 = y;
	var _x2 = x + sprite_width - _size;
	var _y2 = y + sprite_height - _size;
	
	var _w = _x2 - _x1;
	var _h = _y2 - _y1;
	
	var _columns = _w div _size;
	var _rows	 = _h div _size;
	
	var _col = oDrawer.c_destruct_blocks;
	
	//CORNERS
	//top left
	draw_sprite_part_ext(sDestruct, 0, 0, 0, _size, _size, _x1, _y1, 1, 1, _col, 1);
	
	//top right
	draw_sprite_part_ext(sDestruct, 0, _size * 2, 0, _size, _size, _x1 + ((_columns) * _size), _y1, 1, 1, _col, 1);
	
	//bottom left
	draw_sprite_part_ext(sDestruct, 0, 0, _size * 2, _size, _size, _x1, _y1 + ((_rows) * _size), 1, 1, _col, 1);
	
	//bottom right
	draw_sprite_part_ext(sDestruct, 0, _size * 2, _size * 2, _size, _size, _x1 + ((_columns) * _size), _y1 + ((_rows) * _size), 1, 1, _col, 1);
	
	
	//EDGES
	for (var i = 1; i < (_rows); ++i)
	{
		//left edge
		draw_sprite_part_ext(sDestruct, 0, 0, _size, _size, _size, _x1, _y1 + (i * _size), 1, 1, _col, 1);
		
		//right edge
		draw_sprite_part_ext(sDestruct, 0, _size * 2, _size, _size, _size, _x1 + (_columns * _size), _y1 + (i * _size), 1, 1, _col, 1);
		
	}
	
	for (var i = 1; i < (_columns); ++i)
	{
		//top edge
		draw_sprite_part_ext(sDestruct, 0, _size, 0, _size, _size, _x1 + (i * _size), _y1, 1, 1, _col, 1);
		
		//bottom edge
		draw_sprite_part_ext(sDestruct, 0, _size, _size * 2, _size, _size, _x1 + (i * _size), _y1 + (_rows * _size), 1, 1, _col, 1);
		
	}
	

	//MIDDLE
	for (var i =  1; i < (_columns); ++i)
	{
		for (var j = 1; j < (_rows); ++j)
		{
			draw_sprite_part_ext(sDestruct, 0, _size, _size, _size, _size, _x1 + (i * _size), _y1 + (j * _size), 1, 1, _col, 1);	
		}
	}
}
