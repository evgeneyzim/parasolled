exit;
//Shader
var _strength = (abs(sin((get_timer() - phase)/magnitude)) + 0.1) * 0.05 + bright/100;
shader_set(shdBrightness);
shader_set_uniform_f(u_strength, _strength);

//Spawn effect
bright = max(0, bright - 1);

////Color
////var _col = merge_color(merge_color(oDrawer.c_block, oDrawer.c_destruct_blocks, 0.4), c_green, 0.3);

////if (self_surface != -1)
////{
////	surface_set_target(self_surface);
////	
////}
////else
////	exit;

////var _size = sprite_get_width(sGroundTexture);
//////Draw foreach
////for (var i = 0; i < sprite_width div _size; ++i)
////{
////	for (var j = 0; j < sprite_height div _size; ++j)
////	{
////		draw_sprite_ext(sGroundTexture, 0, i * _size, j * _size, 1, 1, 0, _col, 1);			
////	}
////}
////shader_reset();


//shader_set(shdDestructBlocks);

//shader_set_uniform_f(u_pixelHeight, texelHeight);
//shader_set_uniform_f(u_pixelWidth, texelWidth);

draw_sprite_ext(sprite_index, 0, x, y, 1, 1, 0, c_white, image_alpha);

//Scratch
var _scratch_width = sprite_get_width(sScratch);
var _scratch_height = sprite_get_height(sScratch);

draw_x = clamp(draw_x, _scratch_width/2, sprite_width - _scratch_width/2);
draw_y = clamp(draw_y, _scratch_height/2, sprite_height - _scratch_height/2);

//Draw scratch


shader_reset();

////gpu_set_blendmode(bm_subtract);

////draw_sprite_ext(sDestructBlockMask, 0, 0, 0, image_xscale, image_yscale, 0, c_white, 1);

////gpu_set_blendmode(bm_normal);

////surface_reset_target();
