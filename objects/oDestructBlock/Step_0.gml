if (hp <= 0)
{
	instance_destroy();	
	exit;
}

if (hp < prev_hp )
{
	part_particles_create(global.part_system, x + sprite_width/2, y + sprite_height/2, death, 10);
}

prev_hp = hp;

if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	death = oPartTypes.block_death;
}