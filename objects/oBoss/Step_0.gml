/// @description

if (!part_init && oPartTypes.part_init)
{
	part_init = true;
	death = oPartTypes.boss_death;
}

center_x = oBossArea.x;
center_y = oBossArea.y;

show_hp += sign(hp - show_hp);

image_angle += rotation_speed;

switch (state)
{
	case BossStates.INACTIVE:
		break;
		
	case BossStates.INTRO:
		
		if (y < center_y)
			y += 5;
		else
		{
			if (oDeeJay.silence_timer == 0)
				state = BossStates.TOSTART;	
		}
		
		break;
	
	case BossStates.TOSTART:
		x = lerp(x, center_x, 0.1);	
		y = lerp(y, center_y, 0.1);
	
		if (point_distance(x, y, center_x, center_y) < close_distance)  
		{
			if (hp == charge_switch)  
			{
				state = BossStates.FINAL;
				real_x = x;
				real_y = y;
				sleep_timer = sleep_timer_max/2;
				if (instance_exists(oPlayer))
				{
					target_x = oPlayer.x;
					target_y = oPlayer.y;	
				}
				else
				{
					target_x = x;
					target_y = y;
				}
				max_scale = 1.2;
			}
			else
			{
				state = BossStates.IDLE;
				direction = irandom(irandom_range(0, 4)) * 90 + 45;
				max_scale = 1;
			}
		}
		break;
		
	case BossStates.IDLE:
	
		xspeed = normal_speed * dcos(-direction);
		yspeed = normal_speed * dsin(-direction);
	
		var _collide = false;
		//Bounce
		if (place_meeting(x + xspeed, y, oBlock))
		{	
			xspeed *= -1;
			direction = point_direction(x, y, x + xspeed, y + yspeed);
			ShakeScreen(5, 5);
			_collide = true;
		}
		x += xspeed;
		
		if (place_meeting(x, y + yspeed, oBlock))
		{	
			yspeed *= -1;
			direction = point_direction(x, y, x + xspeed, y + yspeed);
			ShakeScreen(5, 5);
			_collide = true;
		}
		y += yspeed;
		
				
		break;
		
	case BossStates.CENTER:
		x = lerp(x, center_x, 0.1);	
		y = lerp(y, center_y, 0.1);
	
		if (spawn_timer == 0)
		{
			with (instance_create_layer(x, y, "Spikes", oAimProjectile))
			{
				depth -= 1;
				remain_speed = 10;
				started_direction = irandom_range(0, 360);
			}
			spawn_timer = spawn_timer_max;
			spawn_counter++;
			scale = 0.85;
			if (spawn_counter == spawn_counter_max)  
			{
				spawn_timer = spawn_timer_extended;
				spawn_counter = 0;
			}
		}
		else  spawn_timer--;
		break;
		
	case BossStates.FINAL:
	
		if (!instance_exists(oPlayer))
		{
			exit;	
		}
	
		switch (sign(sleep_timer))
		{
			default:				
			case 1:			
				var _retreat_direction = point_direction(x, y, center_x, center_y);
				xspeed = normal_speed * dcos(-_retreat_direction)/2;
				yspeed = normal_speed * dsin(-_retreat_direction)/2;
		
				x = real_x + irandom_range(-shake_magnitude, shake_magnitude);
				y = real_y + irandom_range(-shake_magnitude, shake_magnitude);
				shake_magnitude = max(0, shake_magnitude - 1);
		
				real_x += xspeed;
				real_y += yspeed;
				
				if (sleep_timer >= sleep_timer_max/4)
				{
					if (instance_exists(oPlayer))
					{
						target_x = oPlayer.x;
						target_y = oPlayer.y;
					}
				}
				else 
				{
					scale -= 0.02;
				}
				
				--sleep_timer;
				
				break;
				
			case -1:
			
				if (!block_find)
				{
					var _direction = point_direction(x, y, target_x, target_y);
					xspeed = charged_speed * dcos(-_direction);
					yspeed = charged_speed * dsin(-_direction);
					direction = _direction;
				}
				else
				{
					xspeed = charged_speed * dcos(-direction);
					yspeed = charged_speed * dsin(-direction);	
				}
		
				x += xspeed;
				y += yspeed;
		
				if (point_distance(x, y, target_x, target_y) < close_distance) block_find = true;
		
				if (block_find && place_meeting(x, y, oBlock)) 
				{
					sleep_timer = sleep_timer_max;
					block_find = false;
					ShakeScreen(20, 20);
			
					real_x = x;
					real_y = y;
					shake_magnitude = shake_magnitude_max;
			
				}
				break;
			
		}
		break;
}

scale = min(scale + 0.01, max_scale);
image_xscale = scale;
image_yscale = scale;

//Timer
damage_timer = max(0, damage_timer - 1);

if (hp <= 0)  
{
	part_particles_create(global.enemy_part, x, y, death, 40);
	ShakeScreen(100, 100);
	instance_create_layer(963, 1040, "PortalBlocks", oPortal);
	with (oPortalAura)  image_yscale = 0.5;
	instance_destroy();
}
