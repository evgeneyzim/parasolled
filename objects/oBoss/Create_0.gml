/// @description
enum BossStates
{
	INACTIVE,
	INTRO,
	TOSTART,
	IDLE,
	CENTER,
	FINAL,
}

state = BossStates.INACTIVE;

//Boss movement
direction = irandom(irandom_range(0, 4)) * 90 + 45;
speed = 0;

normal_speed = 15;
xspeed = normal_speed * dcos(direction);
yspeed = normal_speed * dsin(direction);

rotation_speed = 4;

//Damaging
damage_timer = 0;
damage_timer_max = 20;

max_hp = 100;
hp = max_hp;
scale = 1;

//Spawning projectiles
spawn_timer_max = 20;
spawn_timer = spawn_timer_max;

spawn_counter_max = 10;
spawn_counter = 0;
spawn_timer_extended = 120;

//Final state
target_x = x;
target_y = y;

sleep_timer_max = 105;
sleep_timer = sleep_timer_max;

charged_speed = 25;

close_distance = 20;

block_find = false;

real_x = x;
real_y = y;


shake_magnitude_max = 45;
shake_magnitude = 0;

healthbar_alpha = 0.5;
extend = 0;

_image_blend = merge_color(merge_color(oDrawer.c_spikes, c_orange, 0.3), c_gray, 0.45); 
image_blend = _image_blend;
c_boss = image_blend;

//death particles
part_init = false;

max_scale = 1;

//Eye
cur_x = x;
cur_y = y;
eye_direct = 0;


parasol_damage = 10;
center_switch = 80;
charge_switch = 50;

DamageBoss = function()
{
	scale = max_scale * 0.7;
	if (state != BossStates.INTRO)
	{
	
		hp -= parasol_damage;		
		
		if (hp == center_switch)        state = BossStates.CENTER;
		else if (hp == charge_switch)	state = BossStates.TOSTART;
		
		if (hp > 0)
		{
			ShakeScreen(50, 10);		
		}
		
		if (state != BossStates.CENTER)
		{
			spawn_counter = 0;
			spawn_timer = floor(spawn_timer_extended/3);
		}
	}
}

show_hp = 0;