/// @description

//c_boss = merge_color(merge_color(c_orange, c_red, 0.5), _image_blend, 0.3 + 0.7 * (1/2 - 1/2 * sin(get_timer()/5000000)));
//image_blend = c_boss;

// Inherit the parent event
event_inherited();

draw_self();

draw_sprite_ext(sBossFace, 0, x, y, image_xscale, image_yscale, 0, c_white, image_alpha);

//Draw eye
switch (state)
{
	case BossStates.FINAL:
	
		if (sleep_timer >= sleep_timer_max/2)
		{
			cur_x = x;
			cur_y = y - 47 * image_yscale;
		}
		else
		{
			var _a = 0;
		
			_a = point_direction(x, y - 47 * image_yscale, target_x, target_y);
			

			if (_a != 0)
				eye_direct = _a;

			desired_x = (x) + lengthdir_x(143/4 * image_xscale, eye_direct);
			desired_y = (y - 47 * image_yscale) + lengthdir_y(83/4 * image_yscale, eye_direct);

			var _acc = 1;

			cur_x = lerp(cur_x, desired_x, _acc);
			cur_y = lerp(cur_y, desired_y, _acc);	
		}
	
		break;
	
	case BossStates.CENTER:
		var _a = 0;
		
		if (instance_exists(oPlayer))
			_a = point_direction(x, y - 47 * image_yscale, oPlayer.x, oPlayer.y);
			

		if (_a != 0)
			eye_direct = _a;

		desired_x = (x) + lengthdir_x(143/4 * image_xscale, eye_direct);
		desired_y = (y - 47 * image_yscale) + lengthdir_y(83/4 * image_yscale, eye_direct);

		var _acc = 0.3;

		cur_x = lerp(cur_x, desired_x, _acc);
		cur_y = lerp(cur_y, desired_y, _acc);
		
		break;
		
	
	case BossStates.TOSTART:
		cur_x = x;
		cur_y = y - 47 * image_yscale;
		break;
	
	
	case BossStates.IDLE:

		var _a = point_direction(xprevious, yprevious, x, y);

		if (_a != 0)
			eye_direct = _a;

		desired_x = (x) + lengthdir_x(143/2 * image_xscale, eye_direct);
		desired_y = (y - 47 * image_yscale) + lengthdir_y(83/2 * image_yscale, eye_direct);

		var _acc = 0.3;

		cur_x = lerp(cur_x, desired_x, _acc);
		cur_y = lerp(cur_y, desired_y, _acc);
		
		break;
		
	case BossStates.INTRO:
	
		var _a = 0;
		
		if (instance_exists(oPlayer))
			_a = point_direction(x, y - 47 * image_yscale, oPlayer.x, oPlayer.y);
			

		if (_a != 0)
			eye_direct = _a;

		desired_x = (x) + lengthdir_x(143/4 * image_xscale, eye_direct);
		desired_y = (y - 47 * image_yscale) + lengthdir_y(83/4 * image_yscale, eye_direct);

		var _acc = 0.3;

		cur_x = lerp(cur_x, desired_x, _acc);
		cur_y = lerp(cur_y, desired_y, _acc);
		
		break;
		
	default:
		break;
		
	

}

draw_sprite_ext(sBossEye, 0, cur_x, cur_y, image_xscale, image_yscale, 0, c_white, image_alpha);	

if (show_hp != hp)
{
	extend = 10;
	healthbar_alpha = 1;
}
else
{
	extend = max(0, extend - 1);
	healthbar_alpha = max(0.5, healthbar_alpha - 0.05);
}

draw_set_alpha(healthbar_alpha);
draw_healthbar(x - sprite_get_width(sBoss) * 2/3 + 10, y - sprite_get_height(sBoss)/2 - 10 + extend, x + sprite_get_width(sBoss) * 2/3 - 10, y - sprite_get_height(sBoss)/2 - 30 - extend, show_hp, c_gray, c_red, c_red, 0, true, true);
draw_set_alpha(1);


