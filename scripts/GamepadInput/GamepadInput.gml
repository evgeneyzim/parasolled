function GamepadInput(_gamepad) constructor
{
	left = 0;
	right = 0;
	up = 0;
	down = 0;
	
	if (gamepad_is_connected(_gamepad))
	{
		if (global.allowed_to_move)
		{
			if (gamepad_axis_value(_gamepad, gp_axislh) < -global.axis_deadzone)	
			{
				left = true;
				global.allowed_to_move = false;
			}
			
			if (gamepad_axis_value(_gamepad, gp_axislh) > global.axis_deadzone)	
			{
				right = true;
				global.allowed_to_move = false;
			}
			
			if (gamepad_axis_value(_gamepad, gp_axislv) < -global.axis_deadzone)	
			{
				up = true;
				global.allowed_to_move = false;
			}
			
			if (gamepad_axis_value(_gamepad, gp_axislv) > global.axis_deadzone)	
			{
				down = true;
				global.allowed_to_move = false;
			}
		}
		else
		{
			if (abs(gamepad_axis_value(_gamepad, gp_axislh)) < global.axis_free && abs(gamepad_axis_value(_gamepad, gp_axislv)) < global.axis_free)	
			{
				global.allowed_to_move = true;	
			}
		}
	}
	else
	{
		global.allowed_to_move = true;	
	}
}