//Save data to file
function SaveGame() {

	var _save_map =  ds_map_create();

	ds_map_add(_save_map, "Resolution_width", global.resolution_width);
	ds_map_add(_save_map, "Resolution_height", global.resolution_height);
	ds_map_add(_save_map, "Music_volume", global.music_volume);
	ds_map_add(_save_map, "SFX_volume", global.sfx_volume);
	ds_map_add(_save_map, "Progress", global.progress);
	ds_map_add(_save_map, "Language", global.language);
	ds_map_add(_save_map, "Vibration", global.vibration);
	ds_map_add(_save_map, "Highscore", global.speedrun_highscore);
	
	//Save levels' time
	for (var i = 0; i < array_length(global.speedrun_levels); ++i)
	{
		var _key = "Level" + string(i);
		ds_map_add(_save_map, _key, global.speedrun_levels[i]);
	}
	

	
	ds_map_secure_save(_save_map, global.filename);
	ds_map_destroy(_save_map);
	
	global.new_game = false;
}


//Load the game from file.
function LoadGame() {
	
	if (!file_exists(global.filename))
	{
		global.resolution_width = 1920;
		global.resolution_height = 1080;
		global.invoke_fullscreen = 30;
		global.music_volume = 0.5;
		global.sfx_volume = 1.0;
		global.progress = 0;
		global.language = "English";
		global.vibration = true;
		global.speedrun_highscore = -1;
	
		global.speedrun_levels = [];
		for (var i = 0; i < NUMBER_OF_LEVELS; ++i)
		{
			global.speedrun_levels[i] = -1;	
		}
		
		global.new_game = true;
	}
	else
	{
		var _map =	ds_map_secure_load(global.filename);
		global.resolution_width = _map[?"Resolution_width"];
		global.resolution_height = _map[?"Resolution_height"];
		global.music_volume = _map[?"Music_volume"];
		global.sfx_volume = _map[?"SFX_volume"];
		global.progress = _map[?"Progress"];
		global.language = _map[?"Language"];
		global.vibration = _map[?"Vibration"];
		global.speedrun_highscore = _map[?"Highscore"];
		
		for (var i = 0; i < NUMBER_OF_LEVELS; ++i)
		{
			var _key = "Level" + string(i);
			global.speedrun_levels[i] = _map[?_key];	
		}
		
		
		//Default
		if (global.resolution_width == undefined)
			global.resolution_width = 1920;	
		if (global.resolution_height == undefined)
			global.resolution_height = 1080;	
		if (global.music_volume == undefined)
			global.music_volume = 0.5;	
		if (global.sfx_volume == undefined)
			global.sfx_volume = 1.0;	
		if (global.progress == undefined)
			global.progress = 0;	
		if (global.language == undefined)
			global.language = "English";	
		if (global.vibration == undefined)
			global.vibration = true;	
		if (global.speedrun_highscore == undefined)
			global.speedrun_highscore = -1;
		
		
		for (var i = 0; i < NUMBER_OF_LEVELS; ++i)
		{
			if (global.speedrun_levels[i] == undefined)
				global.speedrun_levels[i] = -1;	
		}
			
		
		if (global.resolution_width == 1920)
		{
			global.invoke_fullscreen = 10;
			global.invoke_1920 = 5;
		}
		
		if (global.resolution_width == "Fullscreen")
		{
			global.resolution_width = 1920;
			global.resolution_height = 1080;	
			global.invoke_fullscreen = 10;
		}
		
		global.new_game = false;
		
		ds_map_destroy(_map);
	}

}
