function OnView(_instance){
	var _camera_left = oCamera.x - oCamera.view_w_half;
	var _camera_right = oCamera.x + oCamera.view_w_half;
	var _camera_top = oCamera.y - oCamera.view_h_half;
	var _camera_bottom = oCamera.y + oCamera.view_h_half;
	
	var _top = _instance.bbox_top;
	var _bottom = _instance.bbox_bottom;
	var _left = _instance.bbox_left;
	var _right = _instance.bbox_right;
	
	
	if (_right > _camera_left && _left < _camera_right && _top < _camera_bottom && _bottom > _camera_top)
	{
		return true;	
	}
	return false;
}