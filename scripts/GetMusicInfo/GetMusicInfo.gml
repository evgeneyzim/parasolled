///@desc GetMusicInfo(track)
///@param track
function GetMusicInfo(argument0) {

	///Returns intro_time and end_time for a track.

	var _track = argument0;
	var _intro_time;
	var _end_time;

	switch(_track)
	{
		case musMus:
			_intro_time = 16;
			_end_time = 88;
			break;
		case musTron:
			_intro_time = 0;
			_end_time = 9.41;
			break;
		case musSpacey:
			_intro_time = 0;
			_end_time = 60;
			break;
		case musPrologue:
			_intro_time = 0;
			_end_time = 16;
			break;
		case musSilence:
			_intro_time = 0;
			_end_time = 1;
			break;
		case musBoss1:
			_intro_time = 0;
			_end_time = 48.004;
			break;
		case musBoss2:
			_intro_time = 0;
			_end_time = 51.690;
			break;
		case musBossIntro:
			_intro_time = 0;
			_end_time = 20;
			break;
	}

	return [_intro_time, _end_time];


}

//function Music(_track) constructor
//{
//	var _data = __GetMusicInfo(_track);
	
//	mus = _track;
//	intro_time = _data[0];
//	end_time = _data[1];
	
//	cur_gain = 1;
//}
