function GetText(_number)
{
	ini_open("text.txt");
	var _str = ini_read_string(global.language, string(_number), "Text is not found!");
	ini_close();

	_str = string_replace_all(_str, "*", "\n");
	return _str;
}