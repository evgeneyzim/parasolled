//if statement is false, game ends with an error

function Raise(error_message) {
	show_debug_message(string(error_message));
	game_end();
}

function Assert(statement, error_message) {
	if (!statement)  Raise(error_message);
}

//Print various arguments
function Print() {
	
	var _line = "";
	for (var i = 0; i < argument_count; ++i)
	{
		_line += string(argument[i]);
		_line += " ";
	}

	show_debug_message(_line);
}


