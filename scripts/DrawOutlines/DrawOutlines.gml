function DrawOutlines(_wall_type, _color, _width) {
	with (_wall_type)
	{
		for (var _x = bbox_left; _x <= bbox_right; ++_x)
		{
			//Top-left corner
			if (position_meeting(_x - 1, bbox_bottom, _wall_type) && !position_meeting(_x, bbox_bottom + 1, oWallType) && position_meeting(_x - 1, bbox_bottom + 1, oWallType))
			{
				for (var i = 0; i < _width - 1; ++i)
				{
					draw_set_color(merge_color(_color, c_black, 0.25));
					draw_rectangle(_x - i - 2, bbox_bottom + 1, _x - 1 - i, bbox_bottom - _width/2, false);
					draw_set_color(_color);
					draw_rectangle(_x - i - 2, bbox_bottom - _width/2 + 1, _x - 1 - i, bbox_bottom - _width, false);	
				}
			}
			
			
			//Top
			if (_x != bbox_right && !position_meeting(_x, bbox_top - 1, _wall_type) && !position_meeting(_x, bbox_top - 1, oSpecialWall))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(_x, bbox_top, _x + 1, bbox_top + _width/2, false);
				draw_set_color(_color);
				draw_rectangle(_x, bbox_top + _width/2, _x + 1, bbox_top + _width, false);	
			}
			else
			{
				var _save = _x;
				if (_x != bbox_right && _x != bbox_left)	_x--;
				//Corners
				if ((!position_meeting(_x, bbox_top - 1, oWallType) && position_meeting(_x + 1, bbox_top - 1, _wall_type)) || (!position_meeting(_x, bbox_top - 1, oWallType) && position_meeting(_x + 1, bbox_top - 1, oSpecialWall) && position_meeting(_x + 1, bbox_top, oWall)))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(_x + i + 1, bbox_top, _x + 2 + i, bbox_top + _width/2, false);
						draw_set_color(_color);
						draw_rectangle(_x + i + 1, bbox_top + _width/2, _x + 2 + i, bbox_top + _width, false);	
					}
				}
				if (_save != bbox_right && _save != bbox_left)	_x++;
			}
			
			//Bottom
			if (_x != bbox_right && !position_meeting(_x, bbox_bottom + 1, _wall_type) && !position_meeting(_x, bbox_bottom + 1, oSpecialWall))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(_x, bbox_bottom + 1, _x + 1, bbox_bottom - _width/2, false);
				draw_set_color(_color);
				draw_rectangle(_x, bbox_bottom - _width/2 + 1, _x + 1, bbox_bottom - _width, false);				
			}
			else
			{
				var _save = _x;
				if (_x != bbox_right && _x != bbox_left)	_x--;
				//Corners
				if ((!position_meeting(_x, bbox_bottom + 1, oWallType) && position_meeting(_x + 1, bbox_bottom + 1, _wall_type)) || (!position_meeting(_x, bbox_bottom + 1, oWallType) && position_meeting(_x + 1, bbox_bottom + 1, oSpecialWall) && position_meeting(_x + 1, bbox_bottom, oWall)))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(_x + i + 1, bbox_bottom + 1, _x + 2 + i, bbox_bottom - _width/2, false);
						draw_set_color(_color);
						draw_rectangle(_x + i + 1, bbox_bottom - _width/2 + 1, _x + 2 + i, bbox_bottom - _width, false);	
					}
				}
				if (_save != bbox_right && _save != bbox_left)	_x++;
			}
		}
		
		for (var _y = bbox_top; _y <= bbox_bottom; ++_y)
		{
			
			//Top-left corner
			if (position_meeting(bbox_left, _y - 1, _wall_type) && !position_meeting(bbox_left - 1, _y, oWallType) && position_meeting(bbox_left - 1, _y - 1, oWallType))
			{
				for (var i = 0; i < _width - 1; ++i)
				{
					draw_set_color(merge_color(_color, c_black, 0.25));
					draw_rectangle(bbox_left, _y - i - 2, bbox_left + _width/2, _y - i - 1, false);
					draw_set_color(_color);
					draw_rectangle(bbox_left + _width/2, _y - i - 2, bbox_left + _width, _y - i - 1, false);	
				}
			}
			
			
			//Left
			if (_y != bbox_bottom && !position_meeting(bbox_left - 1, _y, _wall_type) && !position_meeting(bbox_left - 1, _y, oSpecialWall))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(bbox_left, _y, bbox_left + _width/2, _y + 1, false);
				draw_set_color(_color);
				draw_rectangle(bbox_left + _width/2, _y, bbox_left + _width, _y + 1, false);	
								
			}
			else
			{
				var _save = _y;
				if (_y != bbox_bottom && _y != bbox_top) _y--;
				//Corners
				if ((!position_meeting(bbox_left - 1, _y, oWallType) && position_meeting(bbox_left - 1, _y + 1, _wall_type)) || (!position_meeting(bbox_left - 1, _y, oWallType) && position_meeting(bbox_left - 1, _y + 1, oSpecialWall) && position_meeting(bbox_left, _y + 1, oWall)))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(bbox_left, _y + 1 + i, bbox_left + _width/2, _y + 2 + i, false);
						draw_set_color(_color);
						draw_rectangle(bbox_left + _width/2, _y + 1 + i, bbox_left + _width, _y + 2 + i, false);	
					
					}
				}
				if (_save != bbox_bottom && _save != bbox_top) _y++;
			}
			
			//Right
			if (_y != bbox_bottom && !position_meeting(bbox_right + 1, _y, _wall_type) && !position_meeting(bbox_right + 1, _y, oSpecialWall))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(bbox_right + 1, _y, bbox_right - _width/2, _y + 1, false);
				draw_set_color(_color);
				draw_rectangle(bbox_right - _width/2 + 1, _y, bbox_right - _width, _y + 1, false);
			}
			else
			{
				var _save = _y;
				if (_y != bbox_bottom && _y != bbox_top) _y--;
				//Corners
				if ((!position_meeting(bbox_right + 1, _y, oWallType) && position_meeting(bbox_right + 1, _y + 1, _wall_type)) || (!position_meeting(bbox_right + 1, _y, oWallType) && position_meeting(bbox_right + 1, _y + 1, oSpecialWall) && position_meeting(bbox_right, _y + 1, oWall)))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(bbox_right + 1, _y + 1 + i, bbox_right - _width/2, _y + 2 + i, false);
						draw_set_color(_color);
						draw_rectangle(bbox_right - _width/2 + 1, _y + 1 + i, bbox_right - _width, _y + 2 + i, false);	
					
					}
				}
				if (_save != bbox_bottom && _save != bbox_top) _y++;
			}
		}
	}
}

function DrawOutlinesSpecial(_wall_type, _color, _width)
{
	with (_wall_type)
	{
		for (var _x = bbox_left; _x <= bbox_right; ++_x)
		{
			//Top
			if (_x != bbox_right && !position_meeting(_x, bbox_top - 1, _wall_type))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(_x, bbox_top, _x + 1, bbox_top + _width/2, false);
				draw_set_color(_color);
				draw_rectangle(_x, bbox_top + _width/2, _x + 1, bbox_top + _width, false);	
			}
			else
			{
				if (_x != bbox_right)	_x--;
				//Corners
				if (!position_meeting(_x, bbox_top - 1, _wall_type) && position_meeting(_x + 1, bbox_top - 1, _wall_type) && position_meeting(_x + 1, bbox_top, _wall_type))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(_x + i + 1, bbox_top, _x + 2 + i, bbox_top + _width/2, false);
						draw_set_color(_color);
						draw_rectangle(_x + i + 1, bbox_top + _width/2, _x + 2 + i, bbox_top + _width, false);	
					}
				}
				if (_x != bbox_right)	_x++;
			}
			
			//Bottom
			if (_x != bbox_right && !position_meeting(_x, bbox_bottom + 1, _wall_type))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(_x, bbox_bottom + 1, _x + 1, bbox_bottom - _width/2, false);
				draw_set_color(_color);
				draw_rectangle(_x, bbox_bottom - _width/2 + 1, _x + 1, bbox_bottom - _width, false);	
				
				//Top-left corner
				if (position_meeting(_x - 1, bbox_bottom, _wall_type) && position_meeting(_x - 1, bbox_bottom + 1, _wall_type))
				{
					for (var i = 0; i < _width - 1; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(_x - i - 2, bbox_bottom + 1, _x - i - 1, bbox_bottom - _width/2, false);
						draw_set_color(_color);
						draw_rectangle(_x - i - 2, bbox_bottom - _width/2 + 1, _x - i - 1, bbox_bottom - _width, false);	
					}
				}
				
				
			}
			else
			{
				if (_x != bbox_right)	_x--;
				//Corners
				if (!position_meeting(_x, bbox_bottom + 1, _wall_type) && position_meeting(_x + 1, bbox_bottom + 1, _wall_type) && position_meeting(_x + 1, bbox_bottom, _wall_type))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(_x + i + 1, bbox_bottom + 1, _x + 2 + i, bbox_bottom - _width/2, false);
						draw_set_color(_color);
						draw_rectangle(_x + i + 1, bbox_bottom - _width/2 + 1, _x + 2 + i, bbox_bottom - _width, false);	
					}
				}
				if (_x != bbox_right)	_x++;
			}
		}
		
		for (var _y = bbox_top; _y <= bbox_bottom; ++_y)
		{
			//Left
			if (_y != bbox_bottom && !position_meeting(bbox_left - 1, _y, _wall_type))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(bbox_left, _y, bbox_left + _width/2, _y + 1, false);
				draw_set_color(_color);
				draw_rectangle(bbox_left + _width/2, _y, bbox_left + _width, _y + 1, false);	
								
			}
			else
			{
				if (_y != bbox_bottom) _y--;
				//Corners
				if (!position_meeting(bbox_left - 1, _y, _wall_type) && position_meeting(bbox_left - 1, _y + 1, _wall_type) && position_meeting(bbox_left, _y + 1, _wall_type))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(bbox_left, _y + 1 + i, bbox_left + _width/2, _y + 2 + i, false);
						draw_set_color(_color);
						draw_rectangle(bbox_left + _width/2, _y + 1 + i, bbox_left + _width, _y + 2 + i, false);	
					
					}
				}
				if (_y != bbox_bottom) _y++;
			}
			
			//Right
			if (_y != bbox_bottom && !position_meeting(bbox_right + 1, _y, _wall_type))
			{
				draw_set_color(merge_color(_color, c_black, 0.25));
				draw_rectangle(bbox_right + 1, _y, bbox_right - _width/2, _y + 1, false);
				draw_set_color(_color);
				draw_rectangle(bbox_right - _width/2 + 1, _y, bbox_right - _width, _y + 1, false);
			}
			else
			{
				if (_y != bbox_bottom) _y--;
				//Corners
				if (!position_meeting(bbox_right + 1, _y, _wall_type) && position_meeting(bbox_right + 1, _y + 1, _wall_type) && position_meeting(bbox_right, _y + 1, _wall_type))
				{
					for (var i = 0; i < _width; ++i)
					{
						draw_set_color(merge_color(_color, c_black, 0.25));
						draw_rectangle(bbox_right + 1, _y + 1 + i, bbox_right - _width/2, _y + 2 + i, false);
						draw_set_color(_color);
						draw_rectangle(bbox_right - _width/2 + 1, _y + 1 + i, bbox_right - _width, _y + 2 + i, false);	
					
					}
				}
				if (_y != bbox_bottom) _y++;
			}
		}
	}
}