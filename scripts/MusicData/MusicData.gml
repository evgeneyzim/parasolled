function GetMusic()
{
	return [musMus, musSilence, musSpacey, musTron, musPrologue, musBoss1, musBoss2, musBossIntro];
}

function GetSFX()
{
	return [soDying, soJump, soLanding, soParasolClose, soParasolHitGround, soParasolOpenAir, soParasolOpenShield, soSlide, soButtonActivate, soSelect, soClink, soMenuClick, soFan, soSquareHit, soSquareKilled, soMinigame, soGunShoot, soBoxFalling, soGunReload, soSpeedrunLevels, soTurretShoot, soSlimeLanding, soParasolPickUp, soFirstNote, soSecondNote, soThirdNote, soMusicError, soUnableToReturnParasol, soParasolReturn];	
}

function SetGain(_music, _gain) 
{
	for (var i = 0; i < array_length(_music); ++i)
	{
		var _track = _music[i];
		
		var _c = 1;
		if (_track == musSpacey)
		{
			var _c = 0.7;	
		}
	
		audio_sound_gain(_track, _gain * _c, 0);
	}
}