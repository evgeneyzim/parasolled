//MACROS
#macro PARASOL_WIDTH 111

#macro ROOM_START rJumpIntro
#macro ROOM_LAST rFinal
#macro NUMBER_OF_LEVELS 24

#macro SLOW_SPEED 20
#macro NORMAL_SPEED 60

#macro SLOW_EFFECT 0.4
#macro NORMAL_EFFECT 1
#macro SLOW_MOTION_LENGTH 0 //90

#macro THROW_ACTIVATE_ROOM rClusterIntro
#macro SHIELD_ACTIVATE_ROOM rShieldIntro

#macro SHESTE 6

#macro MUS_INTRO_LENGTH 360

///ENUMERATORS
//Used for parasol collisions
enum CollidingSurface
{
	LEFT,
	BOTTOM,
	RIGHT,
	TOP,
};

//Camera modes
enum CameraMode
{
	MENU,
	NORMAL,
};

//Slime states
enum SlimeState
{
	IDLE,
	FIGHT,
};


//CODE TO RUN THE GAME
//Initialize global variables
global.resolution_width = display_get_gui_width();
global.resolution_height = display_get_gui_height();
global.music_volume = 0.5;
global.sfx_volume = 1.0;
global.progress = 0;
global.language = "English";
global.vibration = true;
global.speedrun_highscore = -1;
global.speedrun_levels = [];

global.filename = "parasolled.sav";

global.invoke_fullscreen = 0;
global.invoke_1920 = 0;


//For gamepad controlling
global.allowed_to_move = true;
global.axis_deadzone = 0.7;
global.axis_free = 0.3;


//Load game
LoadGame();


//Particles
global.part_init = false;


//Set resolution
var _res_w = global.resolution_width;
var _res_h = global.resolution_height;

//slow motion
global.slow_motion = 0;
global.slow_motion_effect = 1;


if (_res_w == "Fullscreen")
{
	var _w = display_get_width();
	var _h = display_get_height();
		
	display_set_gui_size(_w, _h);	
	window_set_size(_w, _h);
	surface_resize(application_surface, _w, _h);
	window_set_fullscreen(true);	
}
else
{				
	display_set_gui_size(_res_w, _res_h);	
	window_set_size(_res_w, _res_h);
	surface_resize(application_surface, _res_w, _res_h);
}

//Glitch
BktGlitch_init();
seed = random(1);

//Load audio groups
//audio_group_load(Music);
//audio_group_load(SFX);

SetGain(GetMusic(), global.music_volume);
SetGain(GetSFX(), global.sfx_volume);




