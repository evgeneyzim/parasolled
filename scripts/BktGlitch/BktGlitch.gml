/// @description  BktGlitch_config_preset(preset)
/// @function  BktGlitch_config_preset
/// @param preset
function BktGlitch_config_preset(argument0) {
	/*
	    Sets a start preset for the shader, in case you don't want to set all uniforms
	    manually. Defaults to all-zeroes. 
    
	    If you set up a nice configuration in the demo, you can press C to have its code
	    generated and copied into the clipboard.
    
	    Resolution needs to be set separately.
	
		Presets: 
	        BktGlitchPreset.A
	        BktGlitchPreset.B
	        BktGlitchPreset.C
	        BktGlitchPreset.D
	        BktGlitchPreset.E    
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	var _preset = argument0;

	switch (_preset){
	    case BktGlitchPreset.A:       
	        BktGlitch_set_intensity(1.000000);
	        BktGlitch_set_line_shift(0.004000);
	        BktGlitch_set_line_speed(0.010000);
	        BktGlitch_set_line_resolution(1.000000);
	        BktGlitch_set_line_drift(0.100000);
	        BktGlitch_set_line_vertical_shift(0.000000);
	        BktGlitch_set_noise_level(0.500000);
	        BktGlitch_set_jumbleness(0.200000);
	        BktGlitch_set_jumble_speed(0.166667);
	        BktGlitch_set_jumble_resolution(.23333333);
	        BktGlitch_set_jumble_shift(0.160000);
	        BktGlitch_set_channel_shift(0.004000);
	        BktGlitch_set_channel_dispersion(0.002500);
	        BktGlitch_set_shakiness(0.500000);
	        BktGlitch_set_rng_seed(0.000000);
	    break;
    
	    case BktGlitchPreset.B:       
	        BktGlitch_set_intensity(1.000000);
	        BktGlitch_set_line_shift(0.011000);
	        BktGlitch_set_line_speed(0.166667);
	        BktGlitch_set_line_resolution(0.420000);
	        BktGlitch_set_line_drift(0.249702);
	        BktGlitch_set_line_vertical_shift(0.713333);
	        BktGlitch_set_noise_level(0.940000);
	        BktGlitch_set_jumbleness(0.273333);
	        BktGlitch_set_jumble_speed(0.000000);
	        BktGlitch_set_jumble_resolution(.07333333);
	        BktGlitch_set_jumble_shift(0.626667);
	        BktGlitch_set_channel_shift(0.003333);
	        BktGlitch_set_channel_dispersion(0.000000);
	        BktGlitch_set_shakiness(1.733333);
	        BktGlitch_set_rng_seed(30.000000);
	    break;
    
	    case BktGlitchPreset.C:   
	        BktGlitch_set_intensity(1.000000);
	        BktGlitch_set_line_shift(0.001667);
	        BktGlitch_set_line_speed(0.020397);
	        BktGlitch_set_line_resolution(1.145380);
	        BktGlitch_set_line_drift(0.212783);
	        BktGlitch_set_line_vertical_shift(0.125946);
	        BktGlitch_set_noise_level(1.000000);
	        BktGlitch_set_jumbleness(0.660000);
	        BktGlitch_set_jumble_speed(0.000000);
	        BktGlitch_set_jumble_resolution(.20000000);
	        BktGlitch_set_jumble_shift(0.673333);
	        BktGlitch_set_channel_shift(0.012000);
	        BktGlitch_set_channel_dispersion(0.063333);
	        BktGlitch_set_shakiness(3.933333);
	        BktGlitch_set_rng_seed(48.294403);
	    break;
    
	    case BktGlitchPreset.D:   
	        BktGlitch_set_intensity(1.000000);
	        BktGlitch_set_line_shift(0.001333);
	        BktGlitch_set_line_speed(0.023333);
	        BktGlitch_set_line_resolution(2.160000);
	        BktGlitch_set_line_drift(0.573333);
	        BktGlitch_set_line_vertical_shift(0.326667);
	        BktGlitch_set_noise_level(1.000000);
	        BktGlitch_set_jumbleness(0.660000);
	        BktGlitch_set_jumble_speed(0.000000);
	        BktGlitch_set_jumble_resolution(.24000000);
	        BktGlitch_set_jumble_shift(0.040000);
	        BktGlitch_set_channel_shift(0.000667);
	        BktGlitch_set_channel_dispersion(0.003333);
	        BktGlitch_set_shakiness(2.466667);
	        BktGlitch_set_rng_seed(0.000000);
	    break;
    
	    case BktGlitchPreset.E:   
	        BktGlitch_set_intensity(1.000000);
	        BktGlitch_set_line_shift(0.001333);
	        BktGlitch_set_line_speed(0.250000);
	        BktGlitch_set_line_resolution(1.660000);
	        BktGlitch_set_line_drift(0.493333);
	        BktGlitch_set_line_vertical_shift(0.740000);
	        BktGlitch_set_noise_level(0.353333);
	        BktGlitch_set_jumbleness(0.300000);
	        BktGlitch_set_jumble_speed(25.000000);
	        BktGlitch_set_jumble_resolution(.10000000);
	        BktGlitch_set_jumble_shift(0.086667);
	        BktGlitch_set_channel_shift(0.034667);
	        BktGlitch_set_channel_dispersion(0.020000);
	        BktGlitch_set_shakiness(2.266667);
	        BktGlitch_set_rng_seed(48.666667);
	    break;
    
	    default:
	        BktGlitch_config_zero();
	}



}


/// @description  BktGlitch_config_zero()
/// @function  BktGlitch_config_zero
function BktGlitch_config_zero() {
	/*
	    One-liner that sets almost all properties of the shader to zero. 
    
	    Resolution needs to be set separately.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	BktGlitch_set_line_shift(0);
	BktGlitch_set_line_speed(0);
	BktGlitch_set_line_resolution(0);
	BktGlitch_set_line_drift(0);
	BktGlitch_set_line_vertical_shift(0);
	BktGlitch_set_jumbleness(0);
	BktGlitch_set_jumble_speed(0);
	BktGlitch_set_jumble_resolution(0);
	BktGlitch_set_jumble_shift(0);
	BktGlitch_set_noise_level(0);
	BktGlitch_set_channel_shift(0);
	BktGlitch_set_channel_dispersion(0);
	BktGlitch_set_shakiness(0);
	BktGlitch_set_intensity(0);
	BktGlitch_set_rng_seed(0);
	BktGlitch_set_time(0);



}

/// @description  BktGlitch_init()
/// @function  BktGlitch_init
function BktGlitch_init() {
	/*
	    Initialises the shader, run at the start of the game!
	*/

	enum bktGlitch {
	    lineSpeed,
	    lineShift,
	    lineResolution,
	    lineVertShift,
	    lineDrift,
	    jumbleSpeed,
	    jumbleShift,
	    jumbleResolution,
	    jumbleness,
	    dispersion,
	    channelShift,
	    noiseLevel, 
	    shakiness,
	    rngSeed,
	    intensity,
	    time,
	    resolution,
	};

	enum BktGlitchPreset {
	    A,
	    B,
	    C,
	    D,
	    E
	};

	global.bktGlitchUniform[bktGlitch.lineSpeed] = shader_get_uniform(shdBktGlitch, "lineSpeed");
	global.bktGlitchUniform[bktGlitch.lineDrift] = shader_get_uniform(shdBktGlitch, "lineDrift");
	global.bktGlitchUniform[bktGlitch.lineResolution] = shader_get_uniform(shdBktGlitch, "lineResolution");
	global.bktGlitchUniform[bktGlitch.lineVertShift] = shader_get_uniform(shdBktGlitch, "lineVertShift");
	global.bktGlitchUniform[bktGlitch.lineShift] = shader_get_uniform(shdBktGlitch, "lineShift");   
	global.bktGlitchUniform[bktGlitch.jumbleness] = shader_get_uniform(shdBktGlitch, "jumbleness"); 
	global.bktGlitchUniform[bktGlitch.jumbleResolution] = shader_get_uniform(shdBktGlitch, "jumbleResolution");   
	global.bktGlitchUniform[bktGlitch.jumbleShift] = shader_get_uniform(shdBktGlitch, "jumbleShift");  
	global.bktGlitchUniform[bktGlitch.jumbleSpeed] = shader_get_uniform(shdBktGlitch, "jumbleSpeed");    
	global.bktGlitchUniform[bktGlitch.dispersion] = shader_get_uniform(shdBktGlitch, "dispersion");
	global.bktGlitchUniform[bktGlitch.channelShift] = shader_get_uniform(shdBktGlitch, "channelShift"); 
	global.bktGlitchUniform[bktGlitch.noiseLevel] = shader_get_uniform(shdBktGlitch, "noiseLevel");   
	global.bktGlitchUniform[bktGlitch.shakiness] = shader_get_uniform(shdBktGlitch, "shakiness");
	global.bktGlitchUniform[bktGlitch.rngSeed] = shader_get_uniform(shdBktGlitch, "rngSeed");
	global.bktGlitchUniform[bktGlitch.intensity] = shader_get_uniform(shdBktGlitch, "intensity"); 
	global.bktGlitchUniform[bktGlitch.time] = shader_get_uniform(shdBktGlitch, "time");   
	global.bktGlitchUniform[bktGlitch.resolution] = shader_get_uniform(shdBktGlitch, "resolution");




}

/// @description  BktGlitch_set_channel_dispersion(dispersion)
/// @function  BktGlitch_set_channel_dispersion
/// @param dispersion
function BktGlitch_set_channel_dispersion(argument0) {
	/*
	    Sets level of horizontal noisy RGB channel dispersion.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.dispersion], abs(argument0));



}


/// @description  BktGlitch_set_channel_shift(shift)
/// @function  BktGlitch_set_channel_shift
/// @param shift
function BktGlitch_set_channel_shift(argument0) {
	/*
	    Sets level of horizontal RGB channel shift.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.channelShift], abs(argument0));



}

/// @description  BktGlitch_set_intensity(intensity)
/// @function  BktGlitch_set_intensity
/// @param intensity
function BktGlitch_set_intensity(argument0) {
	/*
	    Sets overall intensity of the shader.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.intensity], abs(argument0));



}

/// @description  BktGlitch_set_jumble_resolution(resolution)
/// @function  BktGlitch_set_jumble_resolution
/// @param resolution
function BktGlitch_set_jumble_resolution(argument0) {
	/*
	    Sets resolution of glitch blocks. 
	    Range based around 0-1, no upper limit.
	    Higher = smaller blocks
	    0 = entire texture is a single block
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.jumbleResolution], abs(argument0));



}

/// @description  BktGlitch_set_jumble_shift(shift)
/// @function  BktGlitch_set_jumble_shift
/// @param shift
function BktGlitch_set_jumble_shift(argument0) {
	/*
	    Sets level of texture offset in glitch blocks.
	    Range based around 0-1, no upper limit.
	    Higher = more "scrambled" look
	    0 = no shift at all, blocks not visible
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.jumbleShift], abs(argument0));



}

/// @description  BktGlitch_set_jumble_speed(jumble speed)
/// @function  BktGlitch_set_jumble_speed
/// @param jumble speed
function BktGlitch_set_jumble_speed(argument0) {
	/*
	    Sets speed of jumble variation.
	    Range based around 0-1, no upper limit.
	    Higher = faster block variation
	    0 = no change over time
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.jumbleSpeed], abs(argument0));



}

/// @description  BktGlitch_set_jumbleness(jumbleness)
/// @function  BktGlitch_set_jumbleness
/// @param jumbleness
function BktGlitch_set_jumbleness(argument0) {
	/*
	    Sets level of "jumbleness" - glitch blocks.
	    Range based around 0-1, no upper limit.
	    Higher = more blocks
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.jumbleness], abs(argument0));



}

/// @description  BktGlitch_set_line_drift(drift)
/// @function  BktGlitch_set_line_drift
/// @param drift
function BktGlitch_set_line_drift(argument0) {
	/*
	    Sets added scanline-y drift to horizontal lines.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.lineDrift], abs(argument0));



}

/// @description  BktGlitch_set_line_resolution(resolution)
/// @function  BktGlitch_set_line_resolution
/// @param resolution
function BktGlitch_set_line_resolution(argument0) {
	/*
	    Sets resolution of horizontal lines.
	    Range based around 0-1, no upper limit.
	    Less = more blocky
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.lineResolution], abs(argument0));



}

/// @description  BktGlitch_set_line_shift(offset)
/// @function  BktGlitch_set_line_shift
/// @param offset
function BktGlitch_set_line_shift(argument0) {
	/*
	    Sets base horizontal line offset.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.lineShift], abs(argument0));



}

/// @description  BktGlitch_set_line_speed(speed)
/// @function  BktGlitch_set_line_speed
/// @param speed
function BktGlitch_set_line_speed(argument0) {
	/*
	    Sets waving speed of horizontal lines.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.lineSpeed], abs(argument0));



}

/// @description  BktGlitch_set_line_vertical_shift(shift)
/// @function  BktGlitch_set_line_vertical_shift
/// @param shift
function BktGlitch_set_line_vertical_shift(argument0) {
	/*
	    Sets vertical wave offset (y position) of horizontal lines. 
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.lineVertShift], abs(argument0));



}

/// @description  BktGlitch_set_noise_level(noise level)
/// @function  BktGlitch_set_noise_level
/// @param noise level
function BktGlitch_set_noise_level(argument0) {
	/*
	    Sets level of noise.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.noiseLevel], abs(argument0));



}

/// @description  BktGlitch_set_resolution_of_application_surface()
/// @function  BktGlitch_set_resolution_of_application_surface
function BktGlitch_set_resolution_of_application_surface() {
	/*
	    Passes resolution to the shader set to the size of the application surface.
	    In most cases, this is what you'd want to use.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	if (surface_exists(application_surface)){
	    shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.resolution], surface_get_width(application_surface), surface_get_height(application_surface));
	}



}

/// @description  BktGlitch_set_rng_seed(seed)
/// @function  BktGlitch_set_rng_seed
/// @param seed
function BktGlitch_set_rng_seed(argument0) {
	/*
	    Changes seed used for random calculations - adds variation to all effects.
	    Can be for example used for single "hits". 
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.rngSeed], abs(argument0));



}

/// @description  BktGlitch_set_shakiness(shakiness)
/// @function  BktGlitch_set_shakiness
/// @param shakiness
function BktGlitch_set_shakiness(argument0) {
	/*
	    Sets "shakiness" of horizontal lines.
	    Range based around 0-1, no upper limit.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.shakiness], abs(argument0));



}

/// @description  BktGlitch_set_time(time)
/// @function  BktGlitch_set_time
/// @param time
function BktGlitch_set_time(argument0) {
	/*
	    Passes time variable to the shader, neeeds to change for animation.
	    Designed for a variable that increases by one every frame at 60 FPS.
    
	    ONLY RUN WHILE THE SHADER IS ACTIVE!
	*/

	shader_set_uniform_f(global.bktGlitchUniform[bktGlitch.time], abs(argument0));



}
