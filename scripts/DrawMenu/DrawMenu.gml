///Draw listed menu.
function DrawMenu(_buttons, _position, _x, _y, _active) {
	if (!_active)
	{
		_position = -1;	
	}

	var _len = array_length(_buttons);
	var _offset = 80;
	
	if (object_index == oLanguageSelect)
	{
		_offset = 240;	
	}

	var _start;
	if (_len & 1)
	{
		_start = _y - (_len div 2) * _offset;
	}
	else
	{
		_start = _y - (_len/2 - 1/2) * _offset;
	}

	var _scale, _angle;
	for (var i = 0; i < _len; ++i)
	{
		if (i == 0 && argument_count >= 6 && argument[5])
		{
			draw_set_color(c_white);
			_scale = 1;
			_angle = 0;
		}
		else if (i == 3 && argument_count >= 7 && argument[6])
		{
			draw_set_color(merge_color(c_white, c_gray, 0.9));
			_scale = 1;
			_angle = 0;
		}
		else if (_position == i)
		{
			draw_set_color(c_white);
			_scale = scale;
			_angle = angle;
		}
		else
		{
			draw_set_color(merge_color(c_white, c_gray, 0.9));
			_scale = 1;
			_angle = 0;	
		}
		
		var _add = 0;
		if (object_index == oMenu)
		{
			if (position == MenuPosition.OPTIONS && i == 0)
				_add = -font_get_size(fontTitle);
		}
			
		if (_buttons[i] == GetText(48) && global.progress < NUMBER_OF_LEVELS)
			draw_set_alpha(0.3);
		draw_text_ext_transformed(_x, _start + i * _offset + _add, _buttons[i], font_get_size(fontTitle) * 1.5, 2000, _scale, _scale, _angle);	
		draw_set_alpha(1.0);
	}


}


/// Draw block level select.
function DrawLevelMenu(_x0, _y0, _selected) {
	var _room_width = 1280;
	var _room_height = 720;

	var _block_size = sprite_get_width(sBlock) * 2;

	var _offset_x = 45;
	var _offset_y = 0;

	var _space_x = _block_size/4;
	var _space_y = _block_size/4;


	var _x = _offset_x;
	var _y = _offset_y;

	var _screen_w = 3 * _block_size;
	var _screen_h = 4 * _block_size;

	var _outline_thickness = 10;


	for (var i = 0; i < NUMBER_OF_LEVELS + 1; ++i)
	{
			
		if (i == _selected)
		{
			draw_set_color(c_green);
			draw_rectangle(_x0 + _x - _outline_thickness, _y0 + _y - _outline_thickness, _x0 + _x + _block_size + _outline_thickness, _y0 + _y + _block_size + _outline_thickness, false);	
			draw_set_color(c_white);
		}
	
		var _sprite;
		if (i == 22 || i == 19)	_sprite = sSpecial;
		else if (i == 24)		_sprite = sDemonic;
		else					_sprite = sBlock;
	
		if (global.progress < i - 1)	draw_set_alpha(0.5);
		draw_sprite_ext(_sprite, 0, _x0 + _x, _y0 + _y, 2, 2, 0, c_white, draw_get_alpha());
		if (i == 0)
		{
			var _str = 8;
			if (global.language == "Russian")
			{	
				draw_set_font(fontText);
			}
			draw_text(_x0 + _x + _block_size/2, _y0 + _y + _block_size/2, GetText(_str));	
			draw_set_font(fntTextMenu);
		}
		else
		{
			draw_text(_x0 + _x + _block_size/2, _y0 + _y + _block_size/2, string(i));	
		}
		draw_set_alpha(1);
		
		_x += _block_size + _space_x;
		if (_x >= _room_width - _offset_x - _screen_w - _space_y)
		{
			_y += _block_size + _space_y;
			_x = _offset_x;
		}
	}


}

