///@desc InitializeParticles
///@param all
/// Initialize plyer's or all particles
function InitializeParticles() {

	if (global.part_init)	return;
	
	global.part_init = true;

	global.part_system_const = part_system_create_layer("Bullets", true);
	global.back_pattern = part_system_create_layer("BackPattern", true);
	global.enemy_part = part_system_create_layer("Particles", true);
	global.part_system = part_system_create_layer("Particles", true);

}


///@desc RestartParticles
///@param all
//Clear player's particles or all particles
function RestartParticles() {

	if (argument_count == 0) 
	{
		part_particles_clear(global.back_pattern);
		part_particles_clear(global.part_system_const);
		part_particles_clear(global.enemy_part);
	}
	else if (argument[0] == false)
	{
		part_particles_clear(global.part_system_const);
	}

	part_particles_clear(global.part_system);

}