function DrawBlock(sprite, blend)
{
	
	var _width = sprite_get_width(sprite);
	var _size = _width div 3;
	
	var _x1 = x;
	var _y1 = y;
	var _x2 = x + sprite_width - _size;
	var _y2 = y + sprite_height - _size;
	
	var _w = sprite_width;
	var _h = sprite_height;
	
	
	
	//TOP LEFT CORNER
	draw_sprite_part_ext(sprite, 0, 0, 0, _size, _size, _x1, _y1, 1, 1, blend, 1);
	
	//TOP RIGHT CORNER
	draw_sprite_part_ext(sprite, 0, _width - _size, 0, _size, _size, _x2, _y1, 1, 1, blend, 1);

	//BOTTOM LEFT CORNER
	draw_sprite_part_ext(sprite, 0, 0, _width - _size, _size, _size, _x1, _y2, 1, 1, blend, 1);
	
	//TOP RIGHT CORNER
	draw_sprite_part_ext(sprite, 0, _width - _size, _width - _size, _size, _size, _x2, _y2, 1, 1, blend, 1);
	
	var _left = _size;
	var _right = _w - 2 * _size;
	
	//TOP + BOTTOM
	for (;_left < _right;)
	{
		draw_sprite_part_ext(sprite, 0, _size, 0, _size, _size, _x1 + _left, _y1, 1, 1, blend, 1);
		draw_sprite_part_ext(sprite, 0, _size, 0, _size, _size, _x1 + _right, _y1, 1, 1, blend, 1);
		
		draw_sprite_part_ext(sprite, 0, _size, _width - _size, _size, _size, _x1 + _left, _y2, 1, 1, blend, 1);
		draw_sprite_part_ext(sprite, 0, _size, _width - _size, _size, _size, _x1 + _right, _y2, 1, 1, blend, 1);
		
		_left += _size;
		_right -= _size;
	}
	
	if (_left - _right < _size)
	{
		draw_sprite_part_ext(sprite, 0, _size, 0, _size, _size, _x1 + _left, _y1, 1, 1, blend, 1);	
		draw_sprite_part_ext(sprite, 0, _size, _width - _size, _size, _size, _x1 + _left, _y2, 1, 1, blend, 1);
	}
	
	var _top = _size;
	var _bottom = _h - 2 * _size;
	
	//LEFT + RIGHT
	for (;_top < _bottom;)
	{
		draw_sprite_part_ext(sprite, 0, 0, _size, _size, _size, _x1, _y1 + _top, 1, 1, blend, 1);
		draw_sprite_part_ext(sprite, 0, 0, _size, _size, _size, _x1, _y1 + _bottom, 1, 1, blend, 1);
		
		draw_sprite_part_ext(sprite, 0, _width - _size, _size, _size, _size, _x2, _y1 + _top, 1, 1, blend, 1);
		draw_sprite_part_ext(sprite, 0, _width - _size, _size, _size, _size, _x2, _y1 + _bottom, 1, 1, blend, 1);
		
		_top += _size;
		_bottom -= _size;
	}
	
	if (_top - _bottom < _size)
	{
		draw_sprite_part_ext(sprite, 0, 0, _size, _size, _size, _x1, _y1 + _top, 1, 1, blend, 1);
		draw_sprite_part_ext(sprite, 0, _width - _size, _size, _size, _size, _x2, _y1 + _top, 1, 1, blend, 1);
	}
	
	
	//CENTER
	var _left = _size;
	var _right = _w - 2 * _size;
	
	
	for (; _left < _right; )
	{
		
		var _top = _size;
		var _bottom = _h - 2 * _size;
		
		for (; _top < _bottom; )
		{
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _left, _y1 + _top, 1, 1, blend, 1);
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _left, _y1 + _bottom, 1, 1, blend, 1);
			
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _right, _y1 + _top, 1, 1, blend, 1);
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _right, _y1 + _bottom, 1, 1, blend, 1);
				
			_top += _size;
			_bottom -= _size;
		}
		
		if (_top - _bottom < _size)
		{
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _left, _y1 + _top, 1, 1, blend, 1);
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _right, _y1 + _top, 1, 1, blend, 1);
		}
		
		_left += _size;
		_right -= _size;
	}
	
	if (_left - _right < _size)
	{
		var _top = _size;
		var _bottom = _h - 2 * _size;
		
		for (; _top < _bottom; )
		{
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _left, _y1 + _top, 1, 1, blend, 1);
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _left, _y1 + _bottom, 1, 1, blend, 1);
							
			_top += _size;
			_bottom -= _size;
		}
		
		if (_top - _bottom < _size)
		{
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + _left, _y1 + _top, 1, 1, blend, 1);
		}
	}
	
	
	/*
	var _columns = _w div _size;
	var _rows	 = _h div _size;
	
	//CORNERS
	//top left
	draw_sprite_part_ext(sprite, 0, 0, 0, _size, _size, _x1, _y1, 1, 1, blend, 1);
	
	//top right
	draw_sprite_part_ext(sprite, 0, _size * 2, 0, _size, _size, _x1 + ((_columns) * _size), _y1, 1, 1, blend, 1);
	
	//bottom left
	draw_sprite_part_ext(sprite, 0, 0, _size * 2, _size, _size, _x1, _y1 + ((_rows) * _size), 1, 1, blend, 1);
	
	//bottom right
	draw_sprite_part_ext(sprite, 0, _size * 2, _size * 2, _size, _size, _x1 + ((_columns) * _size), _y1 + ((_rows) * _size), 1, 1, blend, 1);
	
	
	//EDGES
	for (var i = 1; i < (_rows); ++i)
	{
		//left edge
		draw_sprite_part_ext(sprite, 0, 0, _size, _size, _size, _x1, _y1 + (i * _size), 1, 1, blend, 1);
		
		//right edge
		draw_sprite_part_ext(sprite, 0, _size * 2, _size, _size, _size, _x1 + (_columns * _size), _y1 + (i * _size), 1, 1, blend, 1);
		
	}
	
	for (var i = 1; i < (_columns); ++i)
	{
		//top edge
		draw_sprite_part_ext(sprite, 0, _size, 0, _size, _size, _x1 + (i * _size), _y1, 1, 1, blend, 1);
		
		//bottom edge
		draw_sprite_part_ext(sprite, 0, _size, _size * 2, _size, _size, _x1 + (i * _size), _y1 + (_rows * _size), 1, 1, blend, 1);
		
	}
	

	//MIDDLE
	for (var i =  1; i < (_columns); ++i)
	{
		for (var j = 1; j < (_rows); ++j)
		{
			draw_sprite_part_ext(sprite, 0, _size, _size, _size, _size, _x1 + (i * _size), _y1 + (j * _size), 1, 1, blend, 1);
		}
	}
	*/
}