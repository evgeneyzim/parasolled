///Activate fans
function ButtonActivateFan() {
	for (var i = 0; i < array_length(linked_fans); ++i)
	{
		with (linked_fans[i])  active = true;
	}
}

///Create portal
function ButtonCreatePortal() {
	
	instance_create_layer(portal_x, portal_y, "PortalBlocks", oPortal);

	with (oPortalAura)
	{
		start = true;
		image_alpha = 0;
		with (oCamera)  
		{
			follow = other.id;
			zoom = 0.9;
		}	
	}
}

///Open doors
function ButtonOpenTheDoor() {
	
	for (var i = 0; i < array_length(linked_doors); ++i)
	{
		with (linked_doors[i])	start = true;
	}
}


function ButtonStopGear() {
	for (var i = 0; i < array_length(linked_gears); ++i)
	{
		with (linked_gears[i]) 
		{
			saved_spd = spd;
			spd = 0;
		}
	}
}
