/// Get number from level
function GetNumberFromLevel(_level) {

	if (_level == rJumpIntro)			return 1;
	if (_level == rDoubleJumpIntro)		return 2;
	if (_level == rParasolIntro)		return 3;
	if (_level == rBigJump)				return 4;
	if (_level == rBigRoom)				return 5;
	if (_level == rCareful)				return 6;
	if (_level == rFloating)			return 7;
	if (_level == rGearIntro)			return 8;
	if (_level == rReturnIntro)			return 9;
	if (_level == rButtonIntro)			return 10;
	if (_level == rButtonFan)			return 11;
	if (_level == rDestroyBlocks)		return 12;
	if (_level == rThrowIntro)			return 13;
	if (_level == rSlimeIntro)			return 14;
	if (_level == rSlimeFight)			return 15;
	if (_level == rGuards)				return 16;
	if (_level == rHorizontalFan)		return 17;
	if (_level == rFloatingReverse)		return 18;
	if (_level == rChase)				return 19;
	if (_level == rTeapot)				return 20;
	if (_level == rShieldIntro)			return 21;
	if (_level == rBossfight)			return 22;
	if (_level == rGun)					return 23;
	if (_level == rFinal)				return 24;
	

	
	if (_level == rSandbox)				return -1;
	if (_level == rCluster)				return -1;
	if (_level == rClusterIntro)		return -1;

	Raise("Wrong level in function GetNumberFromLevel");
}

//Return level from number
function LevelMap(_n) {
	if (_n >= NUMBER_OF_LEVELS)	_n = NUMBER_OF_LEVELS - 1;
	
	static _rooms = [rJumpIntro, rDoubleJumpIntro, rParasolIntro, rBigJump, rBigRoom, rCareful, rFloating, rGearIntro, 
			rReturnIntro, rButtonIntro, rButtonFan, rDestroyBlocks, rThrowIntro, rSlimeIntro, rSlimeFight, rGuards, 
			rHorizontalFan, rFloatingReverse, rChase, rTeapot,
			rShieldIntro, rBossfight, rGun, rFinal];
		
	return _rooms[_n];
}


//Restart level
function RestartLevel(_dead, _start_of_the_game) {
	
	instance_activate_all();
	
	//Shake gamepad
	if (_dead)
	{
		if (gamepad_is_connected(SelectGamepad(0)))  gamepad_set_vibration(SelectGamepad(0), 0, 0);
	}

	//Set timer to restart the room
	oLevelControl.restart_room = oLevelControl.restart_room_max;

	//Zoom camera out
	oCamera.zoom = 1;
	oCamera.timer_special = 0;
	
	//Stop slow motion
	global.slow_motion = 0;
	room_speed = NORMAL_SPEED;

	//If player died, explode him
	with (oPlayer)  explode = _dead;

	//Do not explode slime
	with (oSlime)	explode = false;
	with (oGuard)	explode = false;
	with (oGoon)	explode = false;
	
	with (oSlimeSpawner)
	{
		ent = ent_start;
		length = array_length(ent);
	}

	if (room == rFinal)
	{
		with(oDeeJay)
		{
			bgm = musMus;
			silence_timer = 0;
			musNext = -1;
		}
	}


	//Destroy respawnable objects
	with (oPlayer)	instance_destroy(spotlight);
	instance_destroy(oPlayer);
	with (oParasol)
	{
		if (!immortal)	instance_destroy();
	}
	instance_destroy(oEnergy);
	instance_destroy(oStaticSquare);
	instance_destroy(oSlime);
	instance_destroy(oGuard);
	instance_destroy(oGoon);
	if (global.destroy_blocks)
		instance_destroy(oDestructBlock);
	instance_destroy(oCube);
	with (oGun)
	{
		if (!immortal)	instance_destroy();	
	}

	//Respawn objects	
	
	if (instance_exists(oPlayerSpawnPrev))
	{
		with (oPlayerSpawnPrev)	instance_create_layer(x, y, "Player", oPlayer);	
	}
	else
	{
		with (oPlayerSpawn)	instance_create_layer(x, y, "Player", oPlayer);
	}
	
	with (oPlayer)	
	{
		if (instance_exists(oSpotlightController))
			spotlight = instance_create_layer(x, y, "Control", oSpotlight);
	}
	
	with (oParasolSpawn)			
	{
		if (blocks_dependant)
		{
			if (!global.destroy_blocks)
			{
				with (instance_create_layer(x, y, "Parasol", oParasol))  image_angle = other.image_angle;
			}
		}
		else
		{
			if (room != rFloatingReverse || global.destroy_blocks)
			{
				with (instance_create_layer(x, y, "Parasol", oParasol))  image_angle = other.image_angle;
			}
		}
		
	}
	
	with (oEnergySpawn)				instance_create_layer(x, y, "Items", oEnergy);
	with (oStaticSquareSpawner)     instance_create_layer(x, y, "Spikes", oStaticSquare);
	with (oGoonSpawn)				
	{
		created_object = instance_create_layer(x, y, "Enemies", oGoon);
		with (created_object)
		{
			functions = other.functions;
			deactivators = other.deactivators;
			marked = other.marked;
		}
		for (var i = 0; i < array_length(deactivators); ++i)
		{
			deactivators[i]();	
		}
		
	
		created_object.gun = instance_create_layer(x, y, "Parasol", oGun);
		created_object.state = GoonStates.WANDER;
		created_object.gun.host = created_object;
		created_object.gun.state = GunStates.LOAD;	
		
		created_object.gun.infinity_gun = infinity_gun;
	}
	
	with (oSlimeSpawn)			    
	{
		created_object = instance_create_layer(x, y, "Enemies", oSlime);
		with (created_object)
		{
			//Pass parametres
			functions = other.functions;
			fan_activator = other.fan_activator;
			door_opener = other.door_opener;
			
			if (fan_activator) deactivators = other.deactivators;
			if (door_opener)   closers = other.closers;
			
			state = other.state;
			
			if (fan_activator)
			{
				for (var i = 0; i < array_length(deactivators); ++i)
				{
					deactivators[i]();	
				}
			}
			
			if (door_opener)
			{
				for (var i = 0; i < array_length(closers); ++i)
				{
					closers[i]();	
				}
			}
					
		}
	}
		
	with (oGuardSpawn)			    
	{
		created_object = instance_create_layer(x, y, "Enemies", oGuard);
		with (created_object)
		{
			//Pass parametres
			functions = other.functions;
			fan_activator = other.fan_activator;
			door_opener = other.door_opener;
			
			if (fan_activator) deactivators = other.deactivators;
			if (door_opener)   closers = other.closers;
			
			state = other.state;
			
			if (fan_activator)
			{
				for (var i = 0; i < array_length(deactivators); ++i)
				{
					deactivators[i]();	
				}
			}
			
			if (door_opener)
			{
				for (var i = 0; i < array_length(closers); ++i)
				{
					closers[i]();	
				}
			}
					
		}
	}
	
	with (oDestructBlockDrawController)
	{
		instance_destroy();
		instance_create_layer(x, y, layer, oDestructBlockDrawController);
	}
	
	with (oSpotlight)
	{
		if (destroy_it)	instance_destroy();
	}
	
	if (global.destroy_blocks)
	{
		with (oBlockSpawner)
		{
			with (instance_create_layer(x, y, "DestructBlocks", oDestructBlock))
			{
				//Pass parametres
				image_xscale = other.image_xscale;
				image_yscale = other.image_yscale;
				hp = other.hp;
				draw_x = other.draw_x;
				draw_y = other.draw_y;
		
				var _scratch_width = sprite_get_width(sScratch);
				var _scratch_height = sprite_get_height(sScratch);
		
				draw_x = clamp(draw_x, _scratch_width/2, sprite_width - _scratch_width/2);
				draw_y = clamp(draw_y, _scratch_height/2, sprite_height - _scratch_height/2);
				
				
				//if (image_xscale == 1 && image_yscale == 1)
				//{
				//	sprite_index = sDestBlockNormal;	
				//}
				//else if (image_xscale == 1.5 && image_yscale == 1)
				//{
				//	sprite_index = sDestBlockLong;	
				//}
				//else if (image_xscale == 1 && image_yscale == 1.5)
				//{
				//	sprite_index = sDestBlockTall;	
				//}
				//else if (image_xscale == 1.5 && image_yscale == 1.5)
				//{
				//	sprite_index = sDestBlockBig;	
				//}
	
				//image_xscale = 1;
				//image_yscale = 1;
				
				
				
			}
		}
	}
	with (oCubeSpawner)
	{
		with (instance_create_layer(x, y, "Box", oCube))
		{
			//Pass parametres
			image_xscale = other.image_xscale;
			image_yscale = other.image_yscale;
		}
	}
	
	
	//Goon Controller
	with (oGoonsControl)
	{
		for (var i = 0; i < array_length(deactivators); ++i)
		{
			deactivators[i]();	
		}
	}
	
	//Reset trigger areas
	with (oTriggerArea)	
	{
		active = true;
		for (var i = 0; i < array_length(deactivators); ++i)
		{
			deactivators[i]();
		}
	}

	
	//Minigame
	with (oMinigame)
	{
		deactivator();	
	}
	
	with (oButtonController)
	{
		amount = 0;
		prev_id = -1;
		with (inst_537606CD)
		{
			start = false;
			spd = 8;
		}
		change = true;
		
		with (inst_3300AC11) start = true;
		
		with (oButtonPress)
		{
			functions[0] = function()
			{
				state = ButtonPressStates.PRESSED;	
			}
		}
					
	}
	
	with (oAlarm)
	{
		instance_destroy();	
	}
	
	//Reset Press Buttons
	with (oButtonPress)
	{
		state = ButtonPressStates.PRESSED;
		
		state_changer = false;
		special = false;
		
		for (var i = 0; i < array_length(deactivators); ++i)
		{
			deactivators[i]();	
		}
	}

	//Set colors
	with (oSpike)        image_blend = oDrawer.c_spikes;
	with (oParasol)      image_blend = oDrawer.c_parasol;
	with (oStaticSquare) image_blend = oDrawer.c_squares;
	with (oCube)		 image_blend = oDrawer.c_box;


	//Respawn static squares
	with (oClusterControl)
	{
		if (portal_summoner)
		{
			instance_destroy(oPortal);
			instance_destroy(oPortalAura);
		}
		else
		{
			deactivators[0]();	
		}
	
		squares_len = 0;
		with (oStaticSquare)
		{
			other.squares[other.squares_len++] = id;	
		}
		
		done = false;
	}

	//Disable all buttons
	with (oButton)  
	{
		if (!immortal)
			image_index = ButtonState.DISABLED;
		
		
		
		if (!_start_of_the_game)
		{
			for(var i = 0; i < array_length(deactivators); ++i)
			{
				deactivators[i]();	
			}
		}
	}


	//Restart gears
	with (oGear)
	{
		if (!_start_of_the_game && saved_spd != 0)  spd = saved_spd;	
	}


	//Set back oChaser
	with (oChaser)
	{	
		instance_create_layer(xstart, ystart, "Boss", oChaser);
		instance_destroy();
	}

	//Set boss to its original state
	if (!_start_of_the_game)
	{
		with (oBoss)  
		{
			hp = max_hp;
			if (!instance_exists(oPlayerSpawnPrev) && state != BossStates.INTRO)
				state = BossStates.TOSTART;
		}
		
		if (room == rBossfight && !instance_exists(oBoss))
		{
			with (oDeeJay)
			{
				bgm = musBossIntro;
				musNext = musBoss2;
				silence_timer = MUS_INTRO_LENGTH;
			}
			
			
			with (oPortalAura)	instance_destroy();
			with (oPortal)		instance_destroy();
			with (instance_create_layer(960, -256, "Boss", oBoss))
			{
				state = BossStates.INTRO;
			}	
		}
	}
	
	with (oFinalBoss)
	{
		image_index = 0;
		x = xstart;
		y = ystart;
	}

	//Restart particles
	RestartParticles(true);
	
}