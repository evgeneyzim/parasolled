///@desc SelectGamepad(gp);
///@arg gamepad
function SelectGamepad(argument0) {

	/// Find connected gamepads and select one of them as an input type.

	var _index = argument0;

	var _list = ds_list_create();
	var _slots = gamepad_get_device_count();

	for (var i = 0; i < _slots; ++i)
	{
		if (gamepad_is_connected(i))
			ds_list_add(_list, i)
	}

	var _res = ds_list_find_value(_list, _index);

	ds_list_destroy(_list);

	if (_res == undefined) 
		return -1;
	else                 
		return _res;
}

function GamepadAnykey(_device) {
	var _res = gamepad_button_check_pressed(_device, gp_face1) || gamepad_button_check_pressed(_device, gp_face2) || gamepad_button_check_pressed(_device, gp_face3) || gamepad_button_check_pressed(_device, gp_face4) || gamepad_button_check_pressed(_device, gp_start) || gamepad_button_check_pressed(_device, gp_select) || gamepad_button_check_pressed(_device, gp_stickl) || gamepad_button_check_pressed(_device, gp_stickr) || gamepad_button_check_pressed(_device, gp_padl) || gamepad_button_check_pressed(_device, gp_padr) || gamepad_button_check_pressed(_device, gp_padu) || gamepad_button_check_pressed(_device, gp_padd) || gamepad_button_check_pressed(_device, gp_shoulderl) || gamepad_button_check_pressed(_device, gp_shoulderr) || gamepad_button_check_pressed(_device, gp_shoulderlb) || gamepad_button_check_pressed(_device, gp_shoulderrb);
	return _res;
}