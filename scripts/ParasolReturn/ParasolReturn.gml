function ParasolReturn()
{
	image_speed = 1;
	sprite_index = sParasolClosedMask;
	
	if (instance_exists(oPlayer))
	{
		
		with (instance_place(x, y, oBoss))
		{
			if (ds_list_find_index(other.damage_list, id) == -1)
			{
				ds_list_add(other.damage_list, id);
				
				DamageBoss();	
				
			}
			
		}
		
		moving = true;
		
		var _x_to = oPlayer.x;
		var _y_to = oPlayer.y;
		
		
		var _dir = point_direction(_x_to, _y_to, x, y);
			
		var _dumping = _dir - image_angle;
		if (_dumping > 0)
		{
			if (360 - _dumping < _dumping)  _dumping = -(360 - _dumping);	
		}
		else
		{
			var _tmp_dumping = -_dumping;
			if (360 - _tmp_dumping < _tmp_dumping)  _tmp_dumping = -(360 - _tmp_dumping);	
			_dumping = -_tmp_dumping;
		}
			
		image_angle += _dumping / 5;
		
		
		xspeed = lengthdir_x(1.5 * thrown_speed, 180 + _dir);
		yspeed = lengthdir_y(1.5 * thrown_speed, 180 + _dir);
		
		x += xspeed;
		y += yspeed;	
		
	}
	else
	{
		instance_destroy();	
	}
}