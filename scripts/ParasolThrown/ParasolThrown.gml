function ParasolThrown() 
{
	//Sprite	
	if (place_meeting(x, y, oGoon))
	{
		sprite_index = sParasolClosedMask;	
	}
	else
	{
		sprite_index = sParasolClosed;	
	}
		
	can_be_picked_up = max(0, can_be_picked_up - 1);
	if (moving)  
	{
		with (instance_place(x, y, oBoss))
		{
			if (ds_list_find_index(other.damage_list, id) == -1)
			{
				ds_list_add(other.damage_list, id);
				
				DamageBoss();	
				
			}
		}
		
		
		can_be_picked_up = can_be_picked_up_max;
		//Particles
		var len = point_distance(x, y, x + lengthdir_x(thrown_speed, image_angle),y + lengthdir_y(thrown_speed, image_angle));
		var ang = image_angle;
		var _x = x;
		var _y = y;
	
		for (var i = 0; i < len; ++i)
		{	
			part_particles_create(global.part_system, _x + lengthdir_x(i, ang), _y + lengthdir_y(i, ang), trail_smoke, 1);
		}

		//Set speed
		xspeed = lengthdir_x(thrown_speed, image_angle);
		yspeed = lengthdir_y(thrown_speed, image_angle);	

		//Set point
		var _parasol_x = x + lengthdir_x(PARASOL_WIDTH, image_angle);
		var _parasol_y = y + lengthdir_y(PARASOL_WIDTH, image_angle);
		
		//If meeting a block
		if (position_meeting(_parasol_x + xspeed, _parasol_y + yspeed, oBlock))
		{
			//Move to the block	
			var _block;
			do
			{
				_parasol_x += lengthdir_x(1, image_angle);	
				_parasol_y += lengthdir_y(1, image_angle);	
				_block = instance_position(_parasol_x, _parasol_y, oBlock);
			} until (_block);
		
			//Activate button
			with (_block.button)
			{
				if (image_index == ButtonState.DISABLED)
				{
					image_index = ButtonState.ENABLED;
					audio_play_sound(soButtonActivate, 1, 0);
					for (var i = 0; i < array_length(functions); ++i)  functions[i]();	
				}
			}
		
			//Deal damage
			if (_block.object_index == oDestructBlock)
			{
				_block.hp--;
				if (_block.hp == 0)	return;
			}	
		
			//Set block position		
			var _left, _right, _top, _bottom;
			var _center_x, _center_y;
		
			with (_block)
			{
				_left = bbox_left + 1;
				_right = bbox_right - 1;
				_top = bbox_top + 1;
				_bottom = bbox_bottom - 1;
				_center_x = (_left + _right)/2;
				_center_y = (_top + _bottom)/2;
			}	
		
			//Set parasol surface
			var _surface = -1;
			if (_parasol_y > _top && _parasol_y < _bottom)
			{
				if (x < _left)  _surface = CollidingSurface.LEFT;
				else            _surface = CollidingSurface.RIGHT;	
			}
			if (_parasol_x > _left && _parasol_x < _right)
			{
				if (y < _top)  _surface = CollidingSurface.TOP;
				else           _surface = CollidingSurface.BOTTOM;
			
			}
		
			desired_angle = image_angle;
				
			//Waving
			amplitude = 15;
			phase = get_timer();
			
			point_x = _parasol_x - _block.x;
			point_y = _parasol_y - _block.y;
			
			xspeed = 0;
			yspeed = 0;
			moving = false;
		
			linked_block = _block;
			surface = _surface;
		
		
			with (linked_block)
			{
				//Place scratch
				draw_x = _parasol_x - x;
				draw_y = _parasol_y - y;
		
				var _scratch_width = sprite_get_width(sScratch);
				var _scratch_height = sprite_get_height(sScratch);
		
				draw_x = clamp(draw_x, _scratch_width/2, sprite_width - _scratch_width/2);
				draw_y = clamp(draw_y, _scratch_height/2, sprite_height - _scratch_height/2);
		
			}
		
			//Play sound
			audio_play_sound(soParasolHitGround, 1, false);
										
		}
	
		//If there was no collision, go forward
		x += xspeed;
		y += yspeed;
	}
	else
	{
		ds_list_clear(damage_list);
		
		if (!instance_exists(linked_block))
		{
			instance_destroy();
			exit;
		}
		//Waving
		image_angle = desired_angle + sin((phase - get_timer() * global.slow_motion_effect)/10000) * amplitude;
		amplitude = max(0, amplitude - 0.5);
			
		//Position the block
		x = linked_block.x + point_x - lengthdir_x(PARASOL_WIDTH + sin((phase - get_timer())/10000) * amplitude/5, image_angle);
		y = linked_block.y + point_y - lengthdir_y(PARASOL_WIDTH + sin((phase - get_timer())/10000) * amplitude/5, image_angle) + additional;
	
		//Being smashed by oCube
		if (linked_block.object_index == oCube && linked_block.yspeed > 0 && surface == CollidingSurface.BOTTOM && position_meeting(x + lengthdir_x(PARASOL_WIDTH, image_angle), linked_block.bbox_bottom + linked_block.yspeed + 1, oBlock))
		{
			part_particles_create(global.part_system, x + lengthdir_x(irandom(PARASOL_WIDTH), image_angle), y + lengthdir_y(irandom(PARASOL_WIDTH), image_angle), death, 50);
			instance_destroy();	
		}
	
		//Set different mask
		sprite_index = sParasolMask;
	}


}
