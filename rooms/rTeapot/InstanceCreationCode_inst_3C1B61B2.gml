functions[0] = function()
{
	with (inst_53A035B9)	active = true;
	with (inst_6B5DAA42)	start = false;
	
	with (oCamera)
	{
		timer_special = timer_special_max;
		follow = inst_62AA9D58;
		zoom = 1;
	}
}

deactivators[0] = function()
{
	with (inst_53A035B9)	active = false;
	with (inst_6B5DAA42)	start = true;
}