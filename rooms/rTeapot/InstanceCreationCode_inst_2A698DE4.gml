functions[0] = function()
{
	with (inst_7B7DBEC5) start  = true;
	with (inst_42F2F22A) active = false;
	
}

functions[1] = function()
{
	with (inst_6B5DAA42) start  = true;
}


deactivators[0] = function()
{
	with (inst_7B7DBEC5) start  = false;
	with (inst_42F2F22A) active = true;
	
}

deactivators[1] = function()
{
	with (inst_6B5DAA42) start  = false;
}	