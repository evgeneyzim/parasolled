functions[0] = function()
{
	with (inst_624313F)	start = true;
}

functions[1] = function()
{
	with (oCamera)
	{
		timer_special = timer_special_max;
		follow = inst_624313F;
	}
}

deactivators[0] = function()
{
	with (inst_624313F)	start = false;
}