functions[0] = function()
{
	
	with (inst_36939824)
	{
		active = true;	
	}
	
	with (inst_6526476E)
	{
		start = true;	
		spd = 0.5;
	}
	
	with (inst_A930FF4)
	{
		start = true;	
		spd = 0.5;
	}
	
	with (inst_2C5F7F6D) start = true;
	
	
}

deactivators[0] = function()
{
	
	with (inst_36939824)
	{
		active = false;	
	}
	
	with (inst_6526476E)
	{
		start = false;	
		spd = 8;
	}
	
	with (inst_A930FF4)
	{
		start = false;	
		spd = 8;
	}
	
	with (inst_2C5F7F6D) start = false;
	
}