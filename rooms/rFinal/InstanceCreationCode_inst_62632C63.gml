functions[0] = function()
{
	with (inst_4C1C9C4D) start = true;	
	with (oCamera)
	{
		timer_special = timer_special_max;
		follow = inst_4C1C9C4D;
	}
}

deactivators[0] = function()
{
	with (inst_4C1C9C4D) start = false;	
}