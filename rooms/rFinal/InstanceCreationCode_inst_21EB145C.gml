functions[0] = function()
{
	instance_destroy(oPlayerSpawnPrev);
	
	instance_destroy(inst_75FEDABF);
	
	instance_destroy(inst_34D1AA17);
	
	instance_destroy(oGuardSpawn);
	instance_destroy(oGoonSpawn);
	
	with (inst_62632C63)
	{
		immortal = true;
		functions = [];
		deactivators = [];
	}
	
	with (inst_66897759)
	{
		functions = [];
		deactivators = [];
		immortal = true;
	}
	
	with (oGun)
	{
		immortal = true;	
	}
	
	var _par = -1;
	
	with (oParasol)
	{
		if (_par == -1)
		{
			_par = id;
		}	
		else
		{
			var _dist1 = distance_to_object(inst_62632C63);
			var _dist2;
			with (_par)
			{
				_dist2 = distance_to_object(inst_62632C63); 	
			}
			
			if (_dist1 < _dist2)
			{
				_par = id;	
			}
		}
	}
	
	with (_par)
	{
		immortal = true;	
	}
	
	
	instance_destroy(inst_518FA6F6);
	
	instance_create_layer(oPlayerSpawn.x, oPlayerSpawn.y, "Parasol", oParasolSpawn);
	
	instance_destroy(oMinigame);
	
	instance_destroy();
	
}
