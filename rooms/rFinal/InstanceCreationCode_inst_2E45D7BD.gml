functions[0] = function()
{
	with (inst_74DD3EC3) start = true;
	with (oFinalBoss)	state = FinalBossStates.ROTATE;
	with (oCamera)		
	{
		timer_special = timer_special_max;
		follow = oFinalBoss;
	}
	
	with (oButtonController)	start = true;
	
	with (oDeeJay)
	{
		silence_timer = MUS_INTRO_LENGTH;
		bgm = musBossIntro;
		musNext = musBoss2;
	}
}

deactivators[0] = function()
{
	with (inst_74DD3EC3) start = false;
	with (oFinalBoss)	
	{
		state = FinalBossStates.START;
	}
	
	with (oButtonController)	start = false;
}