repeatable = true;
label = "R";

functions[0] = function()
{
	
	if (!instance_exists(oPlayer) || (oPlayer.y - y) > -10)
	{
		
		global.destroy_blocks = true;
	
		with (oDestructBlock)
		{
			explode = false;
			instance_destroy();
		}
	
		with (oBlockSpawner)
		{
			with (instance_create_layer(x, y, "DestructBlocks", oDestructBlock))
			{
				//Pass parametres
				image_xscale = other.image_xscale;
				image_yscale = other.image_yscale;
				hp = other.hp;
				draw_x = other.draw_x;
				draw_y = other.draw_y;
		
				var _scratch_width = sprite_get_width(sScratch);
				var _scratch_height = sprite_get_height(sScratch);
		
				draw_x = clamp(draw_x, _scratch_width/2, sprite_width - _scratch_width/2);
				draw_y = clamp(draw_y, _scratch_height/2, sprite_height - _scratch_height/2);
				
			}
		}
	}
}