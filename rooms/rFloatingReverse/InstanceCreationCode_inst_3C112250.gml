functions[0] = function() {
	instance_create_layer(1152, 288, "PortalBlocks", oPortal);
	with (oPortalAura)
	{
		start = true;
		image_alpha = 0;
		with (oCamera)  
		{
			follow = other.id;
			zoom = 0.9;
			timer_special = timer_special_max;
		}	
	}	
}



functions[1] = function() {
	with (inst_1F3211CB)	active = true;
}


deactivators[0] = function() {
	with (inst_1F3211CB)	active = false;
	instance_destroy(oPortal);
	instance_destroy(oPortalAura);
}
