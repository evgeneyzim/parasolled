functions[0] = function()
{
	with (inst_5C62E352)	start = true;
	
	with (oCamera)
	{
		follow = inst_5C62E352;
		timer_special = timer_special_max;
	}
}

deactivators[0] = function()
{
	with (inst_5C62E352)	start = false
}