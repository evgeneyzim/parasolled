functions[0] = function()
{
	with (inst_6BA23A0E)	active = true;
	
	with (oCamera)
	{
		follow = inst_6BA23A0E;
		timer_special = timer_special_max;
	}
}

deactivators[0] = function()
{
	with (inst_6BA23A0E)	active = false
}