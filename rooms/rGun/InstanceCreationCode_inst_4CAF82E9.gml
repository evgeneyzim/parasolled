functions[0] = function()
{
	with (inst_5E2B1C92)
	{
		functions = [];
		deactivators = [];
		immortal = true;
	}
	
	with (inst_500E26D0)
	{
		functions = [];
		deactivators = [];
		immortal = true;
	}
	
	
	instance_create_layer(oPlayerSpawn.x, oPlayerSpawn.y, "Parasol", oParasolSpawn);

	
	instance_destroy(inst_41157A2D);
	instance_destroy(oPlayerSpawnPrev);
	instance_destroy();
}