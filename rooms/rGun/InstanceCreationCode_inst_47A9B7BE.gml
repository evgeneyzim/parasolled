functions[0] = function()
{
	with (inst_99DB2FA)	start = true;	
	with (inst_6A2FA2C3) active = false;
	instance_activate_object(inst_456F177E);
	with (inst_456F177E) 
	{
		start = true;
	}
	with (inst_59C1B160) 
	{
		start = true;
	}
}

deactivators[0] = function()
{
	with (inst_99DB2FA) start = false;
	with (inst_6A2FA2C3) active = true;
	instance_activate_object(inst_456F177E);
	with (inst_456F177E) 
	{
		start = false;
	}
	with (inst_59C1B160) 
	{
		start = false;
	}
}
