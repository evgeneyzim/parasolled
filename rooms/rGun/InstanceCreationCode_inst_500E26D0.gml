functions[0] = function()
{
	with (inst_99DB2FA)  start = false;
	with (inst_62E75256) active = true;
	with (inst_5817D2F1) start = true;
	with (oCamera)
	{
		timer_special = timer_special_max;
		follow = inst_5817D2F1;
	}
	with (inst_59C1B160)	start = false;
	
}

deactivators[0] = function()
{
	with (inst_62E75256) active = false;
	with (inst_5817D2F1) start = false;
}