functions[0] = function() {
	with (inst_4A9A6877) {
		start = true;	
	}
	with (oCamera)
	{
		timer_special = timer_special_max;
		follow = inst_4A9A6877;
	}
}

deactivators[0] = function() {
	with (inst_4A9A6877) {
		start = false;	
	}
}