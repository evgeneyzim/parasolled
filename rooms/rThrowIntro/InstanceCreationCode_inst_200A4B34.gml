functions[0] = function()
{
	instance_destroy(oPlayerSpawnPrev);
	
	instance_destroy(oParasolSpawn);
	
	instance_create_layer(oPlayerSpawn.x, oPlayerSpawn.y, "Parasol", oParasolSpawn);
	
	with (inst_4E87FB95)
	{
		functions = [];
		deactivators = [];
	}
	
	instance_destroy();
}