repeatable = true;

functions[0] = function()
{
	with (oMusicGameControllers)
	{
		if (!inactive)
		{
			read = true;
			data = 2;
		}
		else
		{
			audio_play_sound(soThirdNote, 0, false);	
		}
	}
}