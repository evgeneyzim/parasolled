functions[0] = function()
{
	with (inst_7C9B89B7)	start = true;
	
	with (oCamera)
	{
		timer_special = timer_special_max;
		follow = oParasol;
	}
}

deactivators[0] = function()
{
	with (inst_7C9B89B7)	start = false;
}