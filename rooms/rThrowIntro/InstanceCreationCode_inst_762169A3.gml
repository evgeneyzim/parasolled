repeatable = true;

functions[0] = function()
{
	with (oMusicGameControllers)
	{
		if (!inactive)
		{
			read = true;
			data = 1;
		}
		else
		{
			audio_play_sound(soSecondNote, 0, false);	
		}
	}
}
