repeatable = true;

functions[0] = function()
{
	with (oMusicGameControllers)
	{
		if (!inactive)
		{
			read = true;
			data = 0;
		}
		else
		{
			audio_play_sound(soFirstNote, 0, false);	
		}
	}
}

