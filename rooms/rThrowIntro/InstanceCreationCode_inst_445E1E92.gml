functions[0] = function()
{
	with (inst_7943CD47)	start = true;	
	with (oMusicGameControllers)
	{
		GetNewOrder();
		play = true;
	}
	
	with (oPlayer)
	{
		blocked = true;	
	}
}

functions[1] = function()
{
	with (oMusicGameControllers)
	{
		inactive = false;
	}
}

deactivators[0] = function()
{
	with (inst_7943CD47)	start = false;	
	with (inst_61CA6AE4)	start = false;	
}