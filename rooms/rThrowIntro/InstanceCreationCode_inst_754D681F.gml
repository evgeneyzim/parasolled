functions[0] = function()
{
	with (inst_20C8C651)	start = true;
	with (oCamera)
	{
		follow = inst_20C8C651;
		timer_special = timer_special_max;
	}
}

deactivators[0] = function()
{
	with (inst_20C8C651)	start = false;
}