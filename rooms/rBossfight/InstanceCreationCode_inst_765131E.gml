functions[0] = function()
{
	with (inst_6EA6AFDA) start = true;
	instance_destroy(oPlayerSpawnPrev);
	with (oBoss)
		state = BossStates.INTRO;
		
	with (oDeeJay)
	{
		bgm = musBossIntro;
		musNext = musBoss2;
		silence_timer = MUS_INTRO_LENGTH;
	}	
		
	instance_destroy();
}