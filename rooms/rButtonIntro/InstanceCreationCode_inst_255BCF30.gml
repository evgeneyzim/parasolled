functions[0] = function () {
	instance_create_layer(1152, 480, "PortalBlocks", oPortal);
	with (oPortalAura)
	{
		start = true;
		image_alpha = 0;
		with (oCamera)  
		{
			timer_special = timer_special_max;
			follow = other.id;
			zoom = 0.9;
		}	
	}	
}

deactivators[0] = function() 
{
	instance_destroy(oPortal);	
	instance_destroy(oPortalAura);	
}