state = SlimeState.IDLE;

functions[0] = function()
{
	with (inst_65940EE) start = true;	
}

functions[1] = function()
{
	with (inst_29FD4E6D)	start = true;
	with (inst_1B)	
	{
		with (created_object)	state = SlimeState.FIGHT;
	}
}

door_opener = true;

closers[0] = function()
{
	with (inst_65940EE) start = false;	
}

closers[1] = function()
{
	with (inst_29FD4E6D)	start = false;
}