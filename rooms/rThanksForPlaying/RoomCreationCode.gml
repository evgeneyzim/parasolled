if (instance_exists(oSpeedrunner))
{
	with (oSpeedrunner)
	{
		if (global.speedrun_highscore == -1 || total_time < global.speedrun_highscore)
		{
			global.speedrun_highscore = total_time;	
		}
		
		SaveGame();		
		
	}
	
	room_goto(rSpeedrunResults);
}
else
	room_goto(rEnd);