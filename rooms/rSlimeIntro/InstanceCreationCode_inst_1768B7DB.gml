functions[0] = function()
{
	instance_destroy(oPlayerSpawnPrev);
	
	instance_destroy(oParasolSpawn);
	
	instance_create_layer(oPlayerSpawn.x, oPlayerSpawn.y, "Parasol", oParasolSpawn);
	
	global.destroy_blocks = false;
	
	instance_destroy();
}