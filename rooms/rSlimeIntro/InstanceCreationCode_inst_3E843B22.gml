functions[0] = function() {
	with (inst_17D6C48B)	active = true;
}

functions[1] = function() {
	with (inst_6E29B8DC)	start = true;
}

deactivators[0] = function() {
	with (inst_17D6C48B)	active = false;
}

closers[0] = function() {
	with (inst_6E29B8DC)	start = false;
}

fan_activator = true;
door_opener = true;

state = SlimeState.IDLE;