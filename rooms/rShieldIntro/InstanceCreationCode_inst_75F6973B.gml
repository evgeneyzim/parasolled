functions[0] = function() {
	with (inst_773ACDA3) start = true;	
}

functions[1] = function() {
	with (inst_35D07C09) start = true;	
}

functions[2] = function() {
	with (inst_536793DE) start = true;	
}

functions[3] = function() {
	with (inst_364EF9CD) active = true;	
}

deactivators[0] = function() {
	with (inst_773ACDA3) start = false;	
}

deactivators[1] = function() {
	with (inst_35D07C09) start = false;	
}

deactivators[2] = function() {
	with (inst_536793DE) start = false;	
}

deactivators[3] = function() {
	with (inst_364EF9CD) active = false;	
}