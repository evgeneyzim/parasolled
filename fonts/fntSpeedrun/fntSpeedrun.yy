{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Pixel Font7",
  "styleName": "Regular",
  "size": 50.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":34,"h":67,"character":32,"shift":34,"offset":0,},
    "33": {"x":346,"y":209,"w":8,"h":67,"character":33,"shift":13,"offset":3,},
    "34": {"x":356,"y":209,"w":21,"h":67,"character":34,"shift":27,"offset":3,},
    "35": {"x":379,"y":209,"w":34,"h":67,"character":35,"shift":40,"offset":3,},
    "36": {"x":415,"y":209,"w":34,"h":67,"character":36,"shift":40,"offset":3,},
    "37": {"x":451,"y":209,"w":34,"h":67,"character":37,"shift":40,"offset":3,},
    "38": {"x":487,"y":209,"w":31,"h":67,"character":38,"shift":37,"offset":3,},
    "39": {"x":520,"y":209,"w":8,"h":67,"character":39,"shift":13,"offset":3,},
    "40": {"x":530,"y":209,"w":18,"h":67,"character":40,"shift":23,"offset":3,},
    "41": {"x":550,"y":209,"w":18,"h":67,"character":41,"shift":23,"offset":3,},
    "42": {"x":570,"y":209,"w":24,"h":67,"character":42,"shift":30,"offset":3,},
    "43": {"x":596,"y":209,"w":28,"h":67,"character":43,"shift":34,"offset":3,},
    "44": {"x":626,"y":209,"w":11,"h":67,"character":44,"shift":17,"offset":3,},
    "45": {"x":639,"y":209,"w":28,"h":67,"character":45,"shift":34,"offset":3,},
    "46": {"x":669,"y":209,"w":8,"h":67,"character":46,"shift":13,"offset":3,},
    "47": {"x":679,"y":209,"w":34,"h":67,"character":47,"shift":40,"offset":3,},
    "48": {"x":715,"y":209,"w":34,"h":67,"character":48,"shift":40,"offset":3,},
    "49": {"x":751,"y":209,"w":14,"h":67,"character":49,"shift":20,"offset":3,},
    "50": {"x":310,"y":209,"w":34,"h":67,"character":50,"shift":40,"offset":3,},
    "51": {"x":274,"y":209,"w":34,"h":67,"character":51,"shift":40,"offset":3,},
    "52": {"x":238,"y":209,"w":34,"h":67,"character":52,"shift":40,"offset":3,},
    "53": {"x":915,"y":140,"w":34,"h":67,"character":53,"shift":40,"offset":3,},
    "54": {"x":728,"y":140,"w":34,"h":67,"character":54,"shift":40,"offset":3,},
    "55": {"x":764,"y":140,"w":34,"h":67,"character":55,"shift":40,"offset":3,},
    "56": {"x":800,"y":140,"w":34,"h":67,"character":56,"shift":40,"offset":3,},
    "57": {"x":836,"y":140,"w":34,"h":67,"character":57,"shift":40,"offset":3,},
    "58": {"x":872,"y":140,"w":8,"h":67,"character":58,"shift":13,"offset":3,},
    "59": {"x":882,"y":140,"w":11,"h":67,"character":59,"shift":17,"offset":3,},
    "60": {"x":895,"y":140,"w":18,"h":67,"character":60,"shift":23,"offset":3,},
    "61": {"x":951,"y":140,"w":28,"h":67,"character":61,"shift":34,"offset":3,},
    "62": {"x":218,"y":209,"w":18,"h":67,"character":62,"shift":23,"offset":3,},
    "63": {"x":981,"y":140,"w":28,"h":67,"character":63,"shift":34,"offset":3,},
    "64": {"x":2,"y":209,"w":34,"h":67,"character":64,"shift":40,"offset":3,},
    "65": {"x":38,"y":209,"w":34,"h":67,"character":65,"shift":40,"offset":3,},
    "66": {"x":74,"y":209,"w":34,"h":67,"character":66,"shift":40,"offset":3,},
    "67": {"x":110,"y":209,"w":34,"h":67,"character":67,"shift":40,"offset":3,},
    "68": {"x":146,"y":209,"w":34,"h":67,"character":68,"shift":40,"offset":3,},
    "69": {"x":182,"y":209,"w":34,"h":67,"character":69,"shift":40,"offset":3,},
    "70": {"x":767,"y":209,"w":34,"h":67,"character":70,"shift":40,"offset":3,},
    "71": {"x":833,"y":209,"w":34,"h":67,"character":71,"shift":40,"offset":3,},
    "72": {"x":2,"y":347,"w":34,"h":67,"character":72,"shift":40,"offset":3,},
    "73": {"x":869,"y":209,"w":14,"h":67,"character":73,"shift":20,"offset":3,},
    "74": {"x":405,"y":278,"w":34,"h":67,"character":74,"shift":40,"offset":3,},
    "75": {"x":441,"y":278,"w":34,"h":67,"character":75,"shift":40,"offset":3,},
    "76": {"x":477,"y":278,"w":34,"h":67,"character":76,"shift":40,"offset":3,},
    "77": {"x":513,"y":278,"w":34,"h":67,"character":77,"shift":40,"offset":3,},
    "78": {"x":549,"y":278,"w":34,"h":67,"character":78,"shift":40,"offset":3,},
    "79": {"x":585,"y":278,"w":34,"h":67,"character":79,"shift":40,"offset":3,},
    "80": {"x":621,"y":278,"w":34,"h":67,"character":80,"shift":40,"offset":3,},
    "81": {"x":657,"y":278,"w":34,"h":67,"character":81,"shift":40,"offset":3,},
    "82": {"x":693,"y":278,"w":34,"h":67,"character":82,"shift":40,"offset":3,},
    "83": {"x":729,"y":278,"w":34,"h":67,"character":83,"shift":40,"offset":3,},
    "84": {"x":765,"y":278,"w":34,"h":67,"character":84,"shift":40,"offset":3,},
    "85": {"x":801,"y":278,"w":34,"h":67,"character":85,"shift":40,"offset":3,},
    "86": {"x":837,"y":278,"w":34,"h":67,"character":86,"shift":40,"offset":3,},
    "87": {"x":873,"y":278,"w":34,"h":67,"character":87,"shift":40,"offset":3,},
    "88": {"x":909,"y":278,"w":34,"h":67,"character":88,"shift":40,"offset":3,},
    "89": {"x":945,"y":278,"w":34,"h":67,"character":89,"shift":40,"offset":3,},
    "90": {"x":981,"y":278,"w":34,"h":67,"character":90,"shift":40,"offset":3,},
    "91": {"x":389,"y":278,"w":14,"h":67,"character":91,"shift":20,"offset":3,},
    "92": {"x":353,"y":278,"w":34,"h":67,"character":92,"shift":40,"offset":3,},
    "93": {"x":337,"y":278,"w":14,"h":67,"character":93,"shift":20,"offset":3,},
    "94": {"x":92,"y":278,"w":28,"h":67,"character":94,"shift":34,"offset":3,},
    "95": {"x":885,"y":209,"w":34,"h":67,"character":95,"shift":40,"offset":3,},
    "96": {"x":921,"y":209,"w":14,"h":67,"character":96,"shift":20,"offset":3,},
    "97": {"x":937,"y":209,"w":28,"h":67,"character":97,"shift":34,"offset":3,},
    "98": {"x":967,"y":209,"w":28,"h":67,"character":98,"shift":34,"offset":3,},
    "99": {"x":2,"y":278,"w":28,"h":67,"character":99,"shift":34,"offset":3,},
    "100": {"x":32,"y":278,"w":28,"h":67,"character":100,"shift":34,"offset":3,},
    "101": {"x":62,"y":278,"w":28,"h":67,"character":101,"shift":34,"offset":3,},
    "102": {"x":122,"y":278,"w":21,"h":67,"character":102,"shift":27,"offset":3,},
    "103": {"x":307,"y":278,"w":28,"h":67,"character":103,"shift":34,"offset":3,},
    "104": {"x":145,"y":278,"w":28,"h":67,"character":104,"shift":34,"offset":3,},
    "105": {"x":175,"y":278,"w":8,"h":67,"character":105,"shift":13,"offset":3,},
    "106": {"x":185,"y":278,"w":14,"h":67,"character":106,"shift":20,"offset":3,},
    "107": {"x":201,"y":278,"w":28,"h":67,"character":107,"shift":34,"offset":3,},
    "108": {"x":231,"y":278,"w":8,"h":67,"character":108,"shift":13,"offset":3,},
    "109": {"x":241,"y":278,"w":34,"h":67,"character":109,"shift":40,"offset":3,},
    "110": {"x":277,"y":278,"w":28,"h":67,"character":110,"shift":34,"offset":3,},
    "111": {"x":698,"y":140,"w":28,"h":67,"character":111,"shift":34,"offset":3,},
    "112": {"x":668,"y":140,"w":28,"h":67,"character":112,"shift":34,"offset":3,},
    "113": {"x":638,"y":140,"w":28,"h":67,"character":113,"shift":34,"offset":3,},
    "114": {"x":608,"y":140,"w":28,"h":67,"character":114,"shift":34,"offset":3,},
    "115": {"x":766,"y":2,"w":28,"h":67,"character":115,"shift":34,"offset":3,},
    "116": {"x":796,"y":2,"w":21,"h":67,"character":116,"shift":27,"offset":3,},
    "117": {"x":819,"y":2,"w":28,"h":67,"character":117,"shift":34,"offset":3,},
    "118": {"x":849,"y":2,"w":34,"h":67,"character":118,"shift":40,"offset":3,},
    "119": {"x":885,"y":2,"w":34,"h":67,"character":119,"shift":40,"offset":3,},
    "120": {"x":921,"y":2,"w":28,"h":67,"character":120,"shift":34,"offset":3,},
    "121": {"x":951,"y":2,"w":28,"h":67,"character":121,"shift":34,"offset":3,},
    "122": {"x":2,"y":71,"w":28,"h":67,"character":122,"shift":34,"offset":3,},
    "123": {"x":235,"y":71,"w":14,"h":67,"character":123,"shift":20,"offset":3,},
    "124": {"x":32,"y":71,"w":8,"h":67,"character":124,"shift":13,"offset":3,},
    "125": {"x":42,"y":71,"w":14,"h":67,"character":125,"shift":20,"offset":3,},
    "126": {"x":58,"y":71,"w":31,"h":67,"character":126,"shift":37,"offset":3,},
    "1025": {"x":91,"y":71,"w":34,"h":67,"character":1025,"shift":40,"offset":3,},
    "1040": {"x":127,"y":71,"w":34,"h":67,"character":1040,"shift":40,"offset":3,},
    "1041": {"x":163,"y":71,"w":34,"h":67,"character":1041,"shift":40,"offset":3,},
    "1042": {"x":199,"y":71,"w":34,"h":67,"character":1042,"shift":40,"offset":3,},
    "1043": {"x":733,"y":2,"w":31,"h":67,"character":1043,"shift":37,"offset":3,},
    "1044": {"x":981,"y":2,"w":38,"h":67,"character":1044,"shift":44,"offset":3,},
    "1045": {"x":697,"y":2,"w":34,"h":67,"character":1045,"shift":40,"offset":3,},
    "1046": {"x":290,"y":2,"w":34,"h":67,"character":1046,"shift":40,"offset":3,},
    "1047": {"x":38,"y":2,"w":34,"h":67,"character":1047,"shift":40,"offset":3,},
    "1048": {"x":74,"y":2,"w":34,"h":67,"character":1048,"shift":40,"offset":3,},
    "1049": {"x":110,"y":2,"w":34,"h":67,"character":1049,"shift":40,"offset":3,},
    "1050": {"x":146,"y":2,"w":34,"h":67,"character":1050,"shift":40,"offset":3,},
    "1051": {"x":182,"y":2,"w":34,"h":67,"character":1051,"shift":40,"offset":3,},
    "1052": {"x":218,"y":2,"w":34,"h":67,"character":1052,"shift":40,"offset":3,},
    "1053": {"x":254,"y":2,"w":34,"h":67,"character":1053,"shift":40,"offset":3,},
    "1054": {"x":326,"y":2,"w":34,"h":67,"character":1054,"shift":40,"offset":3,},
    "1055": {"x":625,"y":2,"w":34,"h":67,"character":1055,"shift":40,"offset":3,},
    "1056": {"x":362,"y":2,"w":34,"h":67,"character":1056,"shift":40,"offset":3,},
    "1057": {"x":398,"y":2,"w":34,"h":67,"character":1057,"shift":40,"offset":3,},
    "1058": {"x":434,"y":2,"w":34,"h":67,"character":1058,"shift":40,"offset":3,},
    "1059": {"x":470,"y":2,"w":34,"h":67,"character":1059,"shift":40,"offset":3,},
    "1060": {"x":506,"y":2,"w":41,"h":67,"character":1060,"shift":47,"offset":3,},
    "1061": {"x":549,"y":2,"w":34,"h":67,"character":1061,"shift":40,"offset":3,},
    "1062": {"x":585,"y":2,"w":38,"h":67,"character":1062,"shift":44,"offset":3,},
    "1063": {"x":661,"y":2,"w":34,"h":67,"character":1063,"shift":40,"offset":3,},
    "1064": {"x":251,"y":71,"w":34,"h":67,"character":1064,"shift":40,"offset":3,},
    "1065": {"x":287,"y":71,"w":41,"h":67,"character":1065,"shift":47,"offset":3,},
    "1066": {"x":330,"y":71,"w":41,"h":67,"character":1066,"shift":47,"offset":3,},
    "1067": {"x":38,"y":140,"w":44,"h":67,"character":1067,"shift":50,"offset":3,},
    "1068": {"x":84,"y":140,"w":34,"h":67,"character":1068,"shift":40,"offset":3,},
    "1069": {"x":120,"y":140,"w":34,"h":67,"character":1069,"shift":40,"offset":3,},
    "1070": {"x":156,"y":140,"w":38,"h":67,"character":1070,"shift":44,"offset":3,},
    "1071": {"x":196,"y":140,"w":34,"h":67,"character":1071,"shift":40,"offset":3,},
    "1072": {"x":232,"y":140,"w":28,"h":67,"character":1072,"shift":34,"offset":3,},
    "1073": {"x":262,"y":140,"w":28,"h":67,"character":1073,"shift":34,"offset":3,},
    "1074": {"x":292,"y":140,"w":28,"h":67,"character":1074,"shift":34,"offset":3,},
    "1075": {"x":322,"y":140,"w":28,"h":67,"character":1075,"shift":34,"offset":3,},
    "1076": {"x":352,"y":140,"w":38,"h":67,"character":1076,"shift":44,"offset":3,},
    "1077": {"x":392,"y":140,"w":28,"h":67,"character":1077,"shift":34,"offset":3,},
    "1078": {"x":422,"y":140,"w":34,"h":67,"character":1078,"shift":40,"offset":3,},
    "1079": {"x":458,"y":140,"w":28,"h":67,"character":1079,"shift":34,"offset":3,},
    "1080": {"x":488,"y":140,"w":28,"h":67,"character":1080,"shift":34,"offset":3,},
    "1081": {"x":518,"y":140,"w":28,"h":67,"character":1081,"shift":34,"offset":3,},
    "1082": {"x":548,"y":140,"w":28,"h":67,"character":1082,"shift":34,"offset":3,},
    "1083": {"x":578,"y":140,"w":28,"h":67,"character":1083,"shift":34,"offset":3,},
    "1084": {"x":2,"y":140,"w":34,"h":67,"character":1084,"shift":40,"offset":3,},
    "1085": {"x":966,"y":71,"w":28,"h":67,"character":1085,"shift":34,"offset":3,},
    "1086": {"x":936,"y":71,"w":28,"h":67,"character":1086,"shift":34,"offset":3,},
    "1087": {"x":595,"y":71,"w":28,"h":67,"character":1087,"shift":34,"offset":3,},
    "1088": {"x":373,"y":71,"w":28,"h":67,"character":1088,"shift":34,"offset":3,},
    "1089": {"x":403,"y":71,"w":28,"h":67,"character":1089,"shift":34,"offset":3,},
    "1090": {"x":433,"y":71,"w":28,"h":67,"character":1090,"shift":34,"offset":3,},
    "1091": {"x":463,"y":71,"w":28,"h":67,"character":1091,"shift":34,"offset":3,},
    "1092": {"x":493,"y":71,"w":34,"h":67,"character":1092,"shift":40,"offset":3,},
    "1093": {"x":529,"y":71,"w":28,"h":67,"character":1093,"shift":34,"offset":3,},
    "1094": {"x":559,"y":71,"w":34,"h":67,"character":1094,"shift":40,"offset":3,},
    "1095": {"x":625,"y":71,"w":28,"h":67,"character":1095,"shift":34,"offset":3,},
    "1096": {"x":900,"y":71,"w":34,"h":67,"character":1096,"shift":40,"offset":3,},
    "1097": {"x":655,"y":71,"w":41,"h":67,"character":1097,"shift":47,"offset":3,},
    "1098": {"x":698,"y":71,"w":34,"h":67,"character":1098,"shift":40,"offset":3,},
    "1099": {"x":734,"y":71,"w":38,"h":67,"character":1099,"shift":44,"offset":3,},
    "1100": {"x":774,"y":71,"w":28,"h":67,"character":1100,"shift":34,"offset":3,},
    "1101": {"x":804,"y":71,"w":28,"h":67,"character":1101,"shift":34,"offset":3,},
    "1102": {"x":834,"y":71,"w":34,"h":67,"character":1102,"shift":40,"offset":3,},
    "1103": {"x":870,"y":71,"w":28,"h":67,"character":1103,"shift":34,"offset":3,},
    "1105": {"x":803,"y":209,"w":28,"h":67,"character":1105,"shift":34,"offset":3,},
    "9647": {"x":38,"y":347,"w":25,"h":67,"character":9647,"shift":37,"offset":6,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":1025,"upper":1025,},
    {"lower":1040,"upper":1103,},
    {"lower":1105,"upper":1105,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fntSpeedrun",
  "tags": [],
  "resourceType": "GMFont",
}